
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) Aquevix Solutions Pvt Ltd.
//
//  $AQUEVIX_FRAMEWORK_LICENSE$
//
//     For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
//////////////////////////////////////////////////////////////////////////////


#import "AQEmail.h"
@interface AQEmail()
@property (nonatomic, retain)UIViewController *parentController;
@end


@implementation AQEmail

-(void)send:(NSString *)rec bcc:(NSArray *)bccArr cc:(NSArray *)ccArr subject:(NSString *)sub body:(NSString *)body attachments:(NSArray *)attArr mimeType:(NSString *)mime withController:(UIViewController *)controller
{
    //self=[self retain];
    self.parentController=controller;
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        if(attArr!=nil && attArr.count>0)
        {
            for (id att in attArr) {
                [mailComposer addAttachmentData:att mimeType:mime fileName:@"Attachment"];
            }
        }
        [mailComposer setSubject:sub];
        [mailComposer setToRecipients:[NSArray arrayWithObject:rec]];
        if(ccArr!=nil)
            [mailComposer setCcRecipients:ccArr];
        if(bccArr!=nil)
            [mailComposer setBccRecipients:bccArr];
        [mailComposer setMessageBody:body isHTML:YES];
        [controller presentModalViewController:mailComposer animated:YES];
    }
    else
    {
        // Account not configured in iPhone
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Not Configured" message:@"Seems like your iPhone is currently not configured to send emails. Please check your settings and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        //[alert release];
    }
    
}

-(void)send:(NSString *)rec subject:(NSString *)sub body:(NSString *)body withController:(UIViewController *)controller
{
    [self send:rec bcc:nil cc:nil subject:sub body:body attachments:nil mimeType:nil withController:controller];
}


-(void)send:(NSString *)rec subject:(NSString *)sub body:(NSString *)body data:(NSData *)da mimeType:(NSString *)mime withController:(UIViewController *)controller
{
    NSArray *attArr=[[NSArray alloc] initWithObjects:da, nil];
    [self send:rec bcc:nil cc:nil subject:sub body:body attachments:attArr mimeType:mime withController:controller];
}

-(void)send:(NSString *)rec subject:(NSString *)sub body:(NSString *)body attachments:(NSArray *)attArr mimeType:(NSString *)mime withController:(UIViewController *)controller
{
    [self send:rec bcc:nil cc:nil subject:sub body:body attachments:attArr mimeType:mime withController:controller];
}

-(void)send:(NSString *)rec bcc:(NSArray *)bccArr subject:(NSString *)sub body:(NSString *)body withController:(UIViewController *)controller
{
    [self send:rec bcc:bccArr cc:nil subject:sub body:body attachments:nil mimeType:nil withController:controller];
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString *message=@"";
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = @"User cancelled email.";
            break;
        case MFMailComposeResultSaved:
            message = @"Draft saved.";
            break;
        case MFMailComposeResultSent:
            message = @"Mail has been sent.";
            break;
        case MFMailComposeResultFailed:
            message=@"Mail cannot be sent at this time due to an unknown system problem. Please try again later.";
            break;
    }
    [self.parentController dismissModalViewControllerAnimated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:message
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    //[self release];
    // [alert release];
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    NSString *message=@"";
    switch (result)
    {
        case MessageComposeResultCancelled:
            message = @"User cancelled text message.";
            break;
        case MessageComposeResultSent:
            message = @"Message has been sent.";
            break;
        case MessageComposeResultFailed:
            message=@"Message cannot be sent at this time due to an unknown system problem. Please try again later.";
            break;
    }
    [self.parentController dismissModalViewControllerAnimated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    //[self release];
}
@end
