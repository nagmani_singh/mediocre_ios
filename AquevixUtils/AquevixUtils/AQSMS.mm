
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) Aquevix Solutions Pvt Ltd.
//
//  $AQUEVIX_FRAMEWORK_LICENSE$
//
//     For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
//////////////////////////////////////////////////////////////////////////////


#import "AQSMS.h"

@interface AQSMS()
@property (nonatomic, retain)UIViewController *parentController;
@end

@implementation AQSMS
-(void)send:(NSString *)recipient body:(NSString *)body  withController:(UIViewController *)controller
{
    //self=[self retain];
    self.parentController=controller;
    MFMessageComposeViewController *control = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        control.body = body;
        control.recipients = [NSArray arrayWithObject:recipient];
        control.messageComposeDelegate = self;
        [controller presentModalViewController:control animated:YES];
    }
}


-(void)send:(NSString *)recipient body:(NSString *)body attachmets:(NSArray *) attArr withController:(UIViewController *)controller
{
    self.parentController=controller;
    MFMessageComposeViewController *control = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText] && [MFMessageComposeViewController canSendAttachments])
    {
        control.body = body;
        control.recipients = [NSArray arrayWithObject:recipient];
        control.messageComposeDelegate = self;
        //Here we are adding attachments.
        for (int i=0; i<[attArr count]; i++) {
            id attchment=[attArr objectAtIndex:i];
            NSString* attName=[NSString stringWithFormat:@"image%d.jpg",i];
            [control addAttachmentData:attchment typeIdentifier:@"public.data" filename:attName];
        }
        [controller presentModalViewController:control animated:YES];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    NSString *message=@"";
    switch (result)
    {
        case MessageComposeResultCancelled:
            message = @"User cancelled text message.";
            break;
        case MessageComposeResultSent:
            message = @"Message has been sent.";
            break;
        case MessageComposeResultFailed:
            message=@"Message cannot be sent at this time due to an unknown system problem. Please try again later.";
            break;
    }
    [self.parentController dismissModalViewControllerAnimated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    //[self release];
}
@end
