
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//
//  NSDate+aquevix.m
//  AQAppCore
//
//  Created by Velidi Bharatdwaj on 14/03/14.
//  Copyright (c) 2014 Aquevix Solutions Pvt Ltd. All rights reserved.
//

#import "NSDate+aquevix.h"

@implementation NSDate (aquevix)
static NSString * defaultDateFormatter=@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

+ (void) setDefaultDateFormatter:(NSString *) format
{
    defaultDateFormatter=format;
}

-(NSDate *) toLocal
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

-(NSDate *) toTimezone:(NSDateFormatter *) formatter
{
    NSString* strDate=[formatter stringFromDate:self];
    return [NSDate parse:strDate FormatterString:defaultDateFormatter];
}

-(NSDate *) toUTC
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:defaultDateFormatter];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString* strDate= [dateFormatter stringFromDate:self];
    return [NSDate parse:strDate FormatterString:defaultDateFormatter];
}

+(NSDate *) parse:(NSString *) strDate FormatterString :(NSString *) strFrmtr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if(![strFrmtr isEqualToString:@""])
        [dateFormatter setDateFormat:strFrmtr];
    else
        [dateFormatter setDateFormat:defaultDateFormatter];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    return [dateFormatter dateFromString:strDate];
}

-(NSString *) toString : (NSString *) format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if(![format isEqualToString:@""])
        [dateFormatter setDateFormat:format];
    else
        [dateFormatter setDateFormat:defaultDateFormatter];
    NSString* strDate= [dateFormatter stringFromDate:self];
    return strDate;
}

@end

////Here we are converting current date to String.
//NSDate * currdt=[NSDate date];
//// Here we are converting our current date to EST date using toTimezone methode in NSDate+aquevix helper.
//NSDateFormatter *timeFormatter_est = [[NSDateFormatter alloc] init];
//[timeFormatter_est setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
//[timeFormatter_est setTimeZone:[NSTimeZone timeZoneWithName:@"EST"]];
//[NSDate setDefaultDateFormatter:@"MM/dd/yyyy HH:mm:ss"];
//NSDate* toTimeZone=[currdt toTimezone:timeFormatter_est];
//NSString *result=[toTimeZone toString:@"MM/dd/yyyy HH:mm:ss"];
//// Here we are converting our current date to UTC date using toUTC methode in NSDate+aquevix helper.
//[currdt toUTC];
//// Here we are converting our current date to Local date using toLocal methode in NSDate+aquevix helper.
//[currdt toLocal];
//
//NSLog(@"Converted EST time : %@",result);
