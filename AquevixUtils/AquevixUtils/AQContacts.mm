
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) Aquevix Solutions Pvt Ltd.
//
//  $AQUEVIX_FRAMEWORK_LICENSE$
//
//     For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
//////////////////////////////////////////////////////////////////////////////


#import "AQContacts.h"

// TODO: Reimplement using iOS6, Privacy access, Modification Callback and Outputs a C++ class. Also ARC safe
//  http://stackoverflow.com/questions/14010320/abaddressbookcreate-abaddressbookgetgroupcount-return-0x00000000-ni
//  http://stackoverflow.com/questions/12517394/ios-6-address-book-not-working

@implementation AQContacts
+(NSArray *) getAllFromAddressBook
{
    ABAddressBookRef addressBook = ABAddressBookCreate();
	NSArray *allPeople = (NSArray *)CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
    CFRelease(addressBook);
	NSMutableArray *contactList = [[NSMutableArray alloc]initWithCapacity:[allPeople count]];
	for (id record in allPeople) {
        ABRecordRef recordRef=(ABRecordRef)CFBridgingRetain(record);
		NSString* currContactName = (NSString *)CFBridgingRelease(ABRecordCopyCompositeName(recordRef));
        //[record release];
        
		NSMutableDictionary *contact = [[NSMutableDictionary alloc] init];
		[contact setObject:currContactName forKey:@"name"];
		//[currContactName release];
        
        CFTypeRef phoneProperty = ABRecordCopyValue((ABRecordRef)CFBridgingRetain(record), kABPersonPhoneProperty);
        NSArray *phones = ( NSArray *)CFBridgingRelease(ABMultiValueCopyArrayOfAllValues(phoneProperty));
        CFRelease(phoneProperty);
        
		NSMutableString *newPhone = [[NSMutableString alloc] init];
		for (NSString *phone in phones) {
			if(![newPhone isEqualToString:@""])
				[newPhone appendString:@", "];
			[newPhone appendString:phone];
        }
        //[phones release];
		[contact setObject:newPhone forKey:@"phone"];
        //[newPhone release];
        [contactList addObject:contact];
        //[contact release];
    }
    //[allPeople release];
	
    return contactList;
}
@end
