## ================================================================== ##
##
##      Build script for XCode Projects. Copyrights Aquevix.
##
##                          DO NOT EDIT
## ================================================================== ##

CWD=`dirname $0`
CONFIG=$1

. $CWD/$CONFIG

WORKSPACE="$PROJECT_NAME.xcworkspace"
SCHEME="$PROJECT_NAME"
BUILDDIR="builddir"
ARCHIVE_PATH="$BUILDDIR/$PROJECT_NAME.xcarchive"
IPA_PATH="$BUILDDIR/$PROJECT_NAME"

rm -r $BUILDDIR
mkdir $BUILDDIR
set -o pipefail && /usr/bin/security unlock-keychain -p $KEYCHAIN_PASSWD  $KEYCHAIN
set -o pipefail && /usr/bin/security show-keychain-info $KEYCHAIN

set -o pipefail && xcodebuild clean archive -workspace "$WORKSPACE" -scheme "$SCHEME" -configuration Release -archivePath "$ARCHIVE_PATH" -parallelizeTargets -jobs 4 GCC_PREPROCESSOR_DEFINITIONS="\${inherited} $DEFINE_OPTS"| xcpretty

if [[ "$DISABLEIPA" == "" ]]
then
   set -o pipefail && xcodebuild -exportArchive -archivePath "$ARCHIVE_PATH" -exportPath "$IPA_PATH" -exportFormat ipa -exportProvisioningProfile "$PROVISIONING_PROFILE"
fi
