
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//
//  AQTestingScreen.m
//  LocalView
//
//  Created by Developer on 14/03/13.
//
//

#import "AQTestingScreen.h"


@interface AQTestingScreen ()

@end

@implementation AQTestingScreen
@synthesize screensArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.screensArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=nil;
    cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    NSString* viewName=[self.screensArray objectAtIndex:[indexPath row]];
    [cell.textLabel setText:viewName];
    [cell.textLabel setTextColor:[UIColor blackColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
    [cell.textLabel setTextColor:[UIColor colorWithRed:0.478 green:0.478 blue:0.478 alpha:1.0]];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* viewName=[self.screensArray objectAtIndex:[indexPath row]];
    UIViewController* viewController = (UIViewController*)[[NSClassFromString(viewName) alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}


- (void)viewDidLoad
{
    CGSize screenSize=[[UIScreen mainScreen]bounds].size;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tblTest=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    [self.tblTest setDelegate:self];
    [self.tblTest setDataSource:self];
    [self.view addSubview:self.tblTest];
    [self.tblTest reloadData];
}

// Here we need to add the screen names as NSString objects. They will automatically
// be loaded and shown.
-(void) initScreens:(NSMutableArray*) screensArr
{
    self.screensArray=screensArr;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTblTest:nil];
    [super viewDidUnload];
}
@end
