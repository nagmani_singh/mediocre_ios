
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//
//  AQUIBaseView.m
//  AQAppCore
//
//  Created by  velidibharatdwaj on 11/09/13.
//  Copyright (c) 2013 Aquevix Solutions Pvt Ltd. All rights reserved.
//

#import "AQUIBaseViewController.h"



@interface AQUIBaseViewController ()



@end

@implementation AQUIBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ([UIDevice currentDevice].systemVersion.floatValue>=7.0) {
        [self setAutomaticallyAdjustsScrollViewInsets:NO];
    }
    
    [self setStatusBarBG:YES];
   
}
-(void)onDataSuccess:(id)res {
    if ([[res getRequest]getType] == GET) {
        [self bind:res];
    }
    else if ([[res getRequest]getType] == POST || [[res getRequest]getType] == PUT) {
        [self parse:res];
    }
    
}

-(void)onDataError:(id)res {
    if ([[res getRequest]getType] == GET) {
        [self bind:res];
    }
    else if ([[res getRequest]getType] == POST || [[res getRequest]getType] == PUT) {
        [self parse:res];
    }
}
-(void)parse:(id)obj {
    
}
-(void)bind:(id)obj {
    
}
-(void)setStatusBarBG:(BOOL)state
{
    if (state) {
        if ([[UIDevice currentDevice] systemVersion].floatValue>=7.0) {
            UIImageView *statusImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 20)];
            [statusImg setImage:[UIImage imageNamed:@"Top_statusBarBG.png"]];
            [self.view addSubview:statusImg];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
