#import <Foundation/Foundation.h>

@class ResponseStatus;
@class PodCasts;

@interface GetPodCastsResponse : NSObject
-(id) parse:(NSDictionary *) dict;
@property(nonatomic,strong) ResponseStatus* error;
@property(nonatomic,strong) NSMutableArray * arrPodcasts;


@end
