//
//  Events.h
//  AppAPI
//
//  Created by Pankaj Kumar on 26/10/16.
//  Copyright © 2016 Aquevix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Events : NSObject

@property (strong,nonatomic) NSString *EventTitle;
@property (strong,nonatomic) NSString *datetime;
@property (strong,nonatomic) NSString *TimeFrom;
@property (strong,nonatomic) NSString *TimeTo;
@property (strong,nonatomic) NSString *Company;
@property (strong,nonatomic) NSString *Address;
@property (strong,nonatomic) NSString *Description;
@property (strong,nonatomic) NSString *eventType;

@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (nonatomic) BOOL IsActive;
-(id) parse:(NSDictionary *) dict;
@end
