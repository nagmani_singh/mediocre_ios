/*
 Code generated by Aquevix API Generator (http://www.aquevix.com)
 Project: MediocreApp
 Author:	 Aquevix Solutions Pvt Ltd.
 Generated: 08/03/16 11:22:05



*/

#import "AQGetPodCastsResponse.h"


#include "GetPodCastsResponse.h"
@implementation AQGetPodCastsResponse
-(id)init
{
    self=[super init];
    return self;
}
-(ResponseStatus *)  error;
{
    return error_;
}         
-(NSMutableArray *)  podCasts;
{
    return podcastsVector_;
}

///****************************************
/// Here we are overiding parseResult to
/// parse the json string we are getting.
///****************************************
-(void) parseResult
{
        NSString* jsonStr=[super getData];
        NSData *jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
      if (jsonData) {
        NSError* error=[[NSError alloc]init];
        id value= [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
         if (![value isEqual:[NSNull null]]) {
        GetPodCastsResponse* getpodcastsresponse=[[GetPodCastsResponse alloc]init];
        [getpodcastsresponse parse:value];
       error_ = getpodcastsresponse.error;
       podcastsVector_ = getpodcastsresponse.arrPodcasts;
         }
    }
}
@end


