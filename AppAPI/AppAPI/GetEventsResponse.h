#import <Foundation/Foundation.h>

@class ResponseStatus;
@class PodCasts;

@interface GetEventsResponse : NSObject
-(id) parse:(NSDictionary *) dict;
@property(nonatomic,strong) ResponseStatus* error;
@property(nonatomic,strong) NSMutableArray * arrPodcasts;


@end
