#import "Ads.h"



@implementation Ads

// Synthesize the members here.
@synthesize id;
@synthesize imageUrl;
@synthesize url;
@synthesize company;

// Parse event to load data into self from dictionary.
-(id) parse:(NSDictionary *) dict
{
    self.id=[[dict objectForKey:@"id"]integerValue];
    self.company=[dict objectForKey:@"Company"];
    self.url=[dict objectForKey:@"URL"];
    self.imageUrl=[dict objectForKey:@"ImageURL"];

return self;
}
@end