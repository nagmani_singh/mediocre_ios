#import <Foundation/Foundation.h>

@class ResponseStatus;
@class UserDetail;

@interface PostLoginResponse : NSObject
-(id) parse:(NSDictionary *) dict;
@property(nonatomic,strong)  ResponseStatus* error;  
@property(nonatomic,strong)NSDictionary *dictUserDetail;

@end
