
#import <Foundation/Foundation.h>


@interface Ads : NSObject

// Properties for this data object

@property NSInteger id;
@property (nonatomic,strong) NSString * imageUrl;
@property (nonatomic,strong) NSString * company;
@property (nonatomic,strong) NSString * url;
// Parse method to load data
-(id) parse:(NSDictionary *) dict;
@end
