#import "GetEventsResponse.h"


    #import "ResponseStatus.h"
    #import "Events.h"

@implementation GetEventsResponse
     @synthesize error;
     @synthesize arrPodcasts;

-(id) parse:(NSDictionary *) dict
{
     
     id errorVal=[dict objectForKey:@"error"];
    if (![errorVal isEqual:[NSNull null]]) {
     ResponseStatus * responsestatus_=[[ResponseStatus alloc]init];
     self.error=[[ResponseStatus alloc]init];
     self.error=[responsestatus_ parse:errorVal];
    }
     id val =[dict objectForKey:@"Events"];
     if (![val isEqual:[NSNull null]]) {
     self.arrPodcasts=[[NSMutableArray alloc]init];
     for (id obj in val) {
     Events* podcasts_  =[[Events alloc]init];
     [self.arrPodcasts addObject:[podcasts_ parse:obj]];
     }   }

return self;
}

@end
