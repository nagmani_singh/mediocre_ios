#import <Foundation/Foundation.h>

@class ResponseStatus;
@class Ads;

@interface GetAdsResponse : NSObject
-(id) parse:(NSDictionary *) dict;
@property(nonatomic,strong) ResponseStatus* error;
@property(nonatomic,strong) NSMutableArray * arrAds;


@end
