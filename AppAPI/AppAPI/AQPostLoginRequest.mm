/*
 Code generated by Aquevix API Generator (http://www.aquevix.com)
 Project: MediocreApp
 Author:	 Aquevix Solutions Pvt Ltd.
 Generated: 08/03/16 11:22:05


*/
#import "AQPostLoginRequest.h"
#import "AppAPI.h"
    #import "AQPostLoginResponse.h"
@implementation AQPostLoginRequest
-(id)init
{
     self=[super init];
     [super setHeader:@"X-APP_VERSION" value:[[Application instance] versionString]];
     [super setHeader:@"X-DEVICE_TYPE" value:[[Application instance] phoneType]];
     [super setHeader:@"X-APPKEY" value:[[Application instance]appKey]];
     [super setHeader:@"X-DEVICEID" value:[[Application instance] deviceID]];
     [super setServiceUri:@"/api/v1/user/login"];
     [super setHeader:@"Content-Type" value:@"application/Json"];
     [super setType:POST];
     return self;
    
}

-(void)setServiceUriMiddleValue:(NSString *)serviceUri{
        [super setServiceUri:[NSString stringWithFormat:@"%@user/login",serviceUri]];
    
}
//****************************************
// setter method for adding user_email to the
///params Dictionary.
-(void) setUser_email:(NSString *) user_email
{
    [super setParam:@"user_email" value:user_email];
}
///****************************************
/// getter methode for reading user_email from
/// the params Dictionary.
///****************************************
-(NSString *) getUser_email
{
    return [super getParam:@"user_email"];
}
//****************************************
// setter method for adding user_password to the
///params Dictionary.
-(void) setUser_password:(NSString *) user_password
{
    [super setParam:@"user_password" value:user_password];
}
///****************************************
/// getter methode for reading user_password from
/// the params Dictionary.
///****************************************
-(NSString *) getUser_password
{
    return [super getParam:@"user_password"];
}


-(AQResponse*) createResponse
{
    AQPostLoginResponse* response=[[AQPostLoginResponse alloc]init];
    return response;
}
@end










