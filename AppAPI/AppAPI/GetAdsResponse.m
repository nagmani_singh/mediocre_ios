#import "GetAdsResponse.h"


    #import "ResponseStatus.h"
    #import "Ads.h"

@implementation GetAdsResponse
     @synthesize error;
     @synthesize arrAds;

-(id) parse:(NSDictionary *) dict
{
     
     id errorVal=[dict objectForKey:@"error"];
    if (![errorVal isEqual:[NSNull null]]) {
     ResponseStatus * responsestatus_=[[ResponseStatus alloc]init];
     self.error=[[ResponseStatus alloc]init];
     self.error=[responsestatus_ parse:errorVal];
    }
     id val =[dict objectForKey:@"GetAds"];
     if (![val isEqual:[NSNull null]]) {
     self.arrAds=[[NSMutableArray alloc]init];
     
     Ads* ads_  =[[Ads alloc]init];
     [self.arrAds addObject:[ads_ parse:val]];
    
     }

return self;
}

@end
