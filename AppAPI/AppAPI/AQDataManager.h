//
//  AQDataManager.h
//  PickXCustomerAppAPI
//
//  Created by Atul Kumar on 21/07/15.
//  Copyright (c) 2015 Aquevix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppAPI.h"






@interface AQDataManager : NSObject

+(void)getMasterData:(AQRequest*)obj controller:(UIViewController*)cntl;
+(BOOL)isNetworkAvailable;
@end
