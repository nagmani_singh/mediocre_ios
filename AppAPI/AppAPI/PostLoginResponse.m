#import "PostLoginResponse.h"


    #import "ResponseStatus.h"

@implementation PostLoginResponse
     @synthesize error;
@synthesize dictUserDetail;
-(id) parse:(NSDictionary *) dict
{
    
    id errorVal=[dict objectForKey:@"error"];
    if (![errorVal isEqual:[NSNull null]]) {
        ResponseStatus * responsestatus_=[[ResponseStatus alloc]init];
        self.error=[[ResponseStatus alloc]init];
        self.error=[responsestatus_ parse:errorVal];
    }
    

    
    
    id userDetailVal=[dict objectForKey:@"UserDetail"];
    if (![userDetailVal isEqual:[NSNull null]]) {
        dictUserDetail=userDetailVal;
        
    }
return self;
}

@end
