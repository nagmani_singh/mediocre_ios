#import "ResponseStatus.h"



@implementation ResponseStatus

// Synthesize the members here.
    @synthesize stackTrace,message;

// Parse event to load data into self from dictionary.
-(id) parse:(NSDictionary *) dict
{
    if (dict == (id)[NSNull null])
    {
        
    }
    else
    {
       if([dict objectForKey:@"error"] != nil)
       {
           
           self.message=[dict objectForKey:@"error"];
        }
       else
       {
           self.stackTrace=[dict objectForKey:@"StackTrace"];
           self.message=[dict objectForKey:@"message"];
       }
        
    }
return self;
}
@end