
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol podCastsDelegate <NSObject>
-(void)downloadedPrograssBDM :(float)receivedata withExpectedData:(float)expectedData withCellID:(int)cellId withClass:(UIViewController *)isclass;

-(void)downloadedPrograssPodcast :(float)receivedata withExpectedData:(float)expectedData withCellID:(int)cellId withClass:(UIViewController *)isclass;

-(void)afterTaskComplete:(int)index;

@end

@interface PodCasts : NSObject<NSURLSessionDelegate,NSURLSessionDataDelegate>

// Properties for this data object

@property NSInteger id;
@property (nonatomic,strong) NSString * date;
@property (nonatomic,strong) NSString * url;
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * type;
@property (nonatomic) double  lastTimePlayed;

@property (nonatomic,strong) NSMutableArray * allPodcastArray;
@property (strong)id<podCastsDelegate>delegate;


//@property (nonatomic) enum stateType downloadState;

// Parse method to load data
-(id) parse:(NSDictionary *) dict;
+ (PodCasts*)sharedInstance;
-(void)downloadAudioStream:(NSURL *)aurl :(int)cellID
;
-(void)downloadedPrograss :(float)receivedata withExpectedData:(float)expectedData withCellID:(int)cellId withClass:(UIViewController *)isclass;
-(void)afterTaskComplete:(int)index;
@end
