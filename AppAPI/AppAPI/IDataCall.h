//
//  IDataCall.h
//  PickXCustomerAppAPI
//
//  Created by Atul Kumar on 22/07/15.
//  Copyright (c) 2015 Aquevix. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IDataCall <NSObject>
-(void)onDataSuccess:(id)obj;
-(void)onDataError:(id)obj;
@end
