#import "PodCasts.h"
#import <UIKit/UIKit.h>


@implementation PodCasts
{
    int64_t *receivedData;
    long expectedBytes;
    NSURLSessionConfiguration *sessionConfiguration;
}
// Synthesize the members here.
@synthesize id;
@synthesize date;
@synthesize url;
@synthesize title;
//@synthesize type;
@synthesize delegate;
// Parse event to load data into self from dictionary.

+ (PodCasts*)sharedInstance
{
    static dispatch_once_t once;
    static PodCasts *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        
           });
    return sharedInstance;
}

//-(void)downloadedPrograss :(float)receivedata withExpectedData:(float)expectedData withCellID:(int)cellId withClass:(UIViewController *)isclass
//{
//    
//    if ([self.delegate isKindOfClass:[AQPodcastScreen class]])
//    {
//        if ([self.delegate respondsToSelector:@selector(downloadedPrograssBDM:withExpectedData:withCellID:withClass: )])
//        {
//            [self.delegate downloadedPrograssBDM:receivedata withExpectedData:expectedData withCellID:cellId withClass:isclass];
//            
//        }
//    }else {
//  
//    
//     if ([self.delegate respondsToSelector:@selector(downloadedPrograssPodcast:withExpectedData:withCellID:withClass: )])
//    {
//          [self.delegate downloadedPrograssPodcast:receivedata withExpectedData:expectedData withCellID:cellId withClass:isclass];
//    }
//    }
//  
//    
//}

-(void)afterTaskComplete:(int)index
{

if ([self.delegate respondsToSelector:@selector(afterTaskComplete:)])
{
    [self.delegate afterTaskComplete:index];
    
}

}

-(void)downloadAudioStream:(NSURL *)aurl :(int)cellID
{
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:aurl     cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
   // NSMutableData *receivedData = [[NSMutableData alloc] initWithLength:0];
//    NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self startImmediately:YES];
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfiguration.HTTPMaximumConnectionsPerHost = 3;
//        sessionConfiguration.timeoutIntervalForResource = 120;
//        sessionConfiguration.timeoutIntervalForRequest = 120;
    });
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
   NSURLSessionDownloadTask *downloadTasks= [session downloadTaskWithRequest:theRequest];
    
    downloadTasks.taskDescription=[NSString stringWithFormat:@"%d",cellID];
//    downloadTasks.priority=0;
    [downloadTasks resume];
    
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite

{
    [self downloadedPrograss:(float)totalBytesWritten withExpectedData:(float)totalBytesExpectedToWrite withCellID:downloadTask.taskDescription.integerValue withClass:self.delegate];
    

}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSLog(@"%@",location);

    NSString *documentsPath    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *finalPath        = [documentsPath stringByAppendingPathComponent:[[[downloadTask originalRequest] URL] lastPathComponent]];
    NSLog(@"%@",finalPath);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL success;
    NSError *error;
    if ([fileManager fileExistsAtPath:finalPath])
    {
        success = [fileManager removeItemAtPath:finalPath error:&error];
        NSAssert(success, @"removeItemAtPath error: %@", error);
    }
    
    success = [fileManager moveItemAtURL:location toURL:[NSURL fileURLWithPath:finalPath] error:&error];
    NSAssert(success, @"moveItemAtURL error: %@", error);
    
    
    
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (error)
    {
        dispatch_async(dispatch_get_main_queue(),
                       ^{
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:error.localizedFailureReason message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        });
        
       
        
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init])
    {
        
         self.id =[aDecoder decodeIntForKey:@"id"];
        self.date = [aDecoder decodeObjectForKey:@"PostedOn"];
        self.url = [aDecoder decodeObjectForKey:@"Url"];
       self.title=[aDecoder decodeObjectForKey:@"Title"];
        self.type=[aDecoder decodeObjectForKey:@"Type"];
        self.lastTimePlayed=[aDecoder decodeDoubleForKey:@"lastTimePlayed"];
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeInt:self.id forKey:@"id"];
    [encoder encodeObject:self.date forKey:@"PostedOn"];
    [encoder encodeObject: self.url forKey:@"Url"];
    [encoder encodeObject: self.title forKey:@"Title"];
    [encoder encodeObject:self.type forKey:@"Type"];
    [encoder encodeDouble:self.lastTimePlayed forKey:@"lastTimePlayed"];
    
}


-(id) parse:(NSDictionary *) dict
{
    self.id=[[dict objectForKey:@"id"]integerValue];
    self.date=[dict objectForKey:@"PostedOn"];
    self.url=[dict objectForKey:@"Url"];
    self.title=[dict objectForKey:@"Title"];
    self.type=[dict objectForKey:@"Type"];

return self;
}
@end