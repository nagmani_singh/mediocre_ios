#import <Foundation/Foundation.h>

@class ResponseStatus;

@interface GetIsUserActiveResponse : NSObject{
 
}
-(id) parse:(NSDictionary *) dict;
@property(nonatomic,strong) ResponseStatus* error;
@property(nonatomic,strong) NSDictionary *dictIsUserActive;

@end
