#import "GetIsUserActiveResponse.h"


    #import "ResponseStatus.h"

@implementation GetIsUserActiveResponse
     @synthesize error;
@synthesize dictIsUserActive;

-(id) parse:(NSDictionary *) dict
{
     
     id errorVal=[dict objectForKey:@"error"];
    if (![errorVal isEqual:[NSNull null]]) {
     ResponseStatus * responsestatus_=[[ResponseStatus alloc]init];
     self.error=[[ResponseStatus alloc]init];
     self.error=[responsestatus_ parse:errorVal];
    }
     id val =[dict objectForKey:@"UserDetail"];
     if (![val isEqual:[NSNull null]]) {
   
         dictIsUserActive = val;
         
     }

return self;
}

@end
