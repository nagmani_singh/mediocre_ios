//
//  Events.m
//  AppAPI
//
//  Created by Pankaj Kumar on 26/10/16.
//  Copyright © 2016 Aquevix. All rights reserved.
//

#import "Events.h"

@implementation Events
-(id) parse:(NSDictionary *) dict
{
    _EventTitle=dict[@"EventTitle"];
   _datetime=dict[@"Date"];
    _TimeFrom=dict[@"TimeFrom"];
    _TimeTo=dict[@"TimeTo"];
    _Company=dict[@"Company" ];
    _Address=dict[@"Address"];
//    _latitude=[dict[@"lat" ] floatValue];
//    _longitude=[dict[@"lon" ] floatValue];
    _Description=dict[@"Description" ];
    _IsActive=[dict[@"IsActive" ] boolValue];
    _eventType=dict[@"Type"];
    return self;
}
@end
