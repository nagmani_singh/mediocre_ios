//
//  AppAPI.h
//  AppAPI
//
//  Created by Atul Kumar on 16/09/15.
//  Copyright (c) 2015 Aquevix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//! Project version number for AppAPI.
FOUNDATION_EXPORT double AppAPIVersionNumber;
//! Project version string for AppAPI.
FOUNDATION_EXPORT const unsigned char AppAPIVersionString[];
// In this header, you should import all the public headers of your framework using statements like #import <AppAPI/PublicHeader.h>
#import "AquevixCore/AQRequest.h"
#import "AquevixCore/AQResponse.h"
#import "AquevixCore/AQServiceManager.h"
#import "AquevixCore/Application.h"
#import "AquevixCore/AQNSExtensions.h"
//#import "IBindableUI.h"
//#import "IDataCall.h"
//#import "AQDataManager.h"
@interface AppAPI : NSObject
@end