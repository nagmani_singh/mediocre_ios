//
//  AQDataManager.m
//  PickXCustomerAppAPI
//
//  Created by Atul Kumar on 21/07/15.
//  Copyright (c) 2015 Aquevix. All rights reserved.
//

#import "AQDataManager.h"
#import <Reachability.h>
@implementation AQDataManager

+(void)getMasterData:(AQRequest *)obj controller:(UIViewController *)cntl
{
    AQServiceManager *manager = [AQServiceManager instance];
    [manager enqueue:obj delegate:cntl callbackSelector:nil];
}
+(BOOL)isNetworkAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        return false;
    } else {
        NSLog(@"There IS internet connection");
        return true;
    }
}


@end
