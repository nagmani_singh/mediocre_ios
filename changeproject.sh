#!/bin/sh

PROJ=$1

echo "    ============================================    "
echo
echo "         Aquevix Script to rename a project"
echo
echo "    ============================================    "

## perl -pi -e 's#location = "group:App.xcodeproj"#location = "group:'$PROJ'.xcodeproj"#g' App.xcworkspace/contents.xcworkspacedata
mv App.xcworkspace $PROJ.xcworkspace
## mv App.xcodeproj $PROJ.xcodeproj

perl -pi -e 's#App\.xcworkspace#'$PROJ'.xcworkspace#g' Podfile
perl -pi -e "s#target 'App' do#target '"$PROJ"' do#g" Podfile
perl -pi -e "s#xcodeproj 'App.xcodeproj'#xcodeproj '"$PROJ".xcodeproj'#g" Podfile

echo "$PROJ.xcworkspace/xcuserdata" >> .gitignore

echo "    ============================================    "
echo "         All done. Now you need to do the following:"
echo ""
echo "          1. Open the project and rename App on left navigation to $PROJ"
echo "          2. In Toolbar where it shows project name, click and select Manage Schemes"
echo "          3. Delete the scheme called 'App'"
echo "          4. Click 'Autocreate Schemes'"
echo "          5. At bottom you will see a scheme called $PROJ. Select checkbox 'Shared' for this scheme."
echo "          6. Close and Reopen Solution"
echo "    ============================================    "
echo
echo
echo
echo
