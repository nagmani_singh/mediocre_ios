//
//  AQPlayerMnager.h
//  TomandDanMediocreApp
//
//  Created by Pankaj Kumar on 05/08/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AQSavedPodcastManager.h"
@interface AQPlayerMnager : NSObject
+ (id)sharedManager;
@property (strong,nonatomic) AVPlayer *player;
@property (strong,nonatomic) AVPlayerItem *playerItem;

@property (strong,nonatomic) PodCasts *oldPodcast;
-(void)savingPod:(PodCasts *)podcastOld
;
-(void)addPlayerWithPodcast:(PodCasts *)podCasts orUrl:(NSString *)urlStr;
-(void)setPlayerObj:(AVPlayer *)ply withPodcast:(PodCasts *)podC;
-(void)savinglastPodOnTermination;

@end
