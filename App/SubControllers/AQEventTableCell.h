//
//  AQEventTableCell.h
//  TomandDanMediocreApp
//
//  Created by Pankaj Kumar on 27/10/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AppAPI/Events.h>
@interface AQEventTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgLeft;
@property (weak, nonatomic) IBOutlet UIImageView *imgRight;

-(void)bindCellwithData:(Events *)events withIndex:(long)index;
@end
