//
//  AQBDMCell.m
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQBDMCell.h"
#import "AQConfig.h"
#import "AQHelpers.h"
@implementation AQBDMCell

- (void)awakeFromNib {
    // Initialization code
    self.cellProgress=[[UIProgressView alloc]initWithFrame:CGRectMake(0, 0, 320, 5)];
    [self.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0]];
    [self.contentView addSubview:self.cellProgress];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)bindData:(NSMutableArray*)arrData index:(NSInteger)indexPath{
    //[self setFontAndColor];
    if (indexPath%2!=0)
    {
        self.vwbackground.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
        self.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-bdm.png"];
        self.imgBDM.image = [UIImage imageNamed:@"icon_bdm-even.png"];
        self.lblBDMDate.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
        self.lblBDMTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
        [self.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0]];
        [self.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0]];
    }
    else{
        self.vwbackground.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1.0];
        self.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-bdm.png"];
        self.imgBDM.image = [UIImage imageNamed:@"icon_bdm-odd.png"];
        self.lblBDMDate.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
        self.lblBDMTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
        [self.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0]];
        [self.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1.0]];

    }
    [self.scrlVW setDelegate:self];
     podCastObj = [arrData objectAtIndex:indexPath];
  
        self.lblBDMDate.text = [AQHelpers setFormator:[NSString stringWithFormat:@"%@",podCastObj.date]];
        self.lblBDMTitle.text = [NSString stringWithFormat:@"%@",[podCastObj.title uppercaseString]];
  
//    NSString *dictPodId =[NSString stringWithFormat:@"%ld",(long)podCastObj.id];
    self.cellProgress.progress=0;
    self.cellUrl=[NSURL URLWithString:podCastObj.url];
    [self.scrlVW setContentSize:self.vwbackground.frame.size];
    [self.scrlVW setShowsHorizontalScrollIndicator:NO];
}


- (IBAction)btnBDMCellClicked:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(clickToBDMCellButton:)]) {
        [self.delegate clickToBDMCellButton:sender.tag-1];
    }
}

- (IBAction)btnDownloadBDMClicked:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(clickToBDMDownloadButton:)]) {
        [self.delegate clickToBDMDownloadButton:sender.tag-1];
    }
}

- (IBAction)btnDeleteSelected:(UIButton *)sender
{
    
    if ([self.delegate respondsToSelector:@selector(clickToBtnDelete:)]) {
        [self.delegate clickToBtnDelete:sender.tag-1 ];
    }
    
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x<0)
    {
        [scrollView setFrame:CGRectMake(0, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height)];
        
    }
    else if (self.contentView.frame.size.width <scrollView.contentOffset.x + self.contentView.frame.size.width)
    {
        [scrollView setFrame:CGRectMake(scrollView.contentSize.width-self.vwbackground.frame.size.width, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height)];
        
    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    if ([self.delegate respondsToSelector:@selector(scrollViewEndCallBack:)]) {
        [self.delegate scrollViewEndCallBack:scrollView.tag-1];
    }
    
    [scrollView setContentOffset:CGPointMake(scrollView.contentSize.width-self.contentView.frame.size.width, scrollView.frame.origin.y)];
    
}
-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    [scrollView setContentOffset:scrollView.contentOffset animated:YES];
}


@end
