//
//  SponsorsCell.h
//  TomandDanMediocreApp
//
//  Created by techugo on 26/03/18.
//  Copyright © 2018 com.aquevix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SponsorsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCell;

@end
