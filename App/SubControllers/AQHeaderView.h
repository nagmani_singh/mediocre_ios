//
//  AQHeaderView.h
//  MediocreApp
//
//  Created by Swati Sharma on 07/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AQHeaderDelegate<NSObject>
-(void)clickToMenu;
-(void)clickToBack;
@end

@interface AQHeaderView : UIViewController{
    NSString *strHeader;
   
}
@property (strong, nonatomic) IBOutlet UIButton *btnMenu;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UIImageView *imgMenu;
@property (nonatomic, assign)id<AQHeaderDelegate>delegate;
- (IBAction)btnMenuClicked:(id)sender;
-(id)InitWithdata:(UIViewController*)controllerValue header:(NSString*)headerValue;
@property (strong, nonatomic) IBOutlet UIImageView *imgBack;
- (IBAction)btnBackClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIImageView *imglineBelow;
-(void)corporateFontAndColor;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIImageView *imglineAbove;
@property (weak, nonatomic) IBOutlet UIImageView *imgNewBack;
@property (weak, nonatomic) IBOutlet UIButton *btnNewBack;


@end
