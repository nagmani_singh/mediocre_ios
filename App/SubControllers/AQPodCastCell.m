//
//  AQPodCastCell.m
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQPodCastCell.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
@implementation AQPodCastCell

- (void)awakeFromNib {
    // Initialization code
    self.cellProgress=[[UIProgressView alloc]initWithFrame:CGRectMake(0, 0, 320, 5)];
    [self.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0]];
    [self.contentView addSubview:self.cellProgress];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)bindData:(NSMutableArray*)arrData index:(NSInteger)indexPath{
    [self setFontAndColor];
    if (arrData.count <+indexPath) {
        
    }else
    {
      if (indexPath%2!=0)
      {
          self.vwbackground.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
          self.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-2.png"];
          self.imgTomDan.image = [UIImage imageNamed:@"icon_t&d-podcast-2.png"];
          [self.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0]];
          [self.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0]];


      }
      else{
          self.vwbackground.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
          
          self.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-1.png"];
          self.imgTomDan.image = [UIImage imageNamed:@"icon_t&d-podcast-1.png"];
          [self.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0]];
          [self.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0]];
      }
       
        [self.scrlVW setDelegate:self];
    podCastObj = [arrData objectAtIndex:indexPath];
   
    self.lblPodDate.text = [AQHelpers setFormator:[NSString stringWithFormat:@"%@",podCastObj.date]];
    NSLog(@"%@",self.lblPodDate.text);
    self.lblPodTitle.text = [NSString stringWithFormat:@"%@",[podCastObj.title uppercaseString]];
        self.cellProgress.progress=0;
   //NSString *dictPodId =[NSString stringWithFormat:@"%ld",(long)podCastObj.id];
        self.cellUrl=[NSURL URLWithString:podCastObj.url];
        [self.scrlVW setContentSize:self.vwbackground.frame.size];
        [self.scrlVW setShowsHorizontalScrollIndicator:NO];

      }
    
}


-(void)bindCorporateData:(NSMutableArray*)arrData index:(NSInteger)indexPath{
    [self setFontAndColor];
    if (arrData.count <+indexPath) {
        
    }else
    {
        if (indexPath%2!=0)
        {
            self.vwbackground.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateBLACK alpha:1.0];
            self.imgDownloadpod.image = [UIImage imageNamed:@"new_ic_download-yellow.png"];
            self.imgTomDan.image = [UIImage imageNamed:@"new_img_cup.png"];

            [self.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_CorporateCellGray alpha:1.0]];
            [self.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_CorporateBLACK alpha:1.0]];
            self.lblPodTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateYELLOW alpha:1.0];
            self.lblPodDate.textColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateYELLOW alpha:1.0];

            
        }
        else
        {
            self.vwbackground.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateCellGray alpha:1.0];
            self.imgDownloadpod.image = [UIImage imageNamed:@"new_ic_download-white.png"];
            self.imgTomDan.image = [UIImage imageNamed:@"new_img_box.png"];
            [self.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_CorporateBLACK alpha:1.0]];
            [self.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_CorporateCellGray alpha:1.0]];
            self.lblPodTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateBLACK alpha:1.0];
            self.lblPodDate.textColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateBLACK alpha:1.0];
        }
        
        [self.scrlVW setDelegate:self];
        podCastObj = [arrData objectAtIndex:indexPath];
        
        self.lblPodDate.text = [AQHelpers setFormator:[NSString stringWithFormat:@"%@",podCastObj.date]];
        NSLog(@"%@",self.lblPodDate.text);
        self.lblPodTitle.text = [NSString stringWithFormat:@"%@",[podCastObj.title uppercaseString]];
        self.cellProgress.progress=0;
        //NSString *dictPodId =[NSString stringWithFormat:@"%ld",(long)podCastObj.id];
        self.cellUrl=[NSURL URLWithString:podCastObj.url];
        _cellid=podCastObj.id;
        [self.scrlVW setContentSize:self.vwbackground.frame.size];
        [self.scrlVW setShowsHorizontalScrollIndicator:NO];
        
    }
    
}




-(void)setFontAndColor
{
      self.lblPodDate.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
      self.lblPodTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
}

- (IBAction)btnPodCellClicked:(UIButton*)sender {
    
    if ([self.delegate respondsToSelector:@selector(clickToCellButton:)]) {
        [self.delegate clickToCellButton:sender.tag-1];
    }
}

- (IBAction)btnDownloadPodClicked:(UIButton*)sender {
    if ([self.delegate respondsToSelector:@selector(clickToDownloadButton:)]) {
        [self.delegate clickToDownloadButton:sender.tag-1 ];
    }
    

    
}

- (IBAction)btnDeleteSelected:(UIButton *)sender
{

    if ([self.delegate respondsToSelector:@selector(clickToBtnDelete:)]) {
        [self.delegate clickToBtnDelete:sender.tag-1 ];
    }


}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x<0)
    {
        [scrollView setFrame:CGRectMake(0, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height)];
        
        
    }
    else if (self.contentView.frame.size.width <scrollView.contentOffset.x + self.contentView.frame.size.width)
    {
        self.state=openState;
        [scrollView setFrame:CGRectMake(scrollView.contentSize.width-self.vwbackground.frame.size.width, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height)];

    }
}




- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    if ([self.delegate respondsToSelector:@selector(scrollViewEndCallBack:)]) {
        [self.delegate scrollViewEndCallBack:scrollView.tag-1];
    }
    
    
        self.state=openState;
    [scrollView setContentOffset:CGPointMake(scrollView.contentSize.width-self.contentView.frame.size.width, scrollView.frame.origin.y)];
    
 
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    [scrollView setContentOffset:scrollView.contentOffset animated:YES];
}

@end
