//
//  AQMenuCell.h
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AQMenuDelegate<NSObject>
-(void)clickToCellButton:(NSInteger)tagValue;
@end

@interface AQMenuCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnCell;
- (IBAction)btnCellclicked:(id)sender;
@property (nonatomic, assign)id<AQMenuDelegate>delegate;

@end
