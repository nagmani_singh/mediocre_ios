//
//  AQBDMCell.h
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AppAPI/PodCasts.h>
@protocol AQBDMDelegate<NSObject>
-(void)clickToBDMCellButton:(NSInteger)index;
-(void)clickToBDMDownloadButton:(NSInteger)index;
-(void)clickToBtnDelete:(NSInteger)index;
-(void)scrollViewEndCallBack:(NSInteger)index;

@end

@interface AQBDMCell : UITableViewCell<UIScrollViewDelegate>
{
    NSMutableArray *arrDownloadIds;
    PodCasts *podCastObj;
}
@property (strong, nonatomic) IBOutlet UIImageView *imgBDM;
@property (strong, nonatomic) IBOutlet UILabel *lblBDMTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblBDMDate;
- (IBAction)btnBDMCellClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgDownloadpod;
- (IBAction)btnDownloadBDMClicked:(id)sender;
-(void)bindData:(NSMutableArray*)arrData index:(NSInteger)indexPath;
@property (strong, nonatomic) IBOutlet UIView *vwbackground;
@property (nonatomic, assign)id<AQBDMDelegate>delegate;
@property (strong, nonatomic) IBOutlet UIButton *btnDownLoad;
@property (strong, nonatomic) IBOutlet UIButton *btnCell;
@property (strong, nonatomic)  UIProgressView *cellProgress;
@property (strong, nonatomic)  NSURL *cellUrl;
@property (strong,nonatomic) IBOutlet UIScrollView *scrlVW;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgDelete;
-(void)closeScrollview:(UIScrollView *)scrollView;
@property  BOOL *isDownloading;


@end
