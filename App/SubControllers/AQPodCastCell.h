//
//  AQPodCastCell.h
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AppAPI/PodCasts.h>

@protocol AQPodCastDelegate<NSObject>
-(void)clickToCellButton:(NSInteger)index;
-(void)clickToDownloadButton:(NSInteger)index ;
-(void)clickToBtnDelete:(NSInteger)index;
-(void)scrollViewEndCallBack:(NSInteger)index;
@end

@interface AQPodCastCell : UITableViewCell<UIScrollViewDelegate>
{
    NSMutableArray *arrDownloadIds;
    PodCasts *podCastObj;
}
@property ( nonatomic)  enum
{
    openState=1,closedState
} state;
@property (weak, nonatomic) IBOutlet UIImageView *imgDelete;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (strong, nonatomic) IBOutlet UIImageView *imgTomDan;
@property (strong, nonatomic) IBOutlet UILabel *lblPodTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblPodDate;
- (IBAction)btnPodCellClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgDownloadpod;
- (IBAction)btnDownloadPodClicked:(id)sender;
-(void)bindData:(NSMutableArray*)arrData index:(NSInteger)indexPath;
@property (strong, nonatomic) IBOutlet UIView *vwbackground;
@property (nonatomic, assign)id<AQPodCastDelegate>delegate;
@property (strong, nonatomic) IBOutlet UIButton *btnDownload;
@property (strong, nonatomic) IBOutlet UIButton *btnCell;
@property (strong, nonatomic)  UIProgressView *cellProgress;
@property (strong, nonatomic)  NSURL *cellUrl;
@property ( nonatomic)  long cellid;
@property (strong,nonatomic) IBOutlet UIScrollView *scrlVW;

@property  BOOL *isDownloading;

-(void)closeScrollview:(UIScrollView *)scrollView;
-(void)bindCorporateData:(NSMutableArray*)arrData index:(NSInteger)indexPath;
@end
