//
//  AQPlayerMnager.m
//  TomandDanMediocreApp
//
//  Created by Pankaj Kumar on 05/08/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import "AQPlayerMnager.h"


@implementation AQPlayerMnager
+ (id)sharedManager
{
    static AQPlayerMnager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


-(void)setPlayerObj:(AVPlayer *)ply withPodcast:(PodCasts *)podC
{
    if (self.player!=nil)
    {
        AVPlayerItem *currentItem = self.player.currentItem;
        
        NSTimeInterval currentTime = CMTimeGetSeconds(currentItem.currentTime);
        _oldPodcast.lastTimePlayed=currentTime;
        [self savingPod:_oldPodcast];

        [self.player pause];
        self.player=nil;

        [self.player.currentItem removeObserver:self forKeyPath:@"status"];
        [self.player.currentItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
        [self.player.currentItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];

    }
    self.player=ply;
    if (podC!=nil) {
        _oldPodcast=podC;

    }
}

-(void)savingPod:(PodCasts *)podcastOld
{
    [[AQSavedPodcastManager sharedManager]saveOrUpdatePodcast:podcastOld];

}

-(void)savinglastPodOnTermination
{
    if (self.player!=nil)
    {
   
    AVPlayerItem *currentItem = self.player.currentItem;
    
    NSTimeInterval currentTime = CMTimeGetSeconds(currentItem.currentTime);
    _oldPodcast.lastTimePlayed=currentTime;
    [self savingPod:_oldPodcast];
         }
}


@end
