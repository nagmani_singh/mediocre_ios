//
//  AQEventTableCell.m
//  TomandDanMediocreApp
//
//  Created by Pankaj Kumar on 27/10/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import "AQEventTableCell.h"
#import "AQConfig.h"
#import "AQHelpers.h"

@implementation AQEventTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)bindCellwithData:(Events *)events withIndex:(long)index
{
    if (![events.eventType isEqualToString:@"premium"]) {
        if (index%2==0)
        {
            _imgLeft.image=[UIImage imageNamed:@"ic_calender_1.png"];
            self.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
            self.lblTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
            _imgRight.image=[UIImage imageNamed:@"icon_t&d-podcast-2.png"];

        }else
        {
            _imgLeft.image=[UIImage imageNamed:@"ic_calender_2.png"];
            self.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
            self.lblTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
            _imgRight.image=[UIImage imageNamed:@"icon_t&d-podcast-1.png"];        }
    }else
    {
        if (index%2==0)
        {
            _imgLeft.image=[UIImage imageNamed:@"ic_calender_1.png"];
            self.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
            self.lblTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
            _imgRight.image=[UIImage imageNamed:@"icon_bdm-2.png"];
            
        }else
        {
            _imgLeft.image=[UIImage imageNamed:@"ic_calender_2.png"];
            self.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
            self.lblTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
            _imgRight.image=[UIImage imageNamed:@"icon_bdm-1.png"];
        }
    }
  
    self.lblTitle.text=events.EventTitle;
}


@end
