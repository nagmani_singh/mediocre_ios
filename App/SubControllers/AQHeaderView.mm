//
//  AQHeaderView.m
//  MediocreApp
//
//  Created by Swati Sharma on 07/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQHeaderView.h"
#import "AQHelpers.h"
#import "AQConfig.h"
@interface AQHeaderView ()
{
     UIViewController *controller;
}
@end

@implementation AQHeaderView
-(id)InitWithdata:(UIViewController*)controllerValue header:(NSString*)headerValue
{
    strHeader = headerValue;
    controller = controllerValue;
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional sç√f  etup after loading the view from its nib.
    self.lblHeader.text =  strHeader;
    [self.lblHeader setAdjustsFontSizeToFitWidth:YES];
    [self setFontAndColor];
}

-(void)setFontAndColor
{
    self.view.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblHeader.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.btnMenu.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
}

-(void)corporateFontAndColor
{
    self.view.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateYELLOW alpha:1.0];
    self.lblHeader.textColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateBLACK alpha:1.0];
    self.btnMenu.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateBLACK alpha:1.0];
    [self.imgMenu setImage:[UIImage imageNamed:@"new_ic_menu.png"]];
    [self.imgLogo setImage:[UIImage imageNamed:@"new_img_skull.png"]];
    [self.imglineAbove setImage:[UIImage imageNamed:@"new_ic_line.png"]];
    [self.imglineBelow setImage:[UIImage imageNamed:@"new_ic_line.png"]];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnMenuClicked:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(clickToMenu)]) {
        [self.delegate clickToMenu];
    }
}

- (IBAction)btnBackClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(clickToBack)]) {
        [self.delegate clickToBack];
    }
}
@end
