//
//  AQMenuCell.m
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQMenuCell.h"

@implementation AQMenuCell

- (void)awakeFromNib {
    // Initialization code
    [self.lblTitle setAdjustsFontSizeToFitWidth:YES];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnCellclicked:(UIButton*)sender {
    if ([self.delegate respondsToSelector:@selector(clickToCellButton:)]) {
        [self.delegate clickToCellButton:sender.tag-1];
    }
}
@end
