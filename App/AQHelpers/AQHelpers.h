//
//  AQHelpers.h
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AQHelpers : NSObject
+(void)archieveAndSaveObjectWithData:(NSMutableArray *)dic;
+(NSMutableArray *)unarchieveAndRetrieveObjectWithData;
+(void)setScrollVwContentSize:(UIScrollView *)scrvw;
+(NSDate *) convertStringToDate:(NSString*)strDate;
+(void)trimTextToRemoveSpace:(UITextView*)txtview;
+(NSString*)setFormator:(NSString*)date;
+(NSString*)checkPartialUrlAndAdd:(NSString*)passString;
+(UIImage*)resizeImage:(UIImage*)originalImage;
+(BOOL)IsNull:(NSString*)text;
+(BOOL)isNetworkAvailable;
+(void)setImageInBackground:(UIImageView*)img Fromurl:(NSString*)url;
+(BOOL)isValidEmail:(NSString *)checkString;
+(void)OpenWebWithUrl:(NSString*)Url;
+(void)saveStringInDefaultKey:(NSString*)vkey value:(NSString*)value;
+(NSString*)fetchStringFromDefaultsKey:(NSString*)vkey;
+(void)saveBoolInDefaultsKey:(NSString*)key state:(BOOL)state;
+(BOOL)fetchBoolForkey:(NSString*)key;
+(void)disableAllEventTouch;
+(void)enableAllEventTouch;
+(BOOL)getBoolFromString:(NSString*)vString;
+(NSString*)getStringFromBool:(BOOL)vBool;
+ (NSString *)getCurrentAppVersion;
+ (NSString *)getCurrentBuildNumber;
+(NSString *)timeSincePosted:(NSDate *)postedDate;
+(NSDate *) toLocalTime:(NSString*)strDate;
+(NSDate *) toGlobalTime:(NSString*)strDate;
+(NSString*)getTimeDifferenceFromDate:(NSDate*)nxtDate date:(NSDate*)currentDate;
+(NSDate *)firstDateOfWeekWithDate:(NSDate *)date;
+(NSDate*)lastDateOfWeekWithDate:(NSDate*)date;
+(void)showActivtyIndicator:(UIViewController *)cntrl;
+(void)hideActivityIndicator:(UIViewController *)cntrl;
// Player time Interverl Time
+(NSString *)stringFromTimeInterval:(NSTimeInterval)interval;
+(NSString *)setweekDayFromDateForServerFormat:(NSString *)strDate;

//+(BOOL)isInvalidAuthKeyErrorWith:(NSString*)errStr;
//+(void)setLoginScreenCenterController:(MMDrawerController*)cntrl;
@end
@interface NSArray (search)
+(NSArray*) filterByPredicate:(NSString*)filterStr fullArray:(NSMutableArray*)arr objectName:(NSString*)object;
+(NSArray*) filterByPredicate:(NSString*)filterStr fullArray:(NSMutableArray*)arr;
+(NSArray*)filterArrayInAscending:(NSArray*)fullArray;
+(NSMutableArray*)sortArrayAlphaBaticalyFor:(NSString*)string withArray:(NSMutableArray*)globlArray;
@end
@interface UIColor (AQHelpers)
+ (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha;
@end


// UIButton Type Extention
@interface UIUnderlinedButton : UIButton {
    
    UIColor   *borderColor;
    
}
- (UIUnderlinedButton*) underlinedButtonWithColor:(UIColor*)lineColor;
@end

@interface NSDate (AQDate)
-(NSDate *) toLocalTime;
-(NSDate *) toGlobalTime;
@end

