//
//  AQConfig.h
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//


//#ifndef News_Junkie_AQConfig_h
//#define News_Junkie_AQConfig_h

#ifndef AQCONFIG__h
#define AQCONFIG__h

#if  AQUEVIXDEV
#include "AQConfig_AQUEVIXDEV.h"
#elif   CUSTOMERDEV
#include "AQConfig_CUSTOMERDEV.h"
#elif   CUSTOMERLIVE
#include "AQConfig_CUSTOMERLIVE.h"
#else
#include "AQConfig_DEVELOPER.h"
#endif



////Live URL
//#define SERVERAPIBASEURL            @"http://mediocre.dev.aquevix.com"
//#define SERVERAPIMEDILEURL          @"/ApiController.php?action="



// DEV URL
//#define SERVERAPIBASEURL            @"http://192.168.47.66"
//#define SERVERAPIMEDILEURL          @"/~shashankgupta/mediocre/ApiController.php?action="

#define AUTHKEY                     @"Authkey"
#define ISUSERLOGGEDIN              @"IsUserLoggedIn"
#define ISUSERACTIVE                @"IsUserActive"

#define LOGINEMAILID      @"LoginEmailId"
#define APPKEY                              @"aa6bd77d241f430c90955c29f9878fe7"
#define APPVERSION                          @"1.0"
#define DEVICETYPE                          @"iPhone"
#define ERROR_EMAIL_CONFIGURE               @"Please add email account in device setting."
     
// Demo Live url
//#define AUDIOLIVESTREAMINGURL               @"http://dl.blugaa.com/lq.blugaa.com/cdn6/4363c6fd66f12d39596af93f63da2710/awzsv/Laden-(JATTZONE.cOM).mp3"
#define AUDIOLIVESTREAMINGURL               @"http://live.tomanddan.com:8000/listen.m3u"



// Live url
//#define AUDIOLIVESTREAMINGURL               @"http://live.tomanddan.com:8000/listen.m3u"

#define URLFORREGISTER                       @"https://tomanddan.com/registration"
#define PHONENUMBER                          @"18448666326"

#define ADSARRAY           @"AdsArray"
#define COLOR_DARK_RED                      @"#891A1C"
#define COLOR_LIGHT_RED                     @"#D61D24"
#define COLOR_LIGHT_YELLOW                  @"#FAAE17"
#define COLOR_DARK_YELLOW                   @"#E2921E"
#define COLOR_CorporateYELLOW               @"#f6a704"
#define COLOR_CorporateBLACK                @"#0a0405"
#define COLOR_CorporateCellGray                @"#d1d2d4"

#define AQAlertView(title,msg) [[[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show]

#endif
