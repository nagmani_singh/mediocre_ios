//
//  AQHelpers.m
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQHelpers.h"
#import <UIKit/UIKit.h>
#import <Reachability.h>
#import "AQConfig.h"
@implementation AQHelpers

+(void)trimTextToRemoveSpace:(UITextView*)txtview
{
    NSString *rawString = [txtview text];
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    txtview.text=[rawString stringByTrimmingCharactersInSet:whitespace];
}

+(NSString*)setFormator:(NSString*)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE,dd MMM yyyy HH:mm:ss ZZZZ"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    NSDate *dateFormatted = [dateFormatter dateFromString:date];
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"MM.dd.yyyy"];
    
    dateFormatter1.timeZone = [NSTimeZone localTimeZone];
    NSString *dateString = [dateFormatter1 stringFromDate:dateFormatted];
    return dateString;
}
//+(NSString*)checkPartialUrlAndAdd:(NSString*)passString
//{
//    NSString *returnString;
//    if ([passString rangeOfString:@".com/"].location == NSNotFound) {
//        NSLog(@"string does not contain .com/");
//        returnString=[NSString stringWithFormat:@"%@/%@",SERVERURLFORIMAGE,passString];
//    } else {
//        NSLog(@"string contains .com/!");
//        returnString=passString;
//    }
//    return returnString;
//    
//}

+(UIImage*)resizeImage:(UIImage*)originalImage
{
    CGRect rect = CGRectMake(0,0,800,800);
    UIGraphicsBeginImageContext( rect.size );
    [originalImage drawInRect:rect];
    UIImage *picture = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return picture;
}



+(BOOL)IsNull:(NSString*)text{
    
    if ([text isEqual:[NSNull null]] || [text isEqualToString:@""] || [text isEqualToString:@"(null)"] || [text isEqualToString:@"<null>"] || [text isEqualToString:@"null"] || text==nil || [text isEqualToString:@"nil"] || [text isEqualToString:@"<nil>"]  || [text isEqual:[NSNull class]]) {
        return YES;
    }else{
        return NO;
    }
}
+(BOOL)isNetworkAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        return false;
    } else {
        NSLog(@"There IS internet connection");
        return true;
    }
}
+(void)setImageInBackground:(UIImageView*)img Fromurl:(NSString*)url
{
    if (url.length>0) {
        NSURL *imageURL = [NSURL URLWithString:url];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                img.image = [UIImage imageWithData:imageData];
            });
        });
  
    }
    
}

+(void)archieveAndSaveObjectWithData:(NSMutableArray *)dic{
    NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:dic];
     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arr];
    if (data==nil) {
    }
    else
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:data forKey:ADSARRAY];
        [defaults synchronize];
    }
}

+(NSMutableArray *)unarchieveAndRetrieveObjectWithData
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:ADSARRAY];
    NSLog(@"%@", [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys]);
    NSMutableArray *dic=[[NSMutableArray alloc]init];
    if (data==nil)
    {
         return dic;
    }
    else
    {
    dic = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return dic;

    }
   
  }

+(BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(void)showActivtyIndicator:(UIViewController *)cntrl{
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicator setColor:[UIColor yellowColor]];
    indicator.center = CGPointMake(cntrl.view.frame.size.width / 2.0, cntrl.view.frame.size.height / 2.0);
    [cntrl.view addSubview:indicator];
    [indicator startAnimating];
}

+(void)hideActivityIndicator:(UIViewController *)cntrl{
    for (UIActivityIndicatorView *indicator in cntrl.view.subviews){
        if ([indicator isMemberOfClass:[UIActivityIndicatorView class]]){
            [indicator stopAnimating];
            
        }
    }
    
}
+(void)saveStringInDefaultKey:(NSString*)vkey value:(NSString*)value
{
    [[NSUserDefaults standardUserDefaults]setValue:value forKey:vkey];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
+(NSString*)fetchStringFromDefaultsKey:(NSString*)vkey
{
    return [[NSUserDefaults standardUserDefaults]valueForKey:vkey];
}

+(void)saveBoolInDefaultsKey:(NSString*)key state:(BOOL)state
{
    [[NSUserDefaults standardUserDefaults]setBool:state forKey:key];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
+(BOOL)fetchBoolForkey:(NSString*)key
{
    return [[NSUserDefaults standardUserDefaults]boolForKey:key];
}

+(void)disableAllEventTouch
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
}

+(void)enableAllEventTouch
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
}

+(BOOL)getBoolFromString:(NSString*)vString
{
    if ([vString isEqualToString:@"true"]) return true;
    else return false;
}

+(NSString*)getStringFromBool:(BOOL)vBool
{  if (vBool==YES) return @"true";
else return @"false";
}

+(void)OpenWebWithUrl:(NSString*)Url
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:Url]];
}

+ (NSString *)getCurrentAppVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

+ (NSString *)getCurrentBuildNumber
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}



+(NSString *)timeSincePosted:(NSDate *)postedDate
{
    
    double ti = [postedDate timeIntervalSinceNow];
    ti = fabs(ti);
    if (ti<60) {
        return  [NSString stringWithFormat:@"Just now"];
    }
    else if (ti < 3600) {
        int diff = round(ti / 60);
        return [NSString stringWithFormat:@"%d Mins ago", diff];
    } else if (ti < 86400) {
        int diff = round(ti / 60 / 60);
        return[NSString stringWithFormat:@"%d Hours ago", diff];
    }
    else if (ti < 2592000){
        int diff = round(ti / 60 / 60 / 24);
        return[NSString stringWithFormat:@"%d days ago", diff];
    }
    else if (ti < 31536000) {
        int diff = round(ti / 60 / 60 / 24 / 30);
        return [NSString stringWithFormat:@"%d Months ago", diff];
    } else {
        int diff = round(ti / 60 / 60 / 24 / 30 / 12);
        return [NSString stringWithFormat:@"%d Years ago", diff];
    }
}

+(void)setScrollVwContentSize:(UIScrollView *)scrvw
{
    CGFloat scrollVwHeight=0.0;
    for (UIView *view in scrvw.subviews)
    {
        if (!view.hidden)
        {
            CGFloat origin = view.frame.origin.y;
            CGFloat height = view.frame.size.height;
            if (origin + height > scrollVwHeight) {
                scrollVwHeight = origin + height;
            }
            
        }
    }
    scrvw.contentSize = CGSizeMake(scrvw.frame.size.width,scrollVwHeight);
    NSLog(@"%f",scrvw.contentSize.height);
}

+(NSString*)getTimeDifferenceFromDate:(NSDate*)nxtDate date:(NSDate*)currentDate
{
  //  NSDate * now = [NSDate date];
    NSString *scale;
     int timeAgo = [nxtDate timeIntervalSinceDate:currentDate];
    if (timeAgo<60) {
        scale = @"Just now";
        return scale;
    }
    else{
        
        int diffHours = timeAgo / (60 * 60)%24;
        
        int diffDays = timeAgo / (60 * 60 * 24);
        
        int diffMins = timeAgo/60;
        
        NSString *strTime=[NSString stringWithFormat:@"%d %d %d",diffDays,diffHours,diffMins];
        return strTime;
    }
}


+(NSDate *) toLocalTime:(NSString*)strDate
{
    NSDateFormatter *dFormatter=[[NSDateFormatter alloc]init];
    [dFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"];
    NSDate *dte=[dFormatter dateFromString:strDate];
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: dte];
    return [NSDate dateWithTimeInterval: seconds sinceDate: dte];
}

+(NSDate *) convertStringToDate:(NSString*)strDate
{
    NSDateFormatter *dFormatter=[[NSDateFormatter alloc]init];
    [dFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dte=[dFormatter dateFromString:strDate];
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: dte];
    return [NSDate dateWithTimeInterval: seconds sinceDate: dte];
}

+(NSString *)setweekDayFromDateForServerFormat:(NSString *)strDate{

       NSDateFormatter *formtt = [[NSDateFormatter alloc]init];
    [formtt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formtt setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *date2=[formtt dateFromString:strDate];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger units = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit;
    NSDateComponents *components = [calendar components:units fromDate:date2];
    NSDateFormatter *weekDay = [[NSDateFormatter alloc] init];
    [weekDay setDateFormat:@"EEEE"];
    return [weekDay stringFromDate:date2];

}

+(NSDate *) toGlobalTime:(NSString*)strDate
{
    NSDateFormatter *dFormatter=[[NSDateFormatter alloc]init];
    [dFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"];
    NSDate *dte=[dFormatter dateFromString:strDate];
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: dte];
    return [NSDate dateWithTimeInterval: seconds sinceDate: dte];
}


+(NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

+(NSDate *)firstDateOfWeekWithDate:(NSDate *)date {
    
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setLocale:[NSLocale currentLocale]];
    
    NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:date];
    
    [nowComponents setWeekday:6]; //friday
    [nowComponents setWeek: [nowComponents week]]; //Next week
    [nowComponents setHour:0]; //8a.m.
    [nowComponents setMinute:0];
    [nowComponents setSecond:0];
    
    NSDate *beginningOfWeek = [gregorian dateFromComponents:nowComponents];
    
    return beginningOfWeek;
}

+(NSDate*)lastDateOfWeekWithDate:(NSDate*)date
{
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setLocale:[NSLocale currentLocale]];
    
    NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:today];
    
    [nowComponents setWeekday:2]; //Monday
    [nowComponents setWeek: [nowComponents week]+1]; //Next week
    [nowComponents setHour:0]; //8a.m.
    [nowComponents setMinute:0];
    [nowComponents setSecond:-2];
    
    NSDate *endOfWeek = [gregorian dateFromComponents:nowComponents];
    return endOfWeek;
}

@end
@implementation NSArray (search)

+(NSArray*) filterByPredicate:(NSString*)filterStr fullArray:(NSMutableArray*)arr objectName:(NSString*)object{
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"SELF.%@ contains[cd] %@",object,filterStr];
    NSArray *array= [arr filteredArrayUsingPredicate:predicate];
    return array;
}

+(NSArray*) filterByPredicate:(NSString*)filterStr fullArray:(NSMutableArray*)arr {
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"SELF contains[cd] %@",filterStr];
    NSArray *array= [arr filteredArrayUsingPredicate:predicate];
    return array;
}

+(NSArray*)filterArrayInAscending:(NSArray*)fullArray
{
    NSArray *sorted = [fullArray sortedArrayUsingComparator:^(id obj1, id obj2){
        if ([obj1 isKindOfClass:[NSNumber class]] && [obj2 isKindOfClass:[NSNumber class]]) {
            if (obj1 > obj2) {
                return (NSComparisonResult)NSOrderedDescending;
            }     else if (obj1 < obj2) {
                return (NSComparisonResult)NSOrderedAscending;
            }
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    
    return sorted;
}
+(NSMutableArray*)sortArrayAlphaBaticalyFor:(NSString*)string withArray:(NSMutableArray*)globlArray
{
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:string ascending:YES selector:@selector(caseInsensitiveCompare:)];
    return [[globlArray sortedArrayUsingDescriptors:@[sort]]mutableCopy];
    
}
@end

@implementation UIColor (AQHelpers)
//Function For Hex Color Use
+ (unsigned int)intFromHexString:(NSString *)hexStr
{
    unsigned int hexInt = 0;
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}

+ (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    // Convert hex string to an integer
    unsigned int hexint = [self intFromHexString:hexStr];
    
    // Create color object, specifying alpha as well
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    
    return color;
}
@end

// UIButton Extention
@implementation UIUnderlinedButton

- (UIUnderlinedButton*) underlinedButtonWithColor:(UIColor *)lineColor {
    UIUnderlinedButton* button = [[UIUnderlinedButton alloc] init];
    borderColor=lineColor;
    return button;
}

- (void) drawRect:(CGRect)rect {
    CGRect textRect = self.titleLabel.frame;
    
    // need to put the line at top of descenders (negative value)
    CGFloat descender = self.titleLabel.font.descender+2;
    
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    // set to same colour as text
    CGContextSetStrokeColorWithColor(contextRef, borderColor.CGColor);
    
    CGContextMoveToPoint(contextRef, textRect.origin.x, textRect.origin.y + textRect.size.height + descender);
    
    CGContextAddLineToPoint(contextRef, textRect.origin.x + textRect.size.width, textRect.origin.y + textRect.size.height + descender);
    
    CGContextClosePath(contextRef);
    
    CGContextDrawPath(contextRef, kCGPathStroke);
}
@end
@implementation NSDate (AQDate)

-(NSDate *) toLocalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

-(NSDate *) toGlobalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}


@end


