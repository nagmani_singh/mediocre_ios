//
//  BDMDownload.h
//  MediocreApp
//
//  Created by Pankaj Kumar on 12/04/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@protocol podCastsDelegate <NSObject>
-(void)downloadedPrograssPodcast :(float)receivedata withExpectedData:(float)expectedData withCellID:(int)cellId withClass:(UIViewController *)isclass withURL:(NSURL *)url;

-(void)downloadFailedWithError:(int)index;

-(void)afterTaskComplete:(int)index withURL:(NSURL *)url;

@end

@interface BDMDownload : NSObject<NSURLSessionDelegate,NSURLSessionDataDelegate>

// Properties for this data object

@property NSInteger id;
@property (nonatomic,strong) NSString * date;
@property (nonatomic,strong) NSString * url;
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * type;
@property (nonatomic,strong) NSMutableArray * allPodcastArray;
@property (strong)id<podCastsDelegate>delegate;

@property (assign, nonatomic) UIBackgroundTaskIdentifier backgroundUpdateTask;

enum stateType : NSUInteger
{
    NotDownloaded,
    Downloading,
    Downloaded
} ;
@property (nonatomic) enum stateType downloadState;

// Parse method to load data
-(id) parse:(NSDictionary *) dict;
+ (BDMDownload*)sharedInstance;
-(void)downloadAudioStream:(NSURL *)aurl :(int)cellID
;
-(void)downloadedPrograss :(float)receivedata withExpectedData:(float)expectedData withCellID:(int)cellId withClass:(UIViewController *)isclass withURL:(NSURL *)url;
-(void)afterTaskComplete:(int)index withURL:(NSURL *)url;
@end
