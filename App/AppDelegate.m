
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
//	Part of Aquevix App Starter Project. Contains code copyrighted under MIT
//	license.
//
///////////////////////////////////////////////////////////////////////////////

//
//  AQAppDelegate.m
//  App
//
//  Created by  velidibharatdwaj on 12/09/13.
//  Copyright (c) 2013 com.aquevix. All rights reserved.
//

#import "AppDelegate.h"
//#import <SHKConfiguration.h>
//#import <SHKFacebook.h>
#import "AQPlayerMnager.h"
#import "AQSplashScreen.h"
#import "AQHomeScreen.h"
#import "AQMenuScreen.h"
#import "AQConfig.h"
#import <OneSignal/OneSignal.h>
#import "AQSavedPodcastManager.h"
@implementation AppDelegate
@synthesize navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    _openPodcastCellArray =[[NSMutableArray alloc]init];
    
    [self settingAppKey:APPKEY version:APPVERSION deviceType:DEVICETYPE];
//    9533e8a2-c4cb-48b5-a895-3361049ed245
    self.oneSignal = [[OneSignal alloc] initWithLaunchOptions:launchOptions
                      appId:@"7e29efd0-8223-4e3f-8a40-61e0a7016bf1"
                                           handleNotification:nil autoRegister:true];
    
    [self.oneSignal enableInAppAlertNotification:true];

    AQSplashScreen *splash = [[AQSplashScreen alloc]initWithNibName:@"AQSplashScreen" bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:splash];
    [self.window setRootViewController:nav];
    [nav setNavigationBarHidden:YES];
    [self.window makeKeyAndVisible];
    [[AQSavedPodcastManager sharedManager]getUserData];
    return YES;
}

-(void)setMMDrawerController
{
    AQMenuScreen *navMenu=[[AQMenuScreen alloc]initWithNibName:@"AQMenuScreen" bundle:nil];
    AQHomeScreen *homeScreen=[[AQHomeScreen alloc]initWithNibName:@"AQHomeScreen" bundle:nil];
    self.drawerObj=[[MMDrawerController alloc]initWithCenterViewController:homeScreen leftDrawerViewController:navMenu];
    self.navigationController=[[UINavigationController alloc]initWithRootViewController:self.drawerObj];
    
    // To enable right side view..............
    self.drawerObj.centerHiddenInteractionMode = MMDrawerOpenCenterInteractionModeNone;
    self.drawerObj.openDrawerGestureModeMask = MMOpenDrawerGestureModeAll;
    self.drawerObj.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;
    // To enable right side view..............
    
    [self.drawerObj setShowsShadow:NO];
    [self.navigationController setNavigationBarHidden:YES];
    [self.drawerObj setMaximumLeftDrawerWidth:0.1];
}

-(void) initAppSettings {
    
}

-(BOOL) application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    NSString* scheme = [url scheme];
    
//    if ([scheme hasPrefix:[NSString stringWithFormat:@"fb%@", SHKCONFIG(facebookAppId)]]) {
//       // return [SHKFacebook handleOpenURL:url];
//    }
    
    return YES;
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return true;
}
/*
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    //your code execution will here.
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reminder"
                                                        message:userInfo
                                                       delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    
}
*/

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[AQPlayerMnager sharedManager]savinglastPodOnTermination];
    [[AQSavedPodcastManager sharedManager]savingPodcastOnTermination];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[AQSavedPodcastManager sharedManager]getUserData];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    
}

@end
