#import "PodCastsDownload.h"
#import <UIKit/UIKit.h>
#import "AQBDMScreen.h"


@implementation PodCastsDownload
{
    int64_t *receivedData;
    long expectedBytes;
    NSURLSessionConfiguration *sessionConfiguration;
}
// Synthesize the members here.
@synthesize id;
@synthesize date;
@synthesize url;
@synthesize title;
@synthesize delegate;
// Parse event to load data into self from dictionary.

+ (PodCasts*)sharedInstance
{
    static dispatch_once_t once;
    static PodCasts *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        
           });
    return sharedInstance;
}



-(void)setPodArray:(NSMutableArray *)array
{
    self.allPodcastArray=array;
}


-(void)downloadedPrograss :(float)receivedata withExpectedData:(float)expectedData withCellID:(int)cellId withClass:(UIViewController *)isclass withURL:(NSURL *)url
{
    
     if ([self.delegate respondsToSelector:@selector(downloadedPrograssPodcast:withExpectedData:withCellID:withClass:withURL:)])
    {
          [self.delegate downloadedPrograssPodcast:receivedata withExpectedData:expectedData withCellID:cellId withClass:isclass withURL:url];
    }
    
  
    
}






-(void)downloadAudioStream:(NSURL *)aurl :(int)cellID
{
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:aurl     cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
   // NSMutableData *receivedData = [[NSMutableData alloc] initWithLength:0];
//    NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self startImmediately:YES];
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfiguration.HTTPMaximumConnectionsPerHost = 8;
//        sessionConfiguration.timeoutIntervalForResource = 120;
//        sessionConfiguration.timeoutIntervalForRequest = 120;
    });
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
   NSURLSessionDownloadTask *downloadTasks= [session downloadTaskWithRequest:theRequest];
    
    downloadTasks.taskDescription=[NSString stringWithFormat:@"%d",cellID];
//    downloadTasks.priority=0;
    [self beginBackgroundUpdateTask];
    [downloadTasks resume];
    
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        //[self beginBackgroundUpdateTask];
    
    [self downloadedPrograss:(float)totalBytesWritten withExpectedData:(float)totalBytesExpectedToWrite withCellID:downloadTask.taskDescription.integerValue withClass:self.delegate withURL:[[downloadTask originalRequest] URL] ];
        //[self endBackgroundUpdateTask];
//    });
    

}



//-(void)changeCellIdOnarrChangeWithURL:(NSURL *)url
//{
//    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"SELF.url contains[c] %@",url];
//   
//    
//}



- (void) beginBackgroundUpdateTask
{
    self.backgroundUpdateTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [self endBackgroundUpdateTask];
    }];
}

- (void) endBackgroundUpdateTask
{
    [[UIApplication sharedApplication] endBackgroundTask: self.backgroundUpdateTask];
    self.backgroundUpdateTask = UIBackgroundTaskInvalid;
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSLog(@"%@",location);

    NSString *documentsPath    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *finalPath        = [documentsPath stringByAppendingPathComponent:[[[downloadTask originalRequest] URL] lastPathComponent]];
    NSLog(@"%@",finalPath);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL success;
    NSError *error;
    if ([fileManager fileExistsAtPath:finalPath])
    {
        success = [fileManager removeItemAtPath:finalPath error:&error];
        NSAssert(success, @"removeItemAtPath error: %@", error);
    }
    
    success = [fileManager copyItemAtURL:location toURL:[NSURL fileURLWithPath:finalPath] error:&error];
    NSAssert(success, @"moveItemAtURL error: %@", error);
    if (success==YES)
    {
        
        [self afterTaskComplete:downloadTask.taskDescription.integerValue withURL:[[downloadTask originalRequest] URL]];
    }
    
    
    
    
    
}


-(void)afterTaskComplete:(int)index withURL:(NSURL *)url
{
    if ([self.delegate respondsToSelector:@selector(afterTaskComplete:withURL:)])
    {
        [self.delegate afterTaskComplete:index withURL:url];
    }
}

-(void)downloadFailedWithError:(int)index
{
    if ([self.delegate respondsToSelector:@selector(downloadFailedWithError:)])
    {
        [self.delegate downloadFailedWithError:index];
    }
}


- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (error)
    {
        [self downloadFailedWithError:task.taskDescription.integerValue];
        dispatch_async(dispatch_get_main_queue(),
                       ^{
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:error.localizedFailureReason message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        });
        
       
        
    }
}



-(id) parse:(NSDictionary *) dict
{
    self.id=[[dict objectForKey:@"id"]integerValue];
    self.date=[dict objectForKey:@"PostedOn"];
    self.url=[dict objectForKey:@"Url"];
    self.title=[dict objectForKey:@"Title"];
    self.type=[dict objectForKey:@"Type"];

return self;
}
@end