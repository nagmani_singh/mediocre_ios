//
//  AQEventDetailScreen.h
//  TomandDanMediocreApp
//
//  Created by Pankaj Kumar on 26/10/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AppAPI/Events.h>

@interface AQEventDetailScreen : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblEventTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDateTime;
@property (weak, nonatomic) IBOutlet UILabel *lblCompany;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIWebView *webVW;
@property (strong,nonatomic) Events *eventsObj;
@end
