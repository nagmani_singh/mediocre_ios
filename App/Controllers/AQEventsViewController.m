//
//  AQEventsViewController.m
//  TomandDanMediocreApp
//
//  Created by Pankaj Kumar on 26/10/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import "AQEventsViewController.h"
#import "AQHelpers.h"
#import "AQHeaderView.h"
#import "AppDelegate.h"
#import "AQConfig.h"
#import <AppAPI/AQGetEventsResponse.h>
#import <AppAPI/AQGetEventsRequest.h>
#import <AppAPI/AQDataManager.h>
#import "AQEventTableCell.h"
#import "AQEventDetailScreen.h"
@interface AQEventsViewController () <AQHeaderDelegate>
{
   AQHeaderView *header;
       AppDelegate *appObj;
    BOOL isIntialLoad;
    NSMutableArray *arrEvents;
     NSInteger pageCount;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UITableView *tblEvents;
@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UIView *vwFooter;
@property (strong, nonatomic) IBOutlet UILabel *lblfooterTom;
@property (strong, nonatomic) IBOutlet UILabel *lblfooterdan;

@end

@implementation AQEventsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appObj=[UIApplication sharedApplication].delegate ;
    isIntialLoad=true;
    pageCount=1;
    [self setHeaderView];
    [self setFontAndColor];
    [self getEventsRequest];
//    [self.tblEvents registerClass:[AQEventTableCell class] forCellReuseIdentifier:@"cellEventsIdentifier"];
    [self.tblEvents registerNib:[UINib nibWithNibName:@"AQEventTableCell" bundle:nil] forCellReuseIdentifier:@"cellEventsIdentifier"];
}


-(void)setHeaderView{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"EVENTS"];
    header.delegate=self;
    [self.vwHeader addSubview:header.view];
}
-(void)clickToBack{
    
}
-(void)clickToMenu
{
    [self leftDrawerButtonPress:nil];
}

-(void)leftDrawerButtonPress:(id)sender
{
    [appObj.drawerObj toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        if (finished)
        {
            
        }
    }];
}

-(void)setFontAndColor
{
    self.view.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.vwFooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblfooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblfooterdan.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.tblEvents.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    
}

-(void)getEventsRequest
{
    if ([AQHelpers isNetworkAvailable])
    {
        if (!isIntialLoad)
        {
            [self.activityIndicator startAnimating];
        }
        
        AQGetEventsRequest *request=[[AQGetEventsRequest alloc]init];
        [request setBaseUrl:SERVERAPIBASEURL];
        [request setServiceUriMiddleValue:[NSString stringWithFormat:@"%@%@",SERVERAPIMEDILEURL,@"events"]];
//        [request setPodType:@"standard"];
        if (!isIntialLoad)
        {
            [request setPageNo:++pageCount];
        }
        else
        {
            pageCount=1;
            [request setPageNo:pageCount];
            
        }
        [request setPageSize:20];
        
        [AQDataManager getMasterData:request controller:self];
        
    }

}

-(void)bind:(id)obj
{
    
    AQGetEventsResponse *response=obj;
    if ([response getisValid])
    {
        NSString *errorMessage = [NSString stringWithFormat:@"%@",response.error.message];
        if ([AQHelpers IsNull:errorMessage])
        {
            if (response.podCasts.count==0) {
                AQAlertView(@"Alert", @"No Events Found!");
            }
            //check over here something fishy can be found
            if (!isIntialLoad)
            {
                for(int i=0;i<response.podCasts.count;++i)
                {
                    [arrEvents addObject:[response.podCasts objectAtIndex:i]];
                }
                
            }
            else
            {
                arrEvents=response.podCasts;
                
            }
            
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                
                // Background work
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Main thread work (UI usually)
                    [self.tblEvents reloadData];
                    
                }];
            }];
            
            
            
        }
        else{
            AQAlertView(@"Error", errorMessage);
            
        }
        
    }
    else
    {
        AQAlertView(@"Error",@"Unable to connect to the server. Please try again later.");
    }
    [self.activityIndicator stopAnimating];
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    beginOffset=  scrollView.contentOffset.y;
}
float beginOffset;

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    
//    float afterOffset=scrollView.contentOffset.y;
//    if (afterOffset - beginOffset >0)
//    {
//        
//        CGFloat height = scrollView.frame.size.height;
//        CGFloat contentYoffset = scrollView.contentOffset.y;
//        CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
//        if(distanceFromBottom == height)
//        {
//            
//            [self doPaging];
//            
//            
//        }
//    }
//}


-(void)doPaging
{
    id visibleCells=self.tblEvents.indexPathsForVisibleRows;
    for (id obj in visibleCells)
    {
        isIntialLoad=false;
        //self.activityIndicator.frame=CGRectMake(self.activityIndicator.frame.origin.x,screenSize.height- self.activityIndicator.frame.size.height - 10,self.activityIndicator.frame.size.width,self.activityIndicator.frame.size.height);
        self.activityIndicator.color=[UIColor whiteColor];
        
        [self getEventsRequest];
        
        
        
        
        break;
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrEvents.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cellEventsIdentifier";
    [[NSBundle mainBundle]loadNibNamed:@"AQEventTableCell" owner:self options:nil];
    AQEventTableCell *cell=[_tblEvents dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell bindCellwithData:arrEvents[indexPath.row] withIndex:indexPath.row+1
     ];
      if (((![self.isUserLoggedIN isEqualToString:@"1"])&&[[[arrEvents objectAtIndex:indexPath.row] eventType] isEqualToString:@"premium"]))
      {
          cell.lblTitle.textColor=[UIColor getUIColorObjectFromHexString:@"999999" alpha:1.0];
          
          
      }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (([self.isUserLoggedIN isEqualToString:@"1"]&&[[[arrEvents objectAtIndex:indexPath.row] eventType] isEqualToString:@"premium"])||(![[[arrEvents objectAtIndex:indexPath.row] eventType] isEqualToString:@"premium"]))
    {
        AQEventDetailScreen *eventDetail=[AQEventDetailScreen new];
        eventDetail.eventsObj=[arrEvents objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:eventDetail animated:YES];
    }else
    {
        AQAlertView(nil, @"Sorry, you must be a registered BDM to access this event.");
    }
   
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
