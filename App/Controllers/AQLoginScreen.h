//
//  AQLoginScreen.h
//  MediocreApp
//
//  Created by Swati Sharma on 07/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AquevixUI/AQUIActiveDetailViewController.h>
@interface AQLoginScreen : AQUIActiveDetailViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollvw;
@property (strong, nonatomic) IBOutlet UIView *vwUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtUserName;
@property (strong, nonatomic) IBOutlet UIView *vwPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)btnLoginClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSignUp;
- (IBAction)btnSignUpClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *vwFooter;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterTom;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterDomCom;











@end
