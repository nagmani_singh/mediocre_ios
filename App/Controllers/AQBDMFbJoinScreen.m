//
//  AQBDMFbJoinScreen.m
//  TomandDanMediocreApp
//
//  Created by techugo on 25/03/18.
//  Copyright © 2018 com.aquevix. All rights reserved.
//

#import "AQBDMFbJoinScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>

@interface AQBDMFbJoinScreen ()<AQHeaderDelegate,MFMailComposeViewControllerDelegate>{
    AQHeaderView *header;
    AppDelegate *appObj;
}
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

@end

@implementation AQBDMFbJoinScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appObj=[UIApplication sharedApplication].delegate ;
    [self setHeaderView];
    [self setFontAndColor];
}


-(void)setHeaderView{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"#BDM"];
    header.delegate=self;
    [self.headerView addSubview:header.view];
}
-(void)clickToBack{
    
}
-(void)clickToMenu
{
    [self leftDrawerButtonPress:nil];
}

-(void)leftDrawerButtonPress:(id)sender
{
    [appObj.drawerObj toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished)
     {
         if (finished)
         {
         }
     }];
}

-(void)setFontAndColor
{
    self.view.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.footerView.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblfooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblfooterdan.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)ClickToSubmit:(UIButton *)sender {
    // From within your active view controller
    if ([AQHelpers IsNull:self.txtFirstName.text]) {
        AQAlertView(@"", @"Please enter first name");
    } else if ([AQHelpers IsNull:self.txtLastName.text]){
        AQAlertView(@"", @"Please enter last name");
    } else if ([AQHelpers IsNull:self.txtFacebookEmail.text]){
        AQAlertView(@"", @"Please enter email");
    }else if (![AQHelpers isValidEmail:self.txtFacebookEmail.text]){
        AQAlertView(@"", @"Please enter valid email id");
    }else{
        if([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
            mailCont.mailComposeDelegate = self;
            NSString *str = [NSString stringWithFormat:@"<p>Hi Tom and Dan,<br /><br />There is a new request for access to <br />BDM facebook page. <br />Details of the user are:</p><p>First name:%@&nbsp;<br />Last name:%@<br />Email:%@</p><p>Thanks,<br />Mediocre App.</p>",_txtFirstName.text,_txtLastName.text,_txtFacebookEmail.text];
            [mailCont setSubject:@"Request for BDM facebook page"];
            [mailCont setToRecipients:[NSArray arrayWithObject:@"hello@tomanddan.com"]];
            [mailCont setMessageBody:str isHTML:YES];
            
            [self presentViewController:mailCont animated:YES completion:^{
                
            }];
            //[self presentModalViewController:mailCont animated:YES];
        }
    }

}

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    //[self dismissModalViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
