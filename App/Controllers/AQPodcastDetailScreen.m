//
//  AQPodcastDetailScreen.m
//  MediocreApp
//
//  Created by Swati Sharma on 09/03/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import "AQPodcastDetailScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
#import "AQSplashScreen.h"
#import "AQPlayerMnager.h"

@interface AQPodcastDetailScreen ()<AQHeaderDelegate>
{
    AQHeaderView *header;
    AppDelegate *appObj;
    PodCasts *dictPodCastsDetail;
    NSString *playerUrl;
    AVURLAsset *asset;
    
}

@end


@implementation AQPodcastDetailScreen
@synthesize slider,lblStartDuration,lblEndDuration,playerGlobal,playerItem,isPaused;

-(id)initWithData:(PodCasts*)podCastValue topImage:(NSString*)strImg header:(NSString*)headerValue
{
    dictPodCastsDetail=podCastValue;
    self.strTopImage = strImg;
    self.strHeader = headerValue;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _isIntialLoad=true;
    // Do any additional setup after loading the view from its nib.
//    [AQHelpers enableAllEventTouch];
    [self setDateOfPodCasts];
    [self setHeaderView];
    if (_isFromCorporatePodcast==YES)
    [self setCorporateFontAndColor];
    else
        [self setFontAndColor];
    
    
    [AQHelpers showActivtyIndicator:self];
    [AQHelpers disableAllEventTouch];
//    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    if ([self urlOfCurrentlyPlayingInPlayer:[[AQPlayerMnager sharedManager] player]]==YES)
    {
        [AQHelpers hideActivityIndicator:self];
        [AQHelpers enableAllEventTouch];

        self.playerGlobal=[[AQPlayerMnager sharedManager] player];
        [[AQPlayerMnager sharedManager] setPlayerObj:self.playerGlobal withPodcast:dictPodCastsDetail ];

        [self.btnNext setEnabled:YES];
        [self.btnPrevious setEnabled:YES];
        self.playerItem = [AVPlayerItem playerItemWithAsset:self.playerGlobal.currentItem.asset];
        [self.playerItem addObserver:self forKeyPath:@"status"
                             options:NSKeyValueObservingOptionInitial context:&ItemStatusContext];
      
        int32_t timeScale = self.playerGlobal.currentItem.asset.duration.timescale;
        float timePrev=[[AQSavedPodcastManager sharedManager]getAudioPlayingPosition:dictPodCastsDetail ];
        CMTime time = CMTimeMakeWithSeconds(timePrev, timeScale);
        [self.playerGlobal seekToTime:time
                      toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
        [self AudioPlaying];
        
        
        [playerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
        [playerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew context:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[self.playerGlobal currentItem]];
        float durartion=[self getAudioDuration];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [AQHelpers hideActivityIndicator:self];
            [AQHelpers enableAllEventTouch];
            self.lblStartDuration.text = @"00:00:00";
            if (durartion>0)
            {
                self.lblEndDuration.text = [NSString stringWithFormat:@"%@",
                                            [self timeFormat:durartion]];
                
            }else
            {
                //                                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No broadcast detected, please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //                                        [alert show];
                self.lblEndDuration.text = @"00:00:00";
                [self.btnNext setEnabled:NO];
                [self.btnPrevious setEnabled:NO];
            }
            NSLog(@"%@",self.lblEndDuration.text);
            if (durartion>=0) {
                self.slider.maximumValue = durartion;
                
            }
            self.slider.value=0.0;
            
            
        });
        
    }else
    {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
                   {
                       [self setAudioPlayer];
                       
                       
                   });
    }
    [self setcustomSlider];
}


-(BOOL)urlOfCurrentlyPlayingInPlayer:(AVPlayer *)player
{
    // get current asset
    AVAsset *currentPlayerAsset = player.currentItem.asset;
    // make sure the current asset is an AVURLAsset
    if (![currentPlayerAsset isKindOfClass:AVURLAsset.class])
    {
        return NO;
    }else
    {
     NSURL *urlOfGlobalPlayer=[(AVURLAsset *)currentPlayerAsset URL];
        if ( [urlOfGlobalPlayer.absoluteString isEqualToString:dictPodCastsDetail.url])
        {
            return YES;
        }else
        {
            return NO;
        }
    }
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    MPMediaItemArtwork *artwork = [[MPMediaItemArtwork alloc]initWithImage:[UIImage imageNamed:@"img_mediocre.png"]];
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[dictPodCastsDetail.title uppercaseString]], MPMediaItemPropertyTitle,
                                                             artwork, MPMediaItemPropertyArtwork, nil];
    [self becomeFirstResponder];
    
}
- (BOOL)canBecomeFirstResponder {
    return YES;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    if (event.type == UIEventTypeRemoteControl)
    {
        if (event.subtype == UIEventSubtypeRemoteControlPlay)
        {
            [playerGlobal play];
            if (_isFromCorporatePodcast==YES)
            {
                [self.imgPlay setImage:[UIImage imageNamed:@"new_ic_stop.png"]];
                
            }else
            {
                [self.imgPlay setImage:[UIImage imageNamed:@"icon_pause.png"]];
            }
            isPaused = true;
        }
        
        else if (event.subtype == UIEventSubtypeRemoteControlPause)
        {
            [playerGlobal pause];
            if (_isFromCorporatePodcast==YES)
            {
                [self.imgPlay setImage:[UIImage imageNamed:@"new_ic_play.png"]];
                
            }else
            {
                [self.imgPlay setImage:[UIImage imageNamed:@"icon_play.png"]];
            }
            isPaused = false;
        }
        else if (event.subtype == UIEventSubtypeRemoteControlTogglePlayPause)
        {
            if (isPaused==false)
            {
                if (_isFromCorporatePodcast==YES)
                {
                    [self.imgPlay setImage:[UIImage imageNamed:@"new_ic_stop.png"]];
                    
                }else
                {
                    [self.imgPlay setImage:[UIImage imageNamed:@"icon_pause.png"]];
                }
              
                [self.playerGlobal play];
                isPaused = true;
                
                
            } else
            {
                if (_isFromCorporatePodcast==YES)
                {
                    [self.imgPlay setImage:[UIImage imageNamed:@"new_ic_play.png"]];
                    
                }else
                {
                    [self.imgPlay setImage:[UIImage imageNamed:@"icon_play.png"]];
                }
                [self.playerGlobal pause];
                isPaused = false;
            }
        }
        else if (event.subtype == UIEventSubtypeRemoteControlNextTrack)
        {
            [self btnNextClick:playerGlobal.currentItem];
        }
        else if (event.subtype == UIEventSubtypeRemoteControlPreviousTrack)
        {
            [self btnPreviousClick:playerGlobal.currentItem];
        }
    }
}

-(void)setDateOfPodCasts
{
    self.imgTopImage.image = [UIImage imageNamed:self.strTopImage];
    self.lblPodTitle.text = [NSString stringWithFormat:@"%@",[dictPodCastsDetail.title uppercaseString]];
    self.lblPodDate.text = [AQHelpers setFormator:[NSString stringWithFormat:@"%@",dictPodCastsDetail.date]];
    playerUrl = [NSString stringWithFormat:@"%@",dictPodCastsDetail.url];
}

-(void)setHeaderView
{
    header = [[AQHeaderView alloc]InitWithdata:self header:self.strHeader];
    [header setDelegate:self];
    [self.vwHeader addSubview:header.view];

    [header.imgMenu setHidden:YES];
    [header.imgBack setHidden:NO];
    [header.btnMenu setHidden:YES];
    [header.btnBack setHidden:NO];
    
    if (_isFromCorporatePodcast==YES)
    {
        [header corporateFontAndColor];
        [header.imgNewBack setHidden:NO];
        [header.btnNewBack setHidden:NO];

    }

}

-(void)setFontAndColor
{
    self.view.backgroundColor = [UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.vwFooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblFooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblFooterdan.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.vwTop.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.vwbottom.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblStartDuration.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.lblEndDuration.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.lblPodTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.lblPodDate.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    
}


-(void)setCorporateFontAndColor
{
    self.vwCorporateFooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateYELLOW alpha:1.0];

    self.vwCorporateFooter.hidden=NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.vwFooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateYELLOW alpha:1.0];
    self.vwTop.backgroundColor=[UIColor whiteColor];
    self.vwbottom.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateBLACK alpha:1.0];
    self.lblStartDuration.textColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateYELLOW alpha:1.0];
    self.lblEndDuration.textColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateYELLOW alpha:1.0];
    self.lblPodTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateYELLOW alpha:1.0];
    self.lblPodDate.textColor=[UIColor getUIColorObjectFromHexString:COLOR_CorporateYELLOW alpha:1.0];
    self.imgTopImage.image=[UIImage imageNamed:@"new_img_corporate.png"];
    [self.imgNext setImage:[UIImage imageNamed:@"new_ic_next.png"] ];
    [self.imgPrevious setImage:[UIImage imageNamed:@"new_ic_previous.png"] ];
    [self.imgLine setImage:[UIImage imageNamed:@"new_ic_foot-line.png"]];
    [self.imgPlay setImage:[UIImage imageNamed:@"new_ic_stop.png.png"]];

}

-(void)handleAudioSessionInterruption:(NSNotification*)notification
{
    if (_isFromCorporatePodcast==YES)
    {
        [self.imgPlay setImage:[UIImage imageNamed:@"new_ic_play.png"]];
    }else
    {
    [self.imgPlay setImage:[UIImage imageNamed:@"icon_play.png"]];
    }
    [self.playerGlobal pause];
    isPaused = false;
}





static const NSString *ItemStatusContext;


-(void)setAudioPlayer
{
    
    
    //    AVURLAsset *asset = [AVURLAsset assetWithURL: [NSURL URLWithString:playerUrl]];
    //
    //    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset: asset];
    ////    AVPlayer *player = [[AVPlayer alloc]initWithPlayerItem: item];
    //    self.playerGlobal = [AVPlayer playerWithPlayerItem:item];;
    //    Float64 duration = CMTimeGetSeconds(self.playerGlobal.currentItem.asset.duration);
    //        NSLog(@"%f",duration);
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *setCategoryError = nil;
    BOOL success = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
    
    NSError *activationError = nil;
    success = [audioSession setActive:YES error:&activationError];
    
    NSString *documentsPath    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAudioSessionInterruption:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:[AVAudioSession sharedInstance]];
    
    NSURL *fileURL = [NSURL URLWithString:playerUrl];
    NSString *finalPath        = [documentsPath stringByAppendingPathComponent:fileURL.lastPathComponent];
    NSLog(@"%@",finalPath);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:finalPath])
    {
        //       fileURL = [NSURL URLWithString:finalPath];
        
        asset = [AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:finalPath] options:nil];
        
    }else
    {
        //        NSDictionary *options = @{ AVURLAssetReferenceRestrictionsKey : @YES };
        
        asset = [AVURLAsset URLAssetWithURL:fileURL options:nil];
        
    }
    NSString *tracksKey = @"tracks";
    
    [asset loadValuesAsynchronouslyForKeys:@[tracksKey] completionHandler:
     ^{
         // The completion block goes here.
         dispatch_async(dispatch_get_main_queue(),
                        ^{
                            NSError *error;
                            AVKeyValueStatus status = [asset statusOfValueForKey:tracksKey error:&error];
                            
                            if (status == AVKeyValueStatusLoaded)
                            {
                                [self.btnNext setEnabled:YES];
                                [self.btnPrevious setEnabled:YES];
                                self.playerItem = [AVPlayerItem playerItemWithAsset:asset];
                                // ensure that this is done before the playerItem is associated with the player
                               
//                                [[NSNotificationCenter defaultCenter] addObserver:self
//                                                                         selector:@selector(playerItemDidReachEnd:)
//                                                                             name:AVPlayerItemDidPlayToEndTimeNotification
//                                                                           object:self.playerItem];
                                
                                self.playerGlobal = [AVPlayer playerWithPlayerItem:self.playerItem];
                                
                                    [[AQPlayerMnager sharedManager] setPlayerObj:self.playerGlobal withPodcast:dictPodCastsDetail ];
                                    self.playerGlobal=[[AQPlayerMnager sharedManager] player];
                               
                               
                                //[self.playerGlobal play];
                                
//                                if ([dict[@"podcastID"] integerValue] == dictPodCastsDetail.id)
//                                {
                                    int32_t timeScale = self.playerGlobal.currentItem.asset.duration.timescale;
                                float timePrev=[[AQSavedPodcastManager sharedManager]getAudioPlayingPosition:dictPodCastsDetail ];                                    CMTime time = CMTimeMakeWithSeconds(timePrev, timeScale);
                                    [self.playerGlobal seekToTime:time
                                                  toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
//                                }
                                [self AudioPlaying];
                                
                                [self.playerItem addObserver:self forKeyPath:@"status"
                                                     options:NSKeyValueObservingOptionInitial context:&ItemStatusContext];                                [playerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
                                [playerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew context:nil];
                                
                                [[NSNotificationCenter defaultCenter] addObserver:self
                                                                         selector:@selector(playerItemDidReachEnd:)
                                                                             name:AVPlayerItemDidPlayToEndTimeNotification
                                                                           object:[self.playerGlobal currentItem]];
                                
                                float durartion=[self getAudioDuration];
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    [AQHelpers hideActivityIndicator:self];
                                    [AQHelpers enableAllEventTouch];
                                    self.lblStartDuration.text = @"00:00:00";
                                    if (durartion>0)
                                    {
                                        self.lblEndDuration.text = [NSString stringWithFormat:@"%@",
                                                                    [self timeFormat:durartion]];
                                        
                                    }else
                                    {
                                        //                                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No broadcast detected, please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                        //                                        [alert show];
                                        self.lblEndDuration.text = @"00:00:00";
                                        [self.btnNext setEnabled:NO];
                                        [self.btnPrevious setEnabled:NO];
                                    }
                                    NSLog(@"%@",self.lblEndDuration.text);
                                    if (durartion>=0) {
                                        self.slider.maximumValue = durartion;
                                        
                                    }
                                    self.slider.value=0.0;
                                    
                                    
                                });
                                
                                
                                
                                
                            }
                            else {
                                // You should deal with the error appropriately.
                                NSLog(@"The asset's tracks were not loaded:\n%@", [error localizedDescription]);
                                //                                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                //                                [alert show];
                                [self setAudioPlayer];
                                //self.lblStartDuration.text = @"00:00:00";
                                //self.lblEndDuration.text = @"00:00:00";
                                [self.btnNext setEnabled:NO];
                                [self.btnPrevious setEnabled:NO];
                                
                            }
                        });
         
     }];
    
}


-(void)setcustomSlider
{
    UIImage *sliderTrackImage = [[UIImage imageNamed:@"icon_media-yellow.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
    [slider setThumbImage:[UIImage imageNamed:@"icon_vertical-bar.png"] forState:UIControlStateNormal];
    [slider setMinimumTrackImage: sliderTrackImage forState: UIControlStateNormal];
    [slider setMaximumTrackImage: sliderTrackImage forState: UIControlStateNormal];
    [slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    
}

- (IBAction)sliderValueChanged:(UISlider *)sender
{
    [self.timer invalidate];
    self.timer=nil;
    
    NSLog(@"slider value = %f", sender.value);
    
    
    self.lblStartDuration.text = [NSString stringWithFormat:@"%@",[self timeFormat:sender.value]];
    
    self.lblEndDuration.text = [NSString stringWithFormat:@"%@",
                                [self timeFormat:[self getAudioDuration]]];
    self.slider.value=sender.value;
    
    int32_t timeScale = self.playerGlobal.currentItem.asset.duration.timescale;
    CMTime time = CMTimeMakeWithSeconds(sender.value, timeScale);
    
    
    
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(updateTime:)
                                                userInfo:nil
                                                 repeats:YES];
    [self.playerGlobal seekToTime:time
                  toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    if(isPaused)
    {
        [playerGlobal play];
        if (_isFromCorporatePodcast==YES)
        {
            [self.imgPlay setImage:[UIImage imageNamed:@"new_ic_stop.png"]];

        }else
        {
        [self.imgPlay setImage:[UIImage imageNamed:@"icon_pause.png"]];
        }
    }
    else
    {
        [playerGlobal pause];
        if (_isFromCorporatePodcast==YES)
        {
            [self.imgPlay setImage:[UIImage imageNamed:@"new_ic_play.png"]];
            
        }else
        {
            [self.imgPlay setImage:[UIImage imageNamed:@"icon_play.png"]];
        }
    }
}



-(NSString*)timeFormat:(int)value
{
   	
    int seconds = value % 60;
    int minutes = (value / 60) % 60;
    int hours = value / 3600;
    
    
    //    float minutes = floor(lroundf(value)/60);
    //    float seconds = lroundf(value) - (minutes * 60);
    //    float hours = floor(lroundf(value)/3600);
    NSInteger roundedSeconds = lroundf(seconds);
    NSInteger roundedMinutes = lroundf(minutes);
    NSInteger roundedhours =  lroundf(hours);
    NSString *time = [[NSString alloc]initWithFormat:@"%02ld:%02ld:%02ld",(long)roundedhours,(long)roundedMinutes,(long)roundedSeconds];
    return time;
    
    
}


-(float)getAudioDuration
{
    AVPlayerItem *currentItem = self.playerGlobal.currentItem;
    NSTimeInterval duration1 = CMTimeGetSeconds(currentItem.asset.duration);
    NSLog(@"get durr time %f",duration1);
    return duration1;
}


-(IBAction)btnPreviousClick:(id)sender
{
    [self.timer invalidate];
    self.timer=nil;
    
    float seconds=-30;
    
    
    //  self.lblStartDuration.text = [NSString stringWithFormat:@"%@",[self timeFormat:[self.lblStartDuration.text floatValue]-seconds]];
    
    //    self.lblEndDuration.text = [NSString stringWithFormat:@"%@",
    //                                [self timeFormat:([self getAudioDuration]+seconds)]];
    
    self.slider.value=self.slider.value+seconds;
    
    if (self.slider.value >0)
    {
        CMTime time = CMTimeMakeWithSeconds(CMTimeGetSeconds(playerGlobal.currentTime)-30, playerGlobal.currentTime.timescale);
        double timePrev= CMTimeGetSeconds(time);
        self.lblStartDuration.text = [NSString stringWithFormat:@"%@",
                                      [self timeFormat:timePrev]];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTime:)
                                                    userInfo:nil
                                                     repeats:YES];
        
        [self.playerGlobal seekToTime:time
                      toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    }else
    {
        CMTime time = CMTimeMakeWithSeconds(0,playerGlobal.currentTime.timescale);
        self.lblStartDuration.text = [NSString stringWithFormat:@"%@",
                                      [self timeFormat:CMTimeGetSeconds(time)]];
        [self.playerGlobal seekToTime:time
                      toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTime:)
                                                    userInfo:nil
                                                     repeats:YES];
    }
    
    
    self.lblEndDuration.text = [NSString stringWithFormat:@"%@",
                                [self timeFormat:[self getAudioDuration] - [self getCurrentAudioTime]]];
}



-(IBAction)btnNextClick:(id)sender
{
    [self.timer invalidate];
    self.timer=nil;
    
    float seconds=30;
    
    
    self.slider.value=self.slider.value+seconds;
    
    
    CMTime time = CMTimeMakeWithSeconds(CMTimeGetSeconds(playerGlobal.currentTime)+30, playerGlobal.currentTime.timescale);
    double timePrev= CMTimeGetSeconds(time);
    self.lblStartDuration.text = [NSString stringWithFormat:@"%@",
                                  [self timeFormat:timePrev]];
    
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(updateTime:)
                                                userInfo:nil
                                                 repeats:YES];
    
    [self.playerGlobal seekToTime:time
                  toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    
    self.lblEndDuration.text = [NSString stringWithFormat:@"%@",
                                [self timeFormat:[self getAudioDuration] - [self getCurrentAudioTime]]];
    
    
    
    
    
    
}




-(IBAction)btnPlayClick:(id)sender
{
    
    [self AudioPlaying];
}

-(void) AudioPlaying
{
    _isIntialLoad=false;
    [self.timer invalidate];
    self.timer=nil;
    
    if (isPaused == false)
    {
        if (_isFromCorporatePodcast==YES)
        {
            [self.imgPlay setImage:[UIImage imageNamed:@"new_ic_stop.png"]];
            
        }else
        {
            [self.imgPlay setImage:[UIImage imageNamed:@"icon_pause.png"]];
        }
        
        [self.playerGlobal play];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTime:)
                                                    userInfo:nil
                                                     repeats:YES];
        isPaused = true;
        NSLog(@"%ld",(long)self.playerGlobal.rate);
        
    }
    else
    {
        if (_isFromCorporatePodcast==YES)
        {
            [self.imgPlay setImage:[UIImage imageNamed:@"new_ic_play.png"]];
            
        }else
        {
            [self.imgPlay setImage:[UIImage imageNamed:@"icon_play.png"]];
        }

        [self.playerGlobal pause];
        NSLog(@"%ld",(long)self.playerGlobal.rate);
        isPaused = false;
    }
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context

{
    if (object == playerItem && [keyPath isEqualToString:@"playbackBufferEmpty"])
    {
        if (playerItem.playbackBufferEmpty)
        {
            
        }
    }
    
    else if (object == playerItem && [keyPath isEqualToString:@"playbackLikelyToKeepUp"])
    {
        if (playerItem.playbackLikelyToKeepUp)
        {
            if (_isIntialLoad)
            {
                
            }
            else
            {
                if(isPaused)
                {
                    [playerGlobal play];

                }
                else
                {

                }
            }
            
            
        }
    }
    
    
    /////////////
    if (object == self.playerItem && [keyPath isEqualToString:@"status"]) {
        if (self.playerGlobal.status == AVPlayerStatusFailed)
        {
            NSLog(@"AVPlayer Failed");
            NSLog(@"%@",self.playerGlobal.error.userInfo);
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No broadcast detected, please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        } else if (self.playerGlobal.status == AVPlayerStatusReadyToPlay)
        {
            NSLog(@"AVPlayerStatusReadyToPlay");
            
            
        } else if (self.playerGlobal.status == AVPlayerItemStatusUnknown)
        {
            NSLog(@"AVPlayer Unknown");
            
        }
    }
    
    
    
}




- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    
    //  code here to play next sound file
    if (_isFromCorporatePodcast==YES)
    {
        [self.imgPlay setImage:[UIImage imageNamed:@"new_ic_play.png"]];
        
    }else
    {
        [self.imgPlay setImage:[UIImage imageNamed:@"icon_play.png"]];
    }
    [playerGlobal pause];
    isPaused = false;
    
    self.slider.value=0.0;
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
    
}


-(void)updateTime:(NSTimer *)timer
{
    
    
    if (!self.scrubbing)
    {
        self.slider.value = [self getCurrentAudioTime];
        NSLog(@"my slider %f",self.slider.value);
    }
    self.lblStartDuration.text = [NSString stringWithFormat:@"%@",
                                  [self timeFormat:[self getCurrentAudioTime]]];
    self.lblEndDuration.text = [NSString stringWithFormat:@"%@",
                                [self timeFormat:[self getAudioDuration] - [self getCurrentAudioTime]]];
    if ([self.lblEndDuration.text isEqual:@"00:00:00"])
    {
        [self.timer invalidate];
    }
}

-(NSTimeInterval)getCurrentAudioTime
{
    AVPlayerItem *currentItem = playerGlobal.currentItem;
    
    NSTimeInterval currentTime = CMTimeGetSeconds(currentItem.currentTime);
    NSLog(@"get curr time %f",currentTime);
    return currentTime;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)clickToMenu{
}
-(void)clickToBack{
    
    
    [[NSUserDefaults standardUserDefaults]setObject: [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f",[self getCurrentAudioTime]],@"playedTime",[NSString stringWithFormat:@"%ld",(long)dictPodCastsDetail.id],@"podcastID", nil] forKey:@"podcastPreviousPlay"];
    [self.timer invalidate];
    self.timer=nil;
    if (playerGlobal != nil && [playerGlobal currentItem] != nil)
    {
        [self.playerItem removeObserver:self forKeyPath:@"status" context:&ItemStatusContext  ];
        [self.playerItem removeObserver:self forKeyPath:@"playbackBufferEmpty" context:nil];
        [self.playerItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp" context:nil];
//        [playerGlobal pause];
//        
//        playerGlobal=nil;
    }

       NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    for (int i = 0; i <[allViewControllers count]; i++)
    {
        UIViewController *vc = [allViewControllers objectAtIndex:i];
        if ([vc isKindOfClass:[AQSplashScreen class]])
        {
            
            [allViewControllers removeObjectAtIndex:i];
        }
    }
    
    
    self.navigationController.viewControllers = allViewControllers;
    [self.navigationController popViewControllerAnimated:self];
}

@end
