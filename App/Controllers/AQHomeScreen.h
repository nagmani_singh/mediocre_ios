//
//  AQHomeScreen.h
//  MediocreApp
//
//  Created by Swati Sharma on 07/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AQHomeScreen : UIViewController
@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblCallTheShow;
@property (strong, nonatomic) IBOutlet UILabel *lblNumber;
- (IBAction)btnCallShowPIMPSClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterTom;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterDanCom;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollvw;
@property (strong, nonatomic) IBOutlet UIView *vwFooter;
@property (strong, nonatomic) IBOutlet UIButton *btnSignUp;
- (IBAction)btnSignUpClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vwAds;
@property (weak, nonatomic) IBOutlet UIImageView *imgAds;
@property (weak, nonatomic) IBOutlet UILabel *lblAdsTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnCloseAds;
- (IBAction)btnCloseAdsClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAdsImages;
- (IBAction)btnadsImagesClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorForAds;




@end
