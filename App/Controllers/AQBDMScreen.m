//
//  AQBDMScreen.m
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQBDMScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
#import "AQPodcastDetailScreen.h"
#import <AppAPI/AQGetPodCastsRequest.h>
#import <AppAPI/AQGetPodCastsResponse.h>
#import <AppAPI/AQDataManager.h>
#import "BDMDownload.h"
#import "NTMonthYearPicker.h"
#import "AQBDMFbJoinScreen.h"

@interface AQBDMScreen ()<AQHeaderDelegate,AQBDMDelegate,UITableViewDelegate,UITableViewDataSource,podCastsDelegate>
{
    NSMutableArray *arrBDMList;
    AQHeaderView *header;
    AppDelegate *appObj;
    float receivedData;
    long expectedBytes;
    NSString *currentUrl;
    AQBDMCell *bdmCell;
    NSInteger indexDownloadBtn;
    NSMutableDictionary *dictPodCast;
    NSMutableArray *arrdownloadId;
    PodCasts *podCastObj;
    CGSize screenSize;
    BOOL isIntialLoad;
    NSInteger pageCount;
    BOOL makeDeletable;
    BOOL isoffline;

    NSString *strForReferingMethod;

}
@property (weak, nonatomic) IBOutlet UIView *vwSearchIcon;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollVW;
@property (weak, nonatomic) IBOutlet UIView *vwdatePicker;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchicon;
@property (strong, nonatomic)   NTMonthYearPicker *datePicker;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation AQBDMScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    pageCount=1;
    arrBDMList=[[NSMutableArray alloc]init];
    screenSize=[[UIScreen mainScreen]bounds].size;
    isIntialLoad = true;
    [self.activityIndicator startAnimating];
    appObj=[UIApplication sharedApplication].delegate ;

    [self getBDMRequest];
    [self setHeaderView];
    [self setFontAndColor];
    [self setNTdatePicker];
    [BDMDownload sharedInstance].delegate=self;

    [self.tblBDM registerClass:[AQBDMCell class] forCellReuseIdentifier:@"cell"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setNTdatePicker
{
    _datePicker=[[NTMonthYearPicker alloc]initWithFrame:CGRectMake(0, 0, _vwdatePicker.frame.size.width,_vwdatePicker.frame.size.height )];
    [_datePicker setMaximumDate:[NSDate date]];
    
    [_vwdatePicker addSubview:_datePicker];
    [_vwdatePicker insertSubview:_datePicker atIndex:0];
    
}
- (IBAction)ClickToJoinBDMFb:(UIButton *)sender {
    
    AQBDMFbJoinScreen *screen = [[AQBDMFbJoinScreen alloc]initWithNibName:@"AQBDMFbJoinScreen" bundle:nil];
    [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
    
}


-(void)setHeaderView{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"#BDM"];
    header.delegate=self;
    [self.vwHeader addSubview:header.view];
}
-(void)clickToBack{
    
}
-(void)clickToMenu
{
    [self leftDrawerButtonPress:nil];
}

-(void)leftDrawerButtonPress:(id)sender
{
    [appObj.drawerObj toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished)
    {
        if (finished)
        {
        }
    }];
}

-(void)setFontAndColor
{
     self.view.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1.0];
    self.vwFooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblfooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblfooterdan.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.tblBDM.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1.0];
}

// UITableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrBDMList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSBundle mainBundle]loadNibNamed:@"AQBDMCell" owner:self options:nil];
    bdmCell=self.cellBDM;
    [bdmCell setDelegate:self];
    bdmCell.btnCell.tag=indexPath.row+1;
    bdmCell.btnDownLoad.tag=indexPath.row+1;
    bdmCell.deleteBtn.tag=indexPath.row+1;
    bdmCell.scrlVW.tag=indexPath.row+1;

    [bdmCell bindData:arrBDMList index:indexPath.row];
    bdmCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *documentsPath    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *finalPath        = [documentsPath stringByAppendingPathComponent:bdmCell.cellUrl.lastPathComponent];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:finalPath]==YES)
    {
        makeDeletable=YES;
        [bdmCell.btnDownLoad setEnabled:NO];
        bdmCell.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-disable.png"];
        [bdmCell.cellProgress setProgress:1.0];

        if (indexPath.row%2!=0)
        {
            [bdmCell.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0]];
            bdmCell.imgDelete.image = [UIImage imageNamed:@"icon_delete-bdm-enabled.png"];
        }
        else
        {
            [bdmCell.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1.0]];
            bdmCell.imgDelete.image = [UIImage imageNamed:@"icon_delete-bdm-enabled.png"];
        }
    }else
    {
        makeDeletable=NO;
        [bdmCell.deleteBtn setEnabled:makeDeletable];
        if (indexPath.row%2!=0)
            bdmCell.imgDelete.image = [UIImage imageNamed:@"icon_delete-disabled-odd.png"];
        else
            bdmCell.imgDelete.image = [UIImage imageNamed:@"icon_delete-disabled-even.png"];
        

        //[bdmCell.deleteBtn setBackgroundColor:[UIColor grayColor]];

    }
    

    
    return bdmCell;
}

// table cell custom delegate method
-(void)clickToBDMCellButton:(NSInteger)index{
//    [AQHelpers disableAllEventTouch];
     podCastObj = [arrBDMList objectAtIndex:index];
    NSIndexPath *sender = [NSIndexPath indexPathForRow:(index) inSection:0];
    NSIndexPath * path = (NSIndexPath *)sender;
    AQBDMCell* cellPod = [self.tblBDM cellForRowAtIndexPath:path];
    if (cellPod.isDownloading==NO)
    {
        AQPodcastDetailScreen *screen = [[AQPodcastDetailScreen alloc]initWithData:podCastObj topImage:@"img_scumbag.png" header:@"#BDM"];
        [self.navigationController pushViewController:screen animated:YES];
    }else
    {
        AQAlertView(nil, @"File is still downloading. Please try later");
    }
   
}

-(void)clickToBDMDownloadButton:(NSInteger)index{
     podCastObj = [arrBDMList objectAtIndex:index];
    NSIndexPath *sender = [NSIndexPath indexPathForRow:(index) inSection:0];
    NSIndexPath * path = (NSIndexPath *)sender;
    bdmCell = [self.tblBDM cellForRowAtIndexPath:path];

    currentUrl=[NSString stringWithFormat:@"%@",podCastObj.url];
    NSURL *url = [NSURL URLWithString:currentUrl];

    receivedData =0;
    [[BDMDownload sharedInstance] downloadAudioStream:url :(int)index ];
    [BDMDownload sharedInstance].delegate=self;
    
}


-(void)afterTaskComplete:(int)index withURL:(NSURL *)url

{
    NSIndexSet *indexes = [arrBDMList indexesOfObjectsPassingTest:^BOOL(PodCasts * obj, NSUInteger idx, BOOL *stop){
        if ([obj isKindOfClass:[PodCasts class]]==YES)
        {
            NSString *s = [obj url];
            NSRange range = [s rangeOfString:url.absoluteString];
            return range.location != NSNotFound;
            
        }else
        {
            return nil;
        }
        
    }];

    
    NSIndexPath *sender = [NSIndexPath indexPathForRow:([indexes firstIndex]) inSection:0];
//    NSIndexPath *sender = [NSIndexPath indexPathForRow:(index) inSection:0];
    NSIndexPath * path = (NSIndexPath *)sender;
    AQBDMCell* cellPod = [self.tblBDM cellForRowAtIndexPath:path];
    podCastObj = [arrBDMList objectAtIndex:[indexes firstIndex]];
    if (![[[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]allKeys]containsObject:@"arrayBDMWithoutInternet"]) {
        NSMutableArray *arrObj=[[NSMutableArray alloc]init];
        NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:podCastObj];
        [arrObj addObject:personEncodedObject];
        [[NSUserDefaults standardUserDefaults]setObject:arrObj forKey:@"arrayBDMWithoutInternet"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }else
    {
        NSMutableArray *arrObj=[[[NSUserDefaults standardUserDefaults]objectForKey:@"arrayBDMWithoutInternet"] mutableCopy];
        [arrObj addObject:[NSKeyedArchiver archivedDataWithRootObject:podCastObj]];
        [[NSUserDefaults standardUserDefaults]setObject:arrObj forKey:@"arrayBDMWithoutInternet"];
    }
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [cellPod.btnDownLoad setEnabled:NO];
                       cellPod.isDownloading=NO;

                       cellPod.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-disable.png"];
                       [cellPod.deleteBtn setEnabled:YES];
                       //[cellPod.deleteBtn setBackgroundColor:[UIColor redColor]];
                       if (index%2!=0)
                       {
                           [cellPod.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0]];
                           cellPod.imgDelete.image = [UIImage imageNamed:@"icon_delete-bdm-enabled.png"];

                       }
                       else
                       {
                           [cellPod.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1.0]];
                           cellPod.imgDelete.image = [UIImage imageNamed:@"icon_delete-bdm-enabled.png"];

                       }
                   });

    
}
-(void)downloadFailedWithError:(int)index
{
    NSIndexPath *sender = [NSIndexPath indexPathForRow:(index) inSection:0];
    NSIndexPath * path = (NSIndexPath *)sender;
    AQBDMCell* cellPod = [self.tblBDM cellForRowAtIndexPath:path];
    [cellPod.btnDownLoad setEnabled:YES];
    cellPod.isDownloading=NO;

}


-(void)downloadedPrograssPodcast :(float)receivedata withExpectedData:(float)expectedData withCellID:(int)cellId withClass:(UIViewController *)isclass withURL:(NSURL *)url
{
    if ([isclass isKindOfClass:[AQBDMScreen class]]==YES)
    {
        receivedData = receivedata;
        
        float progressive = receivedData/expectedData;
        NSIndexSet *indexes = [arrBDMList indexesOfObjectsPassingTest:^BOOL(PodCasts * obj, NSUInteger idx, BOOL *stop){
            if ([obj isKindOfClass:[PodCasts class]]==YES)
            {
                NSString *s = [obj url];
                NSRange range = [s rangeOfString:url.absoluteString];
                return range.location != NSNotFound;
                
            }else
            {
                return nil;
            }
            
        }];
        
        
        NSIndexPath *sender = [NSIndexPath indexPathForRow:([indexes firstIndex]) inSection:0];
//        NSIndexPath *sender = [NSIndexPath indexPathForRow:(cellId) inSection:0];
        NSIndexPath * path = (NSIndexPath *)sender;
                dispatch_async(dispatch_get_main_queue(),
                       ^{
                           AQBDMCell* cellPod = [self.tblBDM cellForRowAtIndexPath:path];

                           if ([cellPod.cellUrl isEqual:url]) {
                               [cellPod.cellProgress setProgress:progressive];
                               
                           }
                           [cellPod.btnDownLoad setEnabled:NO];
                           cellPod.isDownloading=YES;

                           //cellPod.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-disable.png"];
                       });

    }
}





//getBDM Request Method
-(void)getBDMRequest{
    
      if ([AQHelpers isNetworkAvailable]) {
    if (!isIntialLoad)
    {
        [self.activityIndicator startAnimating];
    }
          isoffline=NO;

    AQGetPodCastsRequest *request=[[AQGetPodCastsRequest alloc]init];
    [request setBaseUrl:SERVERAPIBASEURL];
    [request setServiceUriMiddleValue:[NSString stringWithFormat:@"%@%@",SERVERAPIMEDILEURL,@"podcast"]];
    [request setPodType:@"premium"];
    if (!isIntialLoad)
    {
        [request setPageNo:++pageCount];
    }
    else
    {
        pageCount=1;
        [request setPageNo:pageCount];
        
    }
    [request setPageSize:20];
    
          if ([strForReferingMethod isEqualToString:@"searchByDate"])
          {
              [request setpubDate:[self convertdatetostring:_datePicker.date]];
              
          }else if ([strForReferingMethod isEqualToString:@"searchByText"])
          {
              [request setSearchString:_txtSearchicon.text];
              
          }else
          {
              
          }
          
    [AQDataManager getMasterData:request controller:self];
      }
      else{
          [AQHelpers hideActivityIndicator:self];
//          UIAlertView *alertConnection=[[UIAlertView alloc]initWithTitle:@"Connection Failed" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//          [alertConnection show];
          isoffline=YES;

          if ([[[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]allKeys]containsObject:@"arrayBDMWithoutInternet"]) {
              NSMutableArray *arr=[[NSUserDefaults standardUserDefaults]objectForKey:@"arrayBDMWithoutInternet"];
              NSMutableArray *newarray=[[NSMutableArray alloc]init];
              for (NSData *data in arr) {
                  PodCasts*    obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                  [newarray addObject:obj];
              }
              NSMutableArray *tempArray = [newarray mutableCopy];
              
              for (PodCasts *pods in newarray)
              {
                  NSString *documentsPath    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                  
                  
                  NSURL *fileURL = [NSURL URLWithString:pods.url];
                  NSString *finalPath        = [documentsPath stringByAppendingPathComponent:fileURL.lastPathComponent];
                  NSLog(@"%@",finalPath);
                  NSFileManager *fileManager = [NSFileManager defaultManager];
                  if (![fileManager fileExistsAtPath:finalPath])
                  {
                      [tempArray removeObject:pods];
                  }
                  
              }
              newarray=tempArray;

              arrBDMList=newarray;
              makeDeletable=YES;

              [self.tblBDM reloadData];
              
          }else
          {
              
          }

          
          
      }
}

-(void)bind:(id)obj
{
   
    AQGetPodCastsResponse *response=obj;
    if ([response getisValid])
    {
        
        NSString *errorMessage = [NSString stringWithFormat:@"%@",response.error];
        if ([AQHelpers IsNull:errorMessage])
        {
            if (!isIntialLoad)
            {
                for(int i=0;i<response.podCasts.count;++i)
                {
                    [arrBDMList addObject:[response.podCasts objectAtIndex:i]];
                }
            }
            else
            {
                
                if (response.podCasts.count==0)
                {
                    AQAlertView(@"Search Completed", @"No Results Found");
                }else
                {
                arrBDMList=[[NSMutableArray alloc]init];
                arrBDMList=response.podCasts;
                }
            }
            
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                
                // Background work
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Main thread work (UI usually)
                    makeDeletable=NO;

                    [self.tblBDM reloadData];
                }];
                 
                }];
         }
        else
        {
            AQAlertView(@"Error", errorMessage);
            
        }
        
    }
     [self.activityIndicator stopAnimating];
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return makeDeletable;
}


-(void)clickToBtnDelete:(NSInteger)index
{
   
        podCastObj = [arrBDMList objectAtIndex:index];
        
        NSString *documentsPath    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        
        
        NSURL *fileURL = [NSURL URLWithString:podCastObj.url];
        NSString *finalPath        = [documentsPath stringByAppendingPathComponent:fileURL.lastPathComponent];
        NSLog(@"%@",finalPath);
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if ([fileManager fileExistsAtPath:finalPath])
        {
            if ([fileManager isDeletableFileAtPath:finalPath])
            {
                NSError *error;
                [fileManager removeItemAtPath:finalPath error:&error];
                if (error!=nil)
                {
                    AQAlertView(@"Error!", error.localizedDescription);
                }else
                {
                    if (isoffline==NO) {
                        
                    }else
                    {
                        [arrBDMList removeObjectAtIndex:index];
                    }
                    NSMutableArray *arr=[[NSUserDefaults standardUserDefaults]objectForKey:@"arrayBDMWithoutInternet"];
                    NSMutableArray *newarray=[[NSMutableArray alloc]init];
                    for (NSData *data in arr) {
                        PodCasts*    obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                        [newarray addObject:obj];
                    }
                    NSMutableArray *tempArray = [newarray mutableCopy];
                    
                    for (PodCasts *pods in newarray)
                    {
                        NSString *documentsPath    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                        
                        
                        NSURL *fileURL = [NSURL URLWithString:pods.url];
                        NSString *finalPath        = [documentsPath stringByAppendingPathComponent:fileURL.lastPathComponent];
                        NSLog(@"%@",finalPath);
                        NSFileManager *fileManager = [NSFileManager defaultManager];
                        if (![fileManager fileExistsAtPath:finalPath])
                        {
                            [tempArray removeObject:pods];
                        }
                        
                    }
                    newarray=tempArray;
                    
                    NSMutableArray *arrObj=[[NSMutableArray alloc]init];
                    for (PodCasts *podObj in tempArray)
                    {
                        NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:podObj];
                        [arrObj addObject:personEncodedObject];
                        
                    }
                    [[NSUserDefaults standardUserDefaults]setObject:arrObj forKey:@"arrayBDMWithoutInternet"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    AQBDMCell* cellPod = [self.tblBDM cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                    dispatch_async(dispatch_get_main_queue(),
                                   ^{
                                       
                                       if (index%2!=0)
                                       {
                                           cellPod.vwbackground.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
                                           cellPod.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-bdm.png"];
                                           cellPod.imgBDM.image = [UIImage imageNamed:@"icon_bdm-even.png"];
                                           cellPod.lblBDMDate.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
                                           cellPod.lblBDMTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
                                           [cellPod.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0]];
                                           [cellPod.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0]];
                                       }
                                       else{
                                           cellPod.vwbackground.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1.0];
                                           cellPod.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-bdm.png"];
                                           cellPod.imgBDM.image = [UIImage imageNamed:@"icon_bdm-odd.png"];
                                           cellPod.lblBDMDate.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
                                           cellPod.lblBDMTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
                                           [cellPod.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0]];
                                           [cellPod.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1.0]];
                                           
                                       }
                                       
                                   });
                    
                    
                }
            }
        }
        
        [self.tblBDM reloadData];
    }


-(void)scrollViewEndCallBack:(NSInteger)index
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       AppDelegate *appdel= (AppDelegate *)[[UIApplication sharedApplication] delegate] ;
                       
                       
                       if (appdel.openPodcastCellArray.count==0)
                       {
                           
                           [appdel.openPodcastCellArray addObject:[NSString stringWithFormat:@"%ld",(long)index]];
                           
                       }else
                       {
                           
                           NSInteger indexVal=[[appdel.openPodcastCellArray objectAtIndex:0]integerValue];
                           //                          long val=[appdel.openPodcastCellArray[0] longValue];
                           AQBDMCell *oldcell=[self.tblBDM cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexVal inSection:0]];
                           [oldcell.scrlVW setContentOffset:CGPointMake(0, 0)];

                           
                           [appdel.openPodcastCellArray removeAllObjects];
                           [appdel.openPodcastCellArray addObject:[NSString stringWithFormat:@"%ld",(long)index]];
                           
                       }
                       
                   });
    
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    beginOffset=  scrollView.contentOffset.y;
}
float beginOffset;


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float afterOffset=scrollView.contentOffset.y;
    if (afterOffset - beginOffset >0)
    {
        
        CGFloat height = scrollView.frame.size.height;
        CGFloat contentYoffset = scrollView.contentOffset.y;
        CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
        if(distanceFromBottom == height)
        {
        [self doPaging];
        }
    }
}

-(void)doPaging
{
    id visibleCells=self.tblBDM.indexPathsForVisibleRows;
    for (id obj in visibleCells)
    {
        isIntialLoad=false;
       // self.activityIndicator.frame=CGRectMake(self.activityIndicator.frame.origin.x,self.tblBDM.frame.size.height+20,self.activityIndicator.frame.size.width,self.activityIndicator.frame.size.height);
        self.activityIndicator.color=[UIColor yellowColor];
        [self getBDMRequest];
        break;
    }
}



- (IBAction)btnFiilerClicked:(UIButton *)sender
{
    [self.txtSearchicon resignFirstResponder];
    
    if (sender.tag==0) {
        sender.tag=1;
        [_vwSearchIcon setHidden:YES];
        
        [_scrollVW setContentSize:CGSizeMake(_scrollVW.frame.size.width, _scrollVW.frame.size.height+_vwdatePicker.frame.size.height)];
        //    [_scrollVW insertSubview:_vwdatePicker belowSubview:_vwFooter];
        [_vwdatePicker setFrame:CGRectMake(0, _vwFooter.frame.size.height+_vwFooter.frame.origin.y, _vwdatePicker.frame.size.width, _vwdatePicker.frame.size.height)];
        [UIView animateWithDuration:0.3 animations:^{
            [_vwdatePicker setHidden:NO];
            [_scrollVW setContentOffset:CGPointMake(0, _vwdatePicker.frame.size.height)];
        }];
        
    }else
    {
        sender.tag=0;
        self.btnSearch.tag=0;
        [_txtSearchicon resignFirstResponder];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [_scrollVW setContentSize:CGSizeMake(_scrollVW.frame.size.width, _scrollVW.frame.size.height)];
            
            
        }completion:^(BOOL finished) {
            if (finished) {
                [_vwdatePicker setHidden:YES];
                [_vwSearchIcon setHidden:YES];
                
            }
            
        }];
    }
    
    
}
- (IBAction)btnSearchIconClicked:(UIButton *)sender {
    if (sender.tag==0) {
        sender.tag=1;
        [_vwdatePicker setHidden:YES];
        
        [_scrollVW setContentSize:CGSizeMake(_scrollVW.frame.size.width, _scrollVW.frame.size.height+_vwSearchIcon.frame.size.height)];
        [_vwSearchIcon setFrame:CGRectMake(0, _vwFooter.frame.size.height+_vwFooter.frame.origin.y, _vwSearchIcon.frame.size.width, _vwSearchIcon.frame.size.height)];
        [_vwSearchIcon setHidden:NO];
        
        [UIView animateWithDuration:0.3 animations:^{
            [_scrollVW setContentOffset:CGPointMake(0, _vwSearchIcon.frame.size.height)];
        }];
        
    }else
    {
        sender.tag=0;
        self.btnFilter.tag=0;
        [_txtSearchicon resignFirstResponder];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [_scrollVW setContentSize:CGSizeMake(_scrollVW.frame.size.width, _scrollVW.frame.size.height)];
            
        }completion:^(BOOL finished) {
            if (finished) {
                [_vwSearchIcon setHidden:YES];
                [_vwdatePicker setHidden:YES];
                
            }
            
        }];
        
    }
    
}


- (IBAction)btnSearchWithTextClicked:(id)sender {
    
    //    [_txtSearchicon resignFirstResponder];
    isIntialLoad=true;
    strForReferingMethod=@"searchByText";
    [self hidesearchviewwithBtn:_btnSearch];
    [self getBDMRequest];
    
    
    
}

-(void)hidesearchviewwithBtn:(UIButton *)sender
{
    sender.tag=0;
    self.btnFilter.tag=0;
    
    [UIView animateWithDuration:0.3 animations:^{
        [_txtSearchicon resignFirstResponder];
        
        [_scrollVW setContentSize:CGSizeMake(_scrollVW.frame.size.width, _scrollVW.frame.size.height)];
        
    }completion:^(BOOL finished) {
        if (finished) {
            [_vwSearchIcon setHidden:YES];
            [_vwdatePicker setHidden:YES];
            
        }
        
    }];
    
}


- (IBAction)btnSearchByDateClicked:(id)sender {
    
    isIntialLoad=true;
    
    strForReferingMethod=@"searchByDate";
    [self getBDMRequest];
    [self hidesearchviewwithBtn:_btnFilter];
    
    
    
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(NSString *)convertdatetostring:(NSDate *)date
{
    NSDateComponents *components=[[NSCalendar currentCalendar]components:NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitDay fromDate:date];
    NSString *datestr=[NSString stringWithFormat:@"%ld-%ld-%ld",(long)[components year],(long)[components month],(long)[components day]];
    return datestr;
}







@end
