//
//  AQBDMFbJoinScreen.h
//  TomandDanMediocreApp
//
//  Created by techugo on 25/03/18.
//  Copyright © 2018 com.aquevix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AQBDMFbJoinScreen : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;

@property (weak, nonatomic) IBOutlet UITextField *txtFacebookEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblfooterTom;
@property (strong, nonatomic) IBOutlet UILabel *lblfooterdan;

@end
