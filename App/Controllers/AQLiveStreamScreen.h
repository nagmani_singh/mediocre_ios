//
//  AQLiveStreamScreen.h
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface AQLiveStreamScreen : UIViewController
@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollvw;
@property (strong, nonatomic) IBOutlet UIButton *btnStartStreaming;
- (IBAction)btnStartStreamingClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblDays;
@property (strong, nonatomic) IBOutlet UILabel *lblHours;
@property (strong, nonatomic) IBOutlet UILabel *lblMins;
@property (strong, nonatomic) IBOutlet UILabel *lblDurations;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UIView *vwFooter;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterTom;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterDanCom;
@property(strong,nonatomic)AVPlayerItem *playerItem;
@property(strong,nonatomic)AVPlayer *player;;
@property (nonatomic, strong)  NSURL      *urlSavedAudio;
@property (nonatomic, strong) NSTimer  *timer;
@property BOOL isPlayShowing;
-(void)stopTimer;





@end
