//
//  AQMessageScreen.m
//  MediocreApp
//
//  Created by Swati Sharma on 09/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQMessageScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
#import "AQSendAudioScreen.h"
#import "AQSendImageScreen.h"
#import "AQSendVideoScreen.h"
@interface AQMessageScreen ()<AQHeaderDelegate>
{
    AQHeaderView *header;
    AppDelegate *appObj;
}
@end

@implementation AQMessageScreen

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appObj=[UIApplication sharedApplication].delegate ;
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [AQHelpers setScrollVwContentSize:self.vwScroll];
    [self setHeaderView];
    [self setFontAndColor];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setHeaderView
{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"MEDIOCRE MESSAGES"];
    header.delegate=self;
    [self.vwHeader addSubview:header.view];
}

-(void)clickToMenu
{
    [self leftDrawerButtonPress:nil];
}

-(void)leftDrawerButtonPress:(id)sender
{
    [appObj.drawerObj toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished)
    {
        if (finished)
        {
            
        }
    }];
}

-(void)clickToBack
{
    
}

-(void)setFontAndColor
{
     self.vwScroll.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.vwFooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblFooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblFooterDan.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblSendAudio.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.lblSendPhoto.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.lblSendVideo.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
}

- (IBAction)btnSendAudioClicked:(id)sender
{
        AQSendAudioScreen *screen = [[AQSendAudioScreen alloc]initWithNibName:@"AQSendAudioScreen" bundle:nil];
        [self.navigationController pushViewController:screen animated:YES];
   
  
}

- (IBAction)btnSendPhotoClicked:(id)sender
{
    AQSendImageScreen *screen = [[AQSendImageScreen alloc]initWithNibName:@"AQSendImageScreen" bundle:nil];
    [self.navigationController pushViewController:screen animated:YES];
}

- (IBAction)btnSendVideoClicked:(id)sender
{
    AQSendVideoScreen *screen = [[AQSendVideoScreen alloc]initWithNibName:@"AQSendVideoScreen" bundle:nil];
    [self.navigationController pushViewController:screen animated:YES];
}
@end
