//
//  AQSendAudioScreen.h
//  MediocreApp
//
//  Created by Swati Sharma on 10/03/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AQSendAudioScreen : UIViewController
@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (strong, nonatomic) IBOutlet UIButton *btnRecord;
- (IBAction)btnRecordClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *vwfooter;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterTom;
@property (strong, nonatomic) IBOutlet UILabel *btnFooterDan;






@end
