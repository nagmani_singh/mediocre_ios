//
//  AQMessageScreen.h
//  MediocreApp
//
//  Created by Swati Sharma on 09/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AQMessageScreen : UIViewController
@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (strong, nonatomic) IBOutlet UILabel *lblSendAudio;
- (IBAction)btnSendAudioClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblSendPhoto;
- (IBAction)btnSendPhotoClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblSendVideo;
- (IBAction)btnSendVideoClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *vwFooter;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterTom;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterDan;

@property (weak, nonatomic) IBOutlet UIButton *btnSndAudio;
@property (weak, nonatomic) IBOutlet UIButton *btnSndPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnSndVideo;






@end
