//
//  AQPodcastDetailScreen.h
//  MediocreApp
//
//  Created by Swati Sharma on 09/03/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AppAPI/PodCasts.h>
#import <MediaPlayer/MediaPlayer.h>
@interface AQPodcastDetailScreen : UIViewController
@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (strong, nonatomic) IBOutlet UIView *vwTop;
@property (strong, nonatomic) IBOutlet UIView *vwbottom;
@property (strong, nonatomic) IBOutlet UIView *vwFooter;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterTom;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterdan;

@property (strong, nonatomic) IBOutlet UILabel *lblStartDuration;
@property (strong, nonatomic) IBOutlet UILabel *lblEndDuration;
@property (strong, nonatomic) IBOutlet UIButton *btnPlay;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIButton *btnPrevious;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlay;
@property(strong,nonatomic)NSString *strTopImage;
@property(strong,nonatomic)NSString *strHeader;
@property (strong, nonatomic) IBOutlet UISlider *slider;

@property(strong,nonatomic)AVPlayerItem *playerItem;
@property(strong,nonatomic)NSTimer *timer;
@property(assign,nonatomic)BOOL scrubbing;
@property(assign,nonatomic)BOOL isPaused;
@property(strong,nonatomic)AVPlayer *playerGlobal;
-(IBAction)btnPreviousClick:(id)sender;
-(IBAction)btnNextClick:(id)sender;
-(IBAction)btnPlayClick:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblPodDate;
@property(assign,nonatomic)BOOL isIntialLoad;
@property (weak, nonatomic) IBOutlet UIImageView *imgNext;
@property (weak, nonatomic) IBOutlet UIImageView *imgPrevious;
@property (strong, nonatomic) IBOutlet UILabel *lblPodTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgTopImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgLine;
@property (assign, nonatomic)  BOOL isFromCorporatePodcast;
@property (weak, nonatomic) IBOutlet UIView *vwCorporateFooter;

-(id)initWithData:(PodCasts*)podCastValue topImage:(NSString*)strImg header:(NSString*)headerValue;

@end
