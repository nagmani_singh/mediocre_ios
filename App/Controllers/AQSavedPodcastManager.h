//
//  AQSavedPodcastManager.h
//  TomandDanMediocreApp
//
//  Created by Pankaj Kumar on 03/08/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppAPI/PodCasts.h>

@interface AQSavedPodcastManager : NSObject
@property (nonatomic,strong) NSMutableArray * allPodcastArray;
-(void)saveOrUpdatePodcast:(PodCasts *)podcast;
-(void)savePodcast:(PodCasts *)podcast;
+ (id)sharedManager;
-(double)getAudioPlayingPosition:(PodCasts *)podcast;
-(void)savingPodcastOnTermination;
-(void)getUserData;
@end
