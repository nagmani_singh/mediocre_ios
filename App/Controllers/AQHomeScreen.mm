//
//  AQHomeScreen.m
//  MediocreApp
//
//  Created by Swati Sharma on 07/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQHomeScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
#import "AQGlobalClass.h"
#import "AQLiveStreamScreen.h"
#import <AquevixCore/AQNSExtensions.h>
#import <AquevixCore/AQUIExtensions.h>
@interface AQHomeScreen ()<AQHeaderDelegate>
{
    AppDelegate *appObj;
    AQHeaderView *header;
    NSDictionary *dictAdsDetails;
}
@property (weak, nonatomic) IBOutlet UIImageView *liveFridayLabel;
@end

@implementation AQHomeScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appObj=[UIApplication sharedApplication].delegate ;
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [AQHelpers setScrollVwContentSize:self.scrollvw];
    [self setHeaderView];
    [self setFontAndColor];
    [self setAdsData];
    
    /// home screen
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveToHomeScreen)];
    [singleTap setNumberOfTapsRequired:1];
    [self.liveFridayLabel addGestureRecognizer:singleTap];
    

}

-(void)moveToHomeScreen
{
    
    AQLiveStreamScreen *screen = [[AQLiveStreamScreen alloc]initWithNibName:@"AQLiveStreamScreen" bundle:nil];
    [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
    
//    AQLiveStreamScreen *liveStreamObj=[[AQLiveStreamScreen alloc]initWithNibName:@"AQLiveStreamScreen" bundle:nil];
//    [self.navigationController pushViewController:liveStreamObj animated:YES];
    
    
    
}


-(void)setAdsData
{

    if (!AqGlobal::isFirstTime) {
        AqGlobal::isFirstTime=true;

        NSMutableArray *arrForAds = [[NSMutableArray alloc]init];
        arrForAds = [AQHelpers unarchieveAndRetrieveObjectWithData];
        dictAdsDetails = [[NSDictionary alloc]init];
        if (arrForAds.count!=0) {
                   
        dictAdsDetails = [arrForAds objectAtIndex:0];
        }
        NSString *imgUrl = [NSString stringWithFormat:@"%@",[dictAdsDetails objectForKey:@"ImageURL"]];
        if (![AQHelpers IsNull:imgUrl]) {
            
            [self.vwAds setHidden:NO];
            [AQUIExtensions sharedInstance].activity=self.activityIndicatorForAds;
            [self.imgAds loadImageFromURL:imgUrl andActivityIndicator:nil];
            self.lblAdsTitle.text=[NSString stringWithFormat:@"%@",[dictAdsDetails objectForKey:@"Company"]];
        }
        
    }

}



-(void)setHeaderView
{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"HOME"];
    header.delegate=self;
    [self.vwHeader addSubview:header.view];
    header.btnMenu.hidden=false;
}



-(void)clickToBack{
    
}
-(void)clickToMenu
{
     [self leftDrawerButtonPress:nil];
}

-(void)leftDrawerButtonPress:(id)sender
{
    [appObj.drawerObj toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        if (finished) {
        }
    }];
}



-(void)setFontAndColor{
    self.scrollvw.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
     self.vwFooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblCallTheShow.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.lblNumber.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.lblFooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblFooterDanCom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblAdsTitle.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblAdsTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
     self.vwAds.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCallShowPIMPSClicked:(id)sender{
    NSString *URLString = [@"tel://" stringByAppendingString:PHONENUMBER];
    NSURL *URL = [NSURL URLWithString:URLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:URL])
    {
        [[UIApplication sharedApplication] openURL:URL];
    }else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Error Occured" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
- (IBAction)btnSignUpClicked:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLFORREGISTER]];
}
- (IBAction)btnCloseAdsClicked:(id)sender
{
    [appObj.drawerObj setMaximumLeftDrawerWidth:260];
    [self.vwAds setHidden:YES];
}
- (IBAction)btnadsImagesClicked:(id)sender
{
    
    NSString *Url = [NSString stringWithFormat:@"%@",[dictAdsDetails objectForKey:@"URL"]];
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:Url]];
}
@end
