//
//  AQSendImageScreen.h
//  MediocreApp
//
//  Created by Swati Sharma on 10/03/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AQSendImageScreen : UIViewController
@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (strong, nonatomic) IBOutlet UIButton *btnCamera;
- (IBAction)btnCameraClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnGallery;
- (IBAction)btnGalleryClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *vwfooter;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterTom;
@property (strong, nonatomic) IBOutlet UILabel *btnFooterDan;
@property (strong, nonatomic) UIImage *selectedImage;
@end
