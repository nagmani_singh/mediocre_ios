//
//  AQBGDownloader.h
//  MediocreApp
//
//  Created by Pankaj Kumar on 07/04/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppAPI/PodCasts.h>
#import "AQPodCastCell.h"

@interface AQBGDownloader : NSObject<NSURLSessionDataDelegate>
- (id)initWithAqpodcastCell:(AQPodCastCell *)podcast :(AQPodCastCell *)cell;

@end
