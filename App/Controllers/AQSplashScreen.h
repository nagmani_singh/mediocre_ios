//
//  AQSplashScreen.h
//  MediocreApp
//
//  Created by Swati Sharma on 07/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MMDrawerController/MMDrawerController.h>
#import <AquevixUI/AQUIActiveDetailViewController.h>
@interface AQSplashScreen : AQUIActiveDetailViewController
@property (strong, nonatomic) MMDrawerController    *drawerObj;
@end
