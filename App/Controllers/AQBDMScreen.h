//
//  AQBDMScreen.h
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQBDMCell.h"
#import <AquevixUI/AQUIActiveListViewController.h>
@interface AQBDMScreen : AQUIActiveListViewController<AQBDMDelegate>
@property (strong, nonatomic) IBOutlet AQBDMCell *cellBDM;
@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UITableView *tblBDM;
@property (strong, nonatomic) IBOutlet UIView *vwFooter;
@property (strong, nonatomic) IBOutlet UILabel *lblfooterTom;
@property (strong, nonatomic) IBOutlet UILabel *lblfooterdan;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
-(void)downloadedPrograssPodcast :(float)receivedata withExpectedData:(float)expectedData withCellID:(int)cellId withClass:(UIViewController *)isclass withURL:(NSURL *)url;
-(void)downloadFailedWithError:(int)index;
-(void)afterTaskComplete:(int)index;
-(void)clickToBtnDelete:(NSInteger)index;
-(void)scrollViewEndCallBack:(NSInteger)index;

@end
