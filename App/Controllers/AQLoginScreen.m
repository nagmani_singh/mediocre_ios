//
//  AQLoginScreen.m
//  MediocreApp
//
//  Created by Swati Sharma on 07/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQLoginScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
#import "AQBDMScreen.h"
#import <AppAPI/AQPostLoginRequest.h>
#import <AppAPI/AQPostLoginResponse.h>
#import <AppAPI/AQDataManager.h>
@interface AQLoginScreen ()<AQHeaderDelegate>
{
    AQHeaderView *header;
    AppDelegate *appObj;
}
@end

@implementation AQLoginScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.txtUserName setTintColor:[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1] ];
    [self.txtPassword setTintColor:[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1] ];

    
    appObj=[UIApplication sharedApplication].delegate ;
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [AQHelpers setScrollVwContentSize:self.scrollvw];
    [self setHeaderView];
    [self setFontAndColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setHeaderView{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"#BDM"];
    header.delegate=self;
    [self.vwHeader addSubview:header.view];
}

-(void)clickToMenu
{
    [self.txtPassword resignFirstResponder];
    [self.txtUserName resignFirstResponder];
    [self leftDrawerButtonPress:nil];
}

-(void)leftDrawerButtonPress:(id)sender
{
    [appObj.drawerObj toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        if (finished) {
        }
    }];
}
-(void)setFontAndColor{
    self.scrollvw.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.vwFooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    [self.btnLogin setTitleColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:0.8] forState:UIControlStateNormal];
     [self.btnSignUp setTitleColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:0.8] forState:UIControlStateNormal];
    self.lblFooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblFooterDomCom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    [self.txtPassword setValue:[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtUserName setValue:[UIColor getUIColorObjectFromHexString:COLOR_DARK_YELLOW alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    self.txtPassword.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.txtUserName.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
}

-(void)clickToBack
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtUserName)
    {
        [ self.txtPassword becomeFirstResponder];
    }
    else if (textField== self.txtPassword)
    {
        [textField resignFirstResponder];
        [self postLoginRequest];
    }
    return true;
}


- (IBAction)btnLoginClicked:(id)sender
{
    
    if ([AQHelpers IsNull:self.txtUserName.text])
    {
        AQAlertView(@"", @"Please enter email");
    }
    else if([AQHelpers IsNull:self.txtPassword.text])
    {
        AQAlertView(@"", @"Please enter password");
    }
    else{
        [self.txtUserName resignFirstResponder];
        [self.txtPassword resignFirstResponder];
       [self postLoginRequest];
        
    }
}


//PostLogin Request Method
-(void)postLoginRequest
{
     if ([AQHelpers isNetworkAvailable])
     {
    [AQHelpers showActivtyIndicator:self];
    AQPostLoginRequest *request=[[AQPostLoginRequest alloc]init];
    [request setBaseUrl:SERVERAPIBASEURL];
    [request setServiceUriMiddleValue:SERVERAPIMEDILEURL];
    [request setUser_email:self.txtUserName.text];
    [request setUser_password:self.txtPassword.text];
    [AQDataManager getMasterData:request controller:self];
     }
     else
     {
         UIAlertView *alertConnection=[[UIAlertView alloc]initWithTitle:@"Connection Failed" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alertConnection show];
     }
}


-(void)parse:(id)obj
{
    AQPostLoginResponse *response=obj;
    if ([response getisValid])
    {
        NSDictionary *dictLogin=response.dictUserDetailVal;
        NSString *errorMessage =[NSString stringWithFormat:@"%@",response.error.message];
        if ([AQHelpers IsNull:errorMessage])
        {
            
            NSString *authkey = [NSString stringWithFormat:@"%@",[dictLogin objectForKey:AUTHKEY]];
            NSString *userLoggedin = [NSString stringWithFormat:@"%@",[dictLogin objectForKey:ISUSERLOGGEDIN]];
            NSString *userActive = [NSString stringWithFormat:@"%@",[dictLogin objectForKey:ISUSERACTIVE]];
            [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"isuserActiveKey"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        if (![AQHelpers IsNull:authkey])
            [[NSUserDefaults standardUserDefaults]setObject:authkey forKey:AUTHKEY];
        
        if (![AQHelpers IsNull:userLoggedin])
            [[NSUserDefaults standardUserDefaults]setObject:userLoggedin forKey:ISUSERLOGGEDIN];

        if (![AQHelpers IsNull:userActive])
        {
        [[NSUserDefaults standardUserDefaults]setObject:userActive forKey:ISUSERACTIVE];
            
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[NSUserDefaults standardUserDefaults]setObject:self.txtUserName.text forKey:LOGINEMAILID];
        [self goToNextScreen];
        }else
        {
            AQAlertView(@"Alert!",@"Please Login Again");

        }
            
        }
        else
        {
            AQAlertView(@"Error!", response.error.message);
        }
    }
 [AQHelpers hideActivityIndicator:self];
}

-(void)goToNextScreen
{
    AQBDMScreen *screen = [[AQBDMScreen alloc]initWithNibName:@"AQBDMScreen" bundle:nil];
    [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
}


- (IBAction)btnSignUpClicked:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLFORREGISTER]];
}


@end
