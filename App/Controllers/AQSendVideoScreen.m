//
//  AQSendVideoScreen.m
//  
//
//  Created by Swati Sharma on 10/03/16.
//
//

#import "AQSendVideoScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "AQPlayerMnager.h"

@interface AQSendVideoScreen ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,MFMailComposeViewControllerDelegate,AQHeaderDelegate>
{
 
    NSURL                           *urlSavedAudio;
     MFMailComposeViewController     *mailPicker;
    NSTimer                         *audioTimer;
    BOOL                            isAudioStopped;
    NSString                        *subjectEmail;
    AQHeaderView                   *header;
    AppDelegate                    *appObj;
    UIImagePickerController         *pickerController;
    NSString                        *sentFileName;
    BOOL isPlayingPlayer;

}
@end

@implementation AQSendVideoScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    appObj=[UIApplication sharedApplication].delegate ;
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [AQHelpers setScrollVwContentSize:self.vwScroll];
    [self setHeaderView];
    [self setFontAndColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setHeaderView{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"SEND VIDEO"];
    header.delegate=self;
    [self.vwHeader addSubview:header.view];
    [header.imgMenu setHidden:YES];
    [header.imgBack setHidden:NO];
    [header.btnMenu setHidden:YES];
    [header.btnBack setHidden:NO];
}

-(void)setFontAndColor{
    self.vwfooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblFooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.btnFooterDan.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.vwScroll.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    [self.btnRecord setTitleColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:0.8] forState:UIControlStateNormal];
}

-(void)clickToMenu{
    
}

-(void)clickToBack{
   
    [self.navigationController popViewControllerAnimated:self];
}

- (IBAction)btnRecordClicked:(id)sender{
    if ([[AQPlayerMnager sharedManager] player].rate>0) {
        [[[AQPlayerMnager sharedManager] player]pause];
        isPlayingPlayer=YES;
    }
    [self initilizeVideoRecording];
}

-(void)initilizeVideoRecording
{
    self.videoURL=nil;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
        pickerController.allowsEditing = YES;
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        pickerController.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        
        [self presentViewController:pickerController animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    self.videoURL = info[UIImagePickerControllerMediaURL];
    [[[AQPlayerMnager sharedManager] player]play];

    [picker dismissViewControllerAnimated:YES completion:^{
    [self performSelectorInBackground:@selector(getNSDataAndOpenMail) withObject:nil];
       
    }];
    
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if (isPlayingPlayer==YES) {
        [[[AQPlayerMnager sharedManager] player]play];
        isPlayingPlayer=NO;
    }
    [picker dismissViewControllerAnimated:YES completion:nil];

}


-(void)getNSDataAndOpenMail
{
    NSError* error = nil;
    NSData* data = [NSData dataWithContentsOfURL:self.videoURL options:0 error:&error];
    [self performSelectorOnMainThread:@selector(openMailWithAttachment:) withObject:data waitUntilDone:YES];
}
-(void)openMailWithAttachment:(NSData*)data
{
    subjectEmail=@"Video";
    sentFileName=@"Video.MOV";


    if ([MFMailComposeViewController canSendMail])
    {
        mailPicker =[[MFMailComposeViewController alloc] init];
        mailPicker.mailComposeDelegate = self;
        [mailPicker setToRecipients:@[@"hello@tomanddan.com"]];

        [mailPicker setSubject:subjectEmail];
      //   [mailPicker setToRecipients:[NSArray arrayWithObject:EMAIL_ID]];

         NSLog(@"video File Length is %.05f length",(float)(int)[data length]);
        [mailPicker addAttachmentData:data mimeType:@"video/quicktime"
                             fileName:sentFileName];
        [mailPicker setMessageBody:nil isHTML:YES];
        [self presentViewController:mailPicker animated:YES completion:nil];

    }
    else {
         AQAlertView(nil, ERROR_EMAIL_CONFIGURE);
         [self.navigationController popViewControllerAnimated:YES];
    }

 }
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString *message=@"";
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = @"";
            break;
        case MFMailComposeResultSent:
            message = @"Email has been sent.";
            break;
        case MFMailComposeResultFailed:
            message=@"Email cannot be sent at this time due to an unknown system problem. Please try again later.";
            break;
    }
    if (message.length>0) {
    
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Email" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];

    }
    [self dismissViewControllerAnimated:YES completion:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

@end
