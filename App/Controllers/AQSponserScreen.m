//
//  AQSponserScreen.m
//  TomandDanMediocreApp
//
//  Created by techugo on 25/03/18.
//  Copyright © 2018 com.aquevix. All rights reserved.
//

#import "AQSponserScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
#import "AppAPI.h"
#import "SponsorsCell.h"
#import "AQUIExtensions.h"

@interface AQSponserScreen ()<AQHeaderDelegate,NSURLSessionDelegate,UICollectionViewDelegate,UICollectionViewDataSource>{
    AQHeaderView *header;
    AppDelegate *appObj;
    NSArray *sponserlist;
    CGSize screenSize;
    
}

@end

@implementation AQSponserScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appObj=[UIApplication sharedApplication].delegate ;
    //[self.collectionMain registerClass:[SponsorsCell class] forCellWithReuseIdentifier:@"SponsorsCell"];
    [self.collectionMain registerNib:[UINib nibWithNibName:@"SponsorsCell" bundle:nil] forCellWithReuseIdentifier:@"SponsorsCell"];
    [self getSponsersList];
    [self setHeaderView];
    [self setFontAndColor];
    
}


-(void)setHeaderView{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"SPONSORS"];
    header.delegate=self;
    [self.headerView addSubview:header.view];
}
-(void)clickToBack{
    
}
-(void)clickToMenu
{
    [self leftDrawerButtonPress:nil];
}

-(void)leftDrawerButtonPress:(id)sender
{
    [appObj.drawerObj toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished)
     {
         if (finished)
         {
         }
     }];
}

-(void)setFontAndColor
{
    screenSize=[[UIScreen mainScreen]bounds].size;
    self.view.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.footerView.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblfooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblfooterdan.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
}

-(void)getSponsersList
{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"http://mediocre.dev.aquevix.com/ApiController.php?action=getsponsor"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[[Application instance]appKey] forHTTPHeaderField:@"X-APPKEY"];
    [request addValue:[[Application instance] phoneType] forHTTPHeaderField:@"X-DEVICE_TYPE"];
    [request addValue:[[Application instance] deviceID] forHTTPHeaderField:@"X-DEVICEID"];
    [request addValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"Authkey"] forHTTPHeaderField:@"X-AUTHKEY"];
    [request addValue:[[Application instance] versionString] forHTTPHeaderField:@"X-APP_VERSION"];
    
    
    [request setHTTPMethod:@"GET"];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error == nil)
        {
            
            NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
            NSLog(@"%@",text);
            
            NSError *error = nil;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"%@",json);
            sponserlist = json[@"GetSponsor"];
            if(error!=nil)
            {
                NSLog(@"error = %@",error);
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionMain reloadData];
            });
        }
        else{
            
            NSLog(@"Error : %@",error.description);
            
        }
        
        
    }];
    
    
    [postDataTask resume];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return sponserlist.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SponsorsCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"SponsorsCell" forIndexPath:indexPath];
    NSString *imgURL = sponserlist[indexPath.row][@"ImageURL"];
    [cell.imgCell loadImageFromURL:imgURL andActivityIndicator:nil];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(screenSize.width/2.0, screenSize.width/2.0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sponserlist[indexPath.row][@"LinkURL"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
