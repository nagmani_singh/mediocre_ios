//
//  AQEventDetailScreen.m
//  TomandDanMediocreApp
//
//  Created by Pankaj Kumar on 26/10/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import "AQEventDetailScreen.h"
#import "AQHeaderView.h"
#import "AppDelegate.h"
#import "AQHelpers.h"
#import "AQConfig.h"
@interface AQEventDetailScreen ()<AQHeaderDelegate>
{
    AQHeaderView *header;
    AppDelegate *appObj;
}

@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UIView *vwFooter;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollVW;
@property (strong, nonatomic) IBOutlet UILabel *lblfooterTom;
@property (strong, nonatomic) IBOutlet UILabel *lblfooterdan;
@end

@implementation AQEventDetailScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    appObj=[UIApplication sharedApplication].delegate ;
    [self setHeaderView];
    [self setFontAndColor];
    [self setdetailsWithEventObj:self.eventsObj];
    [_webVW.scrollView setScrollEnabled:NO];
    
    UITapGestureRecognizer *tapGestureOverAddress=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(lblAddressClciked:)];
    [_lblAddress addGestureRecognizer:tapGestureOverAddress];
    
}


-(void)lblAddressClciked:(id)sender
{
    NSString* addr = [NSString stringWithFormat:@"http://maps.apple.com/maps?q=%@",_lblAddress.text];
    NSURL* url = [[NSURL alloc] initWithString:[addr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL:url];
    
//    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"maps://maps.apple.com/?q=%@",_lblAddress.text]]];
    
    
    
}



-(void)setdetailsWithEventObj:(Events *)event
{
    self.lblEventTitle.text=event.EventTitle;
   self.lblDateTime.text= [self convertStrDateToDesiredDate:event.datetime datefrom:event.TimeFrom dateTo:event.TimeTo];
    self.lblCompany.text=event.Company;
    self.lblAddress.text=event.Address;
    self.lblEventTitle.text=event.EventTitle;
    [self.webVW setDelegate:self];
    [self.webVW.scrollView setBackgroundColor:[UIColor getUIColorObjectFromHexString:@"891A1C" alpha:1.0]];
    [_webVW setHidden:YES];
    [self.webVW setDataDetectorTypes:UIDataDetectorTypeLink];
      [self.webVW loadHTMLString:[NSString stringWithFormat:@"<body style=\"background-color: #891A1C;color: #FAAE17; text-align: center;padding: 10px;word-wrap:break-word;><div style=\"background-color: #891A1C; color: #FAAE17; text-align: center;>%@</div><style>a{text-decoration:none;color:#d61d24}</style></body>", [event.Description stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"]] baseURL:nil];
//    [self.webVW loadHTMLString:[NSString stringWithFormat:@"<body style=\"background-color: #891A1C;color: #FAAE17; text-align: center;padding: 10px;word-break:break-all;><div style=\"background-color: #891A1C; color: #FAAE17; text-align: center;word-break:break-all;>%@</div></body>", [event.Description stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"]] baseURL:nil];

//    [self.webVW loadHTMLString:event.Description baseURL:nil];
}

-(void)setHeaderView
{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"EVENTS"];
    [header setDelegate:self];
    [self.vwHeader addSubview:header.view];
    
    [header.imgMenu setHidden:YES];
    [header.imgBack setHidden:NO];
    [header.btnMenu setHidden:YES];
    [header.btnBack setHidden:NO];
 
}

-(void)clickToBack
{
    [self.navigationController popViewControllerAnimated:YES];

}

-(void)setFontAndColor
{
    self.view.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.vwFooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblfooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblfooterdan.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    
}

-(NSString *)convertStrDateToDesiredDate:(NSString *)strDate datefrom:(NSString *)strFrom dateTo:(NSString *)strto
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *date=[dateFormatter dateFromString:strDate];
//    dateFormatter.dateFormat=@"MMMM";
//    NSString *monthString = [[dateFormatter stringFromDate:date] capitalizedString];
//    NSLog(@"month: %@", monthString);
//    
//    dateFormatter.dateFormat=@"EEEE";
//    NSString *dayString = [[dateFormatter stringFromDate:date] capitalizedString];
//    NSLog(@"day: %@", dayString);
    NSDateComponents *dateComponent=[[NSCalendar currentCalendar]components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitWeekday fromDate:date];
  
   NSString *string= [NSString stringWithFormat:@"%@, %@ %lu %lu \n%@ - %@",[[[dateFormatter weekdaySymbols]objectAtIndex:[dateComponent weekday]-1] uppercaseString],[[[dateFormatter monthSymbols]objectAtIndex:[dateComponent month]-1] uppercaseString],(long)[dateComponent day],(long)[dateComponent year],strFrom,strto];
    return string;
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType==UIWebViewNavigationTypeLinkClicked)
    {
    
        [[UIApplication sharedApplication]openURL:request.URL];
        return NO;

    }else
    {
        return YES;

    }
    
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{

    [_scrollVW setContentSize:CGSizeMake(self.view.frame.size.width, _scrollVW.frame.size.height + webView.scrollView.contentSize.height-webView.frame.size.height )];
    [webView setFrame:CGRectMake(webView.frame.origin.x, webView.frame.origin.y, webView.frame.size.width, webView.scrollView.contentSize.height)];

    [_webVW setHidden:NO];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
