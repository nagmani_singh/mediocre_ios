//
//  AQMenuScreen.h
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQMenuCell.h"
#import <AquevixUI/AQUIActiveListViewController.h>
@interface AQMenuScreen : AQUIActiveListViewController{
    NSArray *arrMenuList;
}
@property (strong, nonatomic) IBOutlet AQMenuCell *cellMenu;
@property (strong, nonatomic) IBOutlet UITableView *TblMenu;
@property (strong, nonatomic) IBOutlet UIView *vwFooter;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterTom;
@property (strong, nonatomic) IBOutlet UILabel *lblFooterDanCom;

@end
