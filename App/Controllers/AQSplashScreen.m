//
//  AQSplashScreen.m
//  MediocreApp
//
//  Created by Swati Sharma on 07/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQSplashScreen.h"
#import "AQHelpers.h"
#import "AQHomeScreen.h"
#import "AQConfig.h"
#import "AppDelegate.h"

#import <AppAPI/AQGetAdsRequest.h>
#import <AppAPI/AQGetAdsResponse.h>
#import <AppAPI/AQDataManager.h>
#import <AppAPI/Ads.h>
@interface AQSplashScreen ()
{
    Ads *objAds;
}
@end

@implementation AQSplashScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    self.view.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    sleep(2);
     [self getAdsApi];
    
}

-(void)SetHomeScreen
{
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app setMMDrawerController];
    [self.navigationController pushViewController:app.drawerObj animated:true];
}


-(void)getAdsApi
{
     if ([AQHelpers isNetworkAvailable])
     {
         //[AQHelpers showActivtyIndicator:self];
    AQGetAdsRequest *request=[[AQGetAdsRequest alloc]init];
    [request setBaseUrl:SERVERAPIBASEURL];
    [request setServiceUriMiddleValue:SERVERAPIMEDILEURL];
    [AQDataManager getMasterData:request controller:self];
     }
     else
     {
    [self SetHomeScreen];
     }
}

-(void)bind:(id)obj
{
    AQGetAdsResponse *response=obj;
    if ([response getisValid])
    {
        NSString *errorMessage = [NSString stringWithFormat:@"%@",response.error];
        if ([AQHelpers IsNull:errorMessage]) {
        NSMutableArray *arrAds = [[NSMutableArray alloc]init];
        arrAds = [response Ads];
            NSMutableArray *arrSave = [[NSMutableArray alloc]init];
        for (int i =0; i<arrAds.count; i++)
        {
            objAds=[[Ads alloc]init];
            objAds=[arrAds objectAtIndex:i];
            [arrSave addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",objAds.imageUrl],@"ImageURL",[NSString stringWithFormat:@"%@",objAds.company],@"Company",[NSString stringWithFormat:@"%ld",(long)objAds.id],@"id",[NSString stringWithFormat:@"%@",objAds.url],@"URL", nil]];
                
                
        }
        [AQHelpers archieveAndSaveObjectWithData:arrSave];
        }
        else {
           // AQAlertView(<#title#>, <#msg#>)
        }
        
    }
   [self SetHomeScreen];
    //[AQHelpers hideActivityIndicator:self];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
