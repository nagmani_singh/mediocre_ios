//
//  AQCorporatePodcastScreen.h
//  TomandDanMediocreApp
//
//  Created by Pankaj Kumar on 26/07/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQPodCastCell.h"
#import <AquevixUI/AQUIActiveListViewController.h>
@interface AQCorporatePodcastScreen : AQUIActiveListViewController<AQPodCastDelegate>
@property (strong, nonatomic) IBOutlet AQPodCastCell *cellPodCast;
@property (strong, nonatomic) IBOutlet UIView *vwHeader;
@property (strong, nonatomic) IBOutlet UITableView *tblPodscast;
@property (strong, nonatomic) IBOutlet UIView *vwFooter;
@property (strong, nonatomic) IBOutlet UILabel *lblfooterTom;
@property (strong, nonatomic) IBOutlet UILabel *lblfooterdan;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
-(void)downloadedPrograssPodcast :(float)receivedata withExpectedData:(float)expectedData withCellID:(int)cellId withClass:(UIViewController *)isclass withURL:(NSURL *)url;
-(void)downloadFailedWithError:(int)index;
-(void)afterTaskComplete:(int)index;
-(void)clickToBtnDelete:(NSInteger)index;
-(void)scrollViewEndCallBack:(NSInteger)index;

@end
