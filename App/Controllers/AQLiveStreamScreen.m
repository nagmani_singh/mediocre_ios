//
//  AQLiveStreamScreen.m
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQLiveStreamScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
#import "NSDate+ServerDate.h"
#import "AQPlayerMnager.h"

@interface AQLiveStreamScreen ()<AQHeaderDelegate,AVAudioPlayerDelegate>
{
    AQHeaderView                    *header;
    AppDelegate                     *appObj;
    NSTimer                         *audioTimer;
    NSTimeInterval                  secondsBetween2Dates;
    BOOL isPaused;
}
@end

@implementation AQLiveStreamScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
        appObj=[UIApplication sharedApplication].delegate ;
        [self setAutomaticallyAdjustsScrollViewInsets:NO];
        [AQHelpers setScrollVwContentSize:self.scrollvw];
        //[self stopTimer];
        //[self pauseAudioTimer];
        //[self.btnStartStreaming setEnabled:NO];
        [self setFontAndColor];
        [self setHeaderView];
        //[self setDayHourMins];
        //[self settimerForHoursDayMins];
        //[self setPlayerForLiveStreaming];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
//    MPMediaItemArtwork *artwork = [[MPMediaItemArtwork alloc]initWithImage:[UIImage imageNamed:@"img_mediocre.png"]];
//    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",@"LIVE STREAM"], MPMediaItemPropertyTitle,
//                                                             artwork, MPMediaItemPropertyArtwork, nil];
//    [self becomeFirstResponder];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    [[UIApplication sharedApplication]endReceivingRemoteControlEvents];
//    [self resignFirstResponder];

}

- (IBAction)ClickToTwitchLiveStream:(UIButton *)sender {
    
    NSString *customURL = @"twitch://";
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:customURL]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
        
    }else{
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.twitch.tv/tomanddanlive"]];
    }
    
}


-(void)settimerForHoursDayMins
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:60.0 target:self selector:@selector(setDayHourMins) userInfo:nil repeats:YES];
}

-(void)setDayHourMins
{
    
    NSTimeZone *outputTimeZone =  [[NSTimeZone alloc] initWithName:@"America/New_York"];
    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
   // outputDateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];

    [outputDateFormatter setTimeZone:outputTimeZone];
    [outputDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *currentOutputString = [outputDateFormatter stringFromDate:[NSDate serverDate]];
    
    NSDate *currDate = [AQHelpers convertStringToDate:currentOutputString];
    NSDate *fridayDate = [self getFridayDateFromCurrentdate:[NSDate serverDate]];
    
    
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    [calendar setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:fridayDate];
    
    [components setHour:14];
    [components setMinute:0];
    [components setSecond:0];
    
    NSDate *friday2pm = [calendar dateFromComponents:components];
    NSTimeZone* sourceTimeZone = [[NSTimeZone alloc] initWithName:@"America/New_York"];
    NSTimeZone* destinationTimeZone = [[NSTimeZone alloc] initWithName:@"America/New_York"];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:friday2pm];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:friday2pm];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:friday2pm];
    secondsBetween2Dates = [destinationDate timeIntervalSinceDate:currDate];
    if (secondsBetween2Dates<0)
    {
        
        NSInteger Num = [self findWeekday:currDate]+1;
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
        NSDate *newDate1 = [currDate dateByAddingTimeInterval:60*60*24*Num];
        
        NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:newDate1];
        //[calendar setTimeZone:[NSTimeZone localTimeZone]];
        
//         [components setWeekday:(Num+7)];
        [components setHour:14];
       
       
        
        NSDate *friday2pm = [calendar dateFromComponents:components];
        NSTimeZone* sourceTimeZone = [[NSTimeZone alloc] initWithName:@"America/New_York"];
        NSTimeZone* destinationTimeZone = [[NSTimeZone alloc] initWithName:@"America/New_York"];
        
        NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:friday2pm];
        NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:friday2pm];
        NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
        NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:friday2pm];
        
        secondsBetween2Dates = [destinationDate timeIntervalSinceDate:currDate];
        
    }
    int numberOfDays = secondsBetween2Dates / 86400;
    int Hours = secondsBetween2Dates / (60 * 60);
    int finalHours = Hours%24;
    int Mins = secondsBetween2Dates/60;
    int FinalMins = Mins%60;
    
    if (numberOfDays<0) {
        numberOfDays=0;
    }
    if (finalHours<1) {
        finalHours=0;
    }
    if (FinalMins<0) {
        FinalMins=0;
    }
    
    self.lblDays.text= [NSString stringWithFormat:@"%d Days",numberOfDays];
    self.lblMins.text= [NSString stringWithFormat:@"%d Mins",FinalMins];
    if (finalHours==1) {
        self.lblHours.text= [NSString stringWithFormat:@"%d Hour",finalHours];
    }
    else{
        self.lblHours.text= [NSString stringWithFormat:@"%d Hours",finalHours];
    }
    
    if (numberOfDays==0 && finalHours==0 && FinalMins==0) {
        [self setDataIfLiveStrteamingWorking];
    }
}

-(NSInteger)findWeekday:(NSDate*)date
{
  
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit fromDate:date];
    [calendar setTimeZone:[[NSTimeZone alloc] initWithName:@"America/New_York"]];
    //NSTimeZone *tz = [[NSCalendar currentCalendar] timeZone];
    NSUInteger weekdayToday = [components weekday];
//    NSInteger daysToFriday = (6 - weekdayToday) % 7;

    
    
    return weekdayToday;
    
}


-(NSDate*)getFridayDateFromCurrentdate:(NSDate*)currDate
{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit fromDate:currDate];
    [calendar setTimeZone:[[NSTimeZone alloc] initWithName:@"America/New_York"]];
    //NSTimeZone *tz = [[NSCalendar currentCalendar] timeZone];
    NSUInteger weekdayToday = [components weekday];
    NSInteger daysToFriday = (6 - weekdayToday) % 7;
    
    NSDate *nextFriday = [currDate dateByAddingTimeInterval:60*60*24*daysToFriday];
    return nextFriday;
}

-(void)stopTimer
{
    [self.timer invalidate];
    self.timer = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setHeaderView
{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"LIVE STREAM"];
    header.delegate=self;
    [self.vwHeader addSubview:header.view];

}
-(void)clickToBack{
    
}

-(void)clickToMenu
{
    [self leftDrawerButtonPress:nil];
//    [self.player pause];
    [self pauseAudioTimer];
   // [self stopTimer];
    //[self setDataIfLiveStrteamingWorking];
    
}

-(void)leftDrawerButtonPress:(id)sender
{
    [appObj.drawerObj toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        if (finished) {
        }
    }];
}

-(void)setFontAndColor{
    self.scrollvw.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.vwFooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    [self.btnStartStreaming setTitleColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:0.8] forState:UIControlStateNormal];
    self.lblFooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblFooterDanCom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblDays.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.lblMins.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.lblHours.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.lblDurations.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    self.lblStatus.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
}

- (IBAction)btnStartStreamingClicked:(id)sender
{
    if ((self.player.rate != 0) && (self.player.error == nil))
    {
        // player is playing
        [self.player pause];
        [self pauseAudioTimer];
        [self.btnStartStreaming setTitle:@"START STREAMING" forState:UIControlStateNormal];
        self.lblDurations.text = [NSString stringWithFormat:@"   DURATION:00:00:00"];
        self.lblStatus.text=@"STATUS:IDLE";
        [self.btnStartStreaming setUserInteractionEnabled:YES];
        isPaused=true;
    }
    else
    {
        [self.player play];
        [self startAudioTimer];
        [self.btnStartStreaming setTitle:@"STOP STREAMING" forState:UIControlStateNormal];
        self.lblStatus.text=@"STATUS:PLAYING";
        [self.btnStartStreaming setUserInteractionEnabled:YES];
        isPaused=false;
    }
}


-(void)setPlayerForLiveStreaming
{

    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *setCategoryError = nil;
    BOOL success = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
    
    NSError *activationError = nil;
    success = [audioSession setActive:YES error:&activationError];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAudioSessionInterruption:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:[AVAudioSession sharedInstance]];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
    {
    AVPlayer *player = [[AVPlayer alloc]initWithURL:[NSURL URLWithString:AUDIOLIVESTREAMINGURL]];
                       
    dispatch_async(dispatch_get_main_queue(), ^(void)
    {
      self.player = player;
        [[AQPlayerMnager sharedManager] setPlayerObj:self.player withPodcast:nil ];
        self.player=[[AQPlayerMnager sharedManager] player];
     [player.currentItem addObserver:self forKeyPath:@"status" options:0 context:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.player currentItem]];
        
            });
        });
}

-(void)handleAudioSessionInterruption:(NSNotification*)notification
{
  
    [self.btnStartStreaming setTitle:@"START STREAMING" forState:UIControlStateNormal];
  
    [self.player pause];
    isPaused = false;
}



- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self.player.currentItem && [keyPath isEqualToString:@"status"])
    {
        if (self.player.currentItem.status == AVPlayerStatusFailed)
        {
            NSLog(@"AVPlayer Failed");
            [self setDataIfLiveStrteamingNotWorking];
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No live broadcast detected, please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        } else if (self.player.currentItem.status == AVPlayerStatusReadyToPlay)
        {
            NSLog(@"AVPlayerStatusReadyToPlay");
            [self setDataIfLiveStrteamingWorking];
            
        } else if (self.player.currentItem.status == AVPlayerItemStatusUnknown)
        {
            NSLog(@"AVPlayer Unknown");
            
        }
    }else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"No live broadcast detected, please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}
- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    if (event.type == UIEventTypeRemoteControl)
    {
        if (event.subtype == UIEventSubtypeRemoteControlPlay)
        {
            [self.player play];
            [self.btnStartStreaming setTitle:@"STOP STREAMING" forState:UIControlStateNormal];
            
           
            isPaused = true;
        }
        
        else if (event.subtype == UIEventSubtypeRemoteControlPause)
        {
            [self.player pause];
           
            [self.btnStartStreaming setTitle:@"START STREAMING" forState:UIControlStateNormal];
           
            isPaused = false;
        }
        else if (event.subtype == UIEventSubtypeRemoteControlTogglePlayPause)
        {
            if (isPaused==false)
            {
                [self.btnStartStreaming setTitle:@"STOP STREAMING" forState:UIControlStateNormal];
             
                
                [self.player play];
                isPaused = true;
                
                
            } else
            {
                [self.btnStartStreaming setTitle:@"START STREAMING" forState:UIControlStateNormal];
              
                [self.player pause];
                isPaused = false;
            }
        }
        else if (event.subtype == UIEventSubtypeRemoteControlNextTrack)
        {
//            [self btnNextClick:playerGlobal.currentItem];
        }
        else if (event.subtype == UIEventSubtypeRemoteControlPreviousTrack)
        {
//            [self btnPreviousClick:playerGlobal.currentItem];
        }
    }
}



-(void)setDataIfLiveStrteamingWorking{
    [self.btnStartStreaming setEnabled:YES];

    [self.btnStartStreaming setTitle:@"START STREAMING" forState:UIControlStateNormal];
    [self.btnStartStreaming setUserInteractionEnabled:YES];
    self.lblDurations.text = [NSString stringWithFormat:@"   DURATION:00:00:00"];
    self.lblStatus.text = @"STATUS:IDLE";
}

-(void)setDataIfLiveStrteamingNotWorking
{
    
    [self.btnStartStreaming setTitle:@"LIVE STREAM" forState:UIControlStateNormal];
    [self.btnStartStreaming setEnabled:NO];
    [self.btnStartStreaming setUserInteractionEnabled:NO];
    self.lblDurations.text = [NSString stringWithFormat:@"   DURATION:00:00:00"];
    self.lblStatus.text = @"STATUS:IDLE";
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
    [self.player pause];
    [self pauseAudioTimer];
    [self setDataIfLiveStrteamingNotWorking];
}

-(void)startAudioTimer
{
    audioTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startTimeShowing) userInfo:nil repeats:YES];
}
-(void)startTimeShowing
{
    self.lblDurations.text = [NSString stringWithFormat:@"   DURATION:%@",[self timeFormat:[self getCurrentAudioTime]]];
   
}
-(void)pauseAudioTimer
{
    [audioTimer invalidate];
    audioTimer=nil;
}

- (NSTimeInterval)getCurrentAudioTime
{
    AVPlayerItem *currentItem = self.player.currentItem;
    NSTimeInterval currentTime = CMTimeGetSeconds(currentItem.currentTime);
    NSLog(@"get curr time %f",currentTime);
    return currentTime;
}

-(NSString*)timeFormat:(float)value{
    
//    int seconds = value % 60;
//    int minutes = (value / 60) % 60;
//    int hours = value / 3600;
    
    
    
    float minutes = floor(lroundf(value)/60);
    float seconds = lroundf(value) - (minutes * 60);
    float hours = floor(lroundf(value)/3600);
    
    NSInteger roundedSeconds = lroundf(seconds);
    NSInteger roundedMinutes = lroundf(minutes);
    NSInteger roundedhours =  lroundf(hours);
    NSString *time = [[NSString alloc]initWithFormat:@"%02ld:%02ld:%02ld",(long)roundedhours,(long)roundedMinutes,(long)roundedSeconds];
    [AQHelpers hideActivityIndicator:self];
    return time;
}

@end
