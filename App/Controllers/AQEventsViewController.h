//
//  AQEventsViewController.h
//  TomandDanMediocreApp
//
//  Created by Pankaj Kumar on 26/10/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AquevixUI/AQUIActiveListViewController.h>

@interface AQEventsViewController : AQUIActiveListViewController
@property (strong, nonatomic)  NSString *isUserLoggedIN;

@end
