//
//  AQPodcastScreen.m
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQPodcastScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AQPodcastDetailScreen.h"
#import "AppDelegate.h"
#import <AppAPI/AQGetPodCastsRequest.h>
#import <AppAPI/AQGetPodCastsResponse.h>
#import <AppAPI/AQDataManager.h>
#import "PodCastsDownload.h"
#import "AQBGDownloader.h"
#import "NTMonthYearPicker.h"

@interface AQPodcastScreen ()<AQHeaderDelegate,AQPodCastDelegate,UITableViewDelegate,UITableViewDataSource,podCastsDelegate>
{
    NSMutableArray *arrPodList;
    AQHeaderView *header;
    AppDelegate *appObj;
    float receivedData;
    long expectedBytes;
    NSString *currentUrl;
//    AQPodCastCell *cellPod;
    NSInteger indexDownloadBtn;
    NSMutableDictionary *dictPodCast;
    NSMutableArray *arrdownloadId;
    PodCasts *podCastObj;
    CGSize screenSize;
    BOOL isIntialLoad;
    NSInteger pageCount;
    NSInteger cellIndex;
    BOOL makeDeletable;
    BOOL isoffline;
    NSString *strForReferingMethod;
}
@property (weak, nonatomic) IBOutlet UIView *vwSearchIcon;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollVW;
@property (weak, nonatomic) IBOutlet UIView *vwdatePicker;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchicon;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic)   NTMonthYearPicker *datePicker;
@end

@implementation AQPodcastScreen

- (void)viewDidLoad {
    [super viewDidLoad];
       // Do any additional setup after loading the view from its nib.
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    appObj=[UIApplication sharedApplication].delegate ;
    pageCount=1;
    arrPodList=[[NSMutableArray alloc]init];
    screenSize=[[UIScreen mainScreen]bounds].size;
    isIntialLoad = true;
     [self.activityIndicator startAnimating];
//    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 3.0f);
//    _progressView.transform = transform;
//    [_progressView setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0]];
//    [_progressView setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0]];
    [self setNTdatePicker];
    [self getPodCastRequest];
   // [self setDummydata];
    [self setHeaderView];
    [self setFontAndColor];
    [self.tblPodscast registerClass:[AQPodCastCell class] forCellReuseIdentifier:@"cell"];
    [PodCastsDownload sharedInstance].delegate=self;
}



-(void)setNTdatePicker
{
    _datePicker=[[NTMonthYearPicker alloc]initWithFrame:CGRectMake(0, 0, _vwdatePicker.frame.size.width,_vwdatePicker.frame.size.height )];
    
    [_datePicker setMaximumDate:[NSDate date]];

    [_vwdatePicker addSubview:_datePicker];
    [_vwdatePicker insertSubview:_datePicker atIndex:0];

}



static NSString *cellIdentifier=@"cell";

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setHeaderView{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"PODCASTS"];
    header.delegate=self;
    [self.vwHeader addSubview:header.view];
}
-(void)clickToBack{
    
}
-(void)clickToMenu
{
    [self leftDrawerButtonPress:nil];
}

-(void)leftDrawerButtonPress:(id)sender
{
    [appObj.drawerObj toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        if (finished)
        {
            
        }
    }];
}

-(void)setFontAndColor
{
    self.view.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.vwFooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblfooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblfooterdan.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.tblPodscast.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    
}
    
// UITableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrPodList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSBundle mainBundle]loadNibNamed:@"AQPodCastCell" owner:self options:nil];
    AQPodCastCell *cellPod=[self.tblPodscast dequeueReusableCellWithIdentifier:@"cell"];
    cellPod=self.cellPodCast;
    [cellPod setDelegate:self];
    cellPod.btnCell.tag=indexPath.row+1;
    cellPod.btnDownload.tag=indexPath.row+1;
    cellPod.deleteBtn.tag=indexPath.row+1;
    cellPod.scrlVW.tag=indexPath.row+1;
    [cellPod bindData:arrPodList index:indexPath.row];
    cellPod.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *documentsPath    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *finalPath        = [documentsPath stringByAppendingPathComponent:cellPod.cellUrl.lastPathComponent];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSLog(@"%@",finalPath);
    if ([fileManager fileExistsAtPath:finalPath]==YES)
    {
        makeDeletable=YES;
        [cellPod.btnDownload setEnabled:NO];
         cellPod.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-disable.png"];
        [cellPod.cellProgress setProgress:1.0];
        if (indexPath.row%2!=0)
        {
            [cellPod.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0]];
        cellPod.imgDelete.image = [UIImage imageNamed:@"icon_delete_1.png"];
        }
        else
        {
            [cellPod.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0]];
        cellPod.imgDelete.image = [UIImage imageNamed:@"icon_delete_2.png"];
        }

    }else
    {
        makeDeletable=NO;
        [cellPod.deleteBtn setEnabled:makeDeletable];
        cellPod.imgDelete.image = [UIImage imageNamed:@"icon_delete_disabled.png"];
        //[cellPod.deleteBtn setBackgroundColor:[UIColor grayColor]];

    }
    
    
    return cellPod;


}

// table cell custom delegate method
-(void)clickToCellButton:(NSInteger)index
{
//    [AQHelpers disableAllEventTouch];
   podCastObj = [arrPodList objectAtIndex:index];
    NSIndexPath *sender = [NSIndexPath indexPathForRow:(index) inSection:0];
    NSIndexPath * path = (NSIndexPath *)sender;
       AQPodCastCell* cellPod = [self.tblPodscast cellForRowAtIndexPath:path];
    if (cellPod.isDownloading==NO)
    {
        AQPodcastDetailScreen *screen = [[AQPodcastDetailScreen alloc]initWithData:podCastObj topImage:@"img_mediocre.png" header:@"PODCASTS"];
        [self.navigationController pushViewController:screen animated:YES];
    }else
    {
        AQAlertView(nil, @"File is still downloading. Please try later");
    }
   
    
}


-(void)clickToDownloadButton:(NSInteger)index 
{
   
    cellIndex=index;
    podCastObj = [arrPodList objectAtIndex:index];
    NSIndexPath *sender = [NSIndexPath indexPathForRow:(index) inSection:0];
    NSIndexPath * path = (NSIndexPath *)sender;
   AQPodCastCell* cellPod = [self.tblPodscast cellForRowAtIndexPath:path];
    [cellPod.btnDownload setEnabled:NO];
//    cellPod.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-disable.png"];
    currentUrl=[NSString stringWithFormat:@"%@",podCastObj.url];
    NSURL *url = [NSURL URLWithString:currentUrl];
    receivedData =0;
    
    
    
    ////
    
    [[PodCastsDownload sharedInstance] downloadAudioStream:url :(int)index ];
    //[[PodCasts sharedInstance].allPodcastArray[index]setDownloadState:1];
    [PodCastsDownload sharedInstance].delegate=self;
       
  
    
    
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return makeDeletable;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

-(void)clickToBtnDelete:(NSInteger)index
{
 
     podCastObj = [arrPodList objectAtIndex:index];

     NSString *documentsPath    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
     
     
     NSURL *fileURL = [NSURL URLWithString:podCastObj.url];
     NSString *finalPath        = [documentsPath stringByAppendingPathComponent:fileURL.lastPathComponent];
     NSLog(@"%@",finalPath);
     NSFileManager *fileManager = [NSFileManager defaultManager];

     if ([fileManager fileExistsAtPath:finalPath])
     {
         if ([fileManager isDeletableFileAtPath:finalPath])
         {
             NSError *error;
             [fileManager removeItemAtPath:finalPath error:&error];
             if (error!=nil)
             {
                 AQAlertView(@"Error!", error.localizedDescription);
             }else{
                 if (isoffline==NO) {
                     
                 }else
                 {
                 [arrPodList removeObjectAtIndex:index];
                 }

                 NSMutableArray *arr=[[NSUserDefaults standardUserDefaults]objectForKey:@"arrayWithoutInternet"];
                 NSMutableArray *newarray=[[NSMutableArray alloc]init];
                 for (NSData *data in arr) {
                     PodCasts*    obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                     [newarray addObject:obj];
                 }
                 NSMutableArray *tempArray = [newarray mutableCopy];
                 
                 for (PodCasts *pods in newarray)
                 {
                     NSString *documentsPath    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                     
                     
                     NSURL *fileURL = [NSURL URLWithString:pods.url];
                     NSString *finalPath        = [documentsPath stringByAppendingPathComponent:fileURL.lastPathComponent];
                     NSLog(@"%@",finalPath);
                     NSFileManager *fileManager = [NSFileManager defaultManager];
                     if (![fileManager fileExistsAtPath:finalPath])
                     {
                         [tempArray removeObject:pods];
                     }
                     
                 }
                 newarray=tempArray;
                 
                 NSMutableArray *arrObj=[[NSMutableArray alloc]init];
                 for (PodCasts *podObj in newarray)
                 {
                     NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:podObj];
                     [arrObj addObject:personEncodedObject];

                 }
                [[NSUserDefaults standardUserDefaults]setObject:arrObj forKey:@"arrayWithoutInternet"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
                 AQPodCastCell* cellPod = [self.tblPodscast cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                 dispatch_async(dispatch_get_main_queue(),
                                ^{
                                    
                                    if (index%2!=0)
                                    {
                                        cellPod.vwbackground.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
                                        cellPod.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-2.png"];
                                        cellPod.imgTomDan.image = [UIImage imageNamed:@"icon_t&d-podcast-2.png"];
                                        [cellPod.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0]];
                                        [cellPod.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0]];
                                        
                                        
                                    }
                                    else{
                                        cellPod.vwbackground.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
                                        
                                        cellPod.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-1.png"];
                                        cellPod.imgTomDan.image = [UIImage imageNamed:@"icon_t&d-podcast-1.png"];
                                        [cellPod.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0]];
                                        [cellPod.cellProgress setTrackTintColor:[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0]];
                                    }
                                    
                                });
                
               
             }
         }
     }

        [self.tblPodscast reloadData];
    }


-(void)afterTaskComplete:(int)index withURL:(NSURL *)url
{
    NSIndexSet *indexes = [arrPodList indexesOfObjectsPassingTest:^BOOL(PodCasts * obj, NSUInteger idx, BOOL *stop){
        if ([obj isKindOfClass:[PodCasts class]]==YES)
        {
            NSString *s = [obj url];
            NSRange range = [s rangeOfString:url.absoluteString];
            return range.location != NSNotFound;
            
        }else
        {
            return nil;
        }
        
    }];
    
    NSIndexPath *sender = [NSIndexPath indexPathForRow:([indexes firstIndex]) inSection:0];
//    NSIndexPath *sender = [NSIndexPath indexPathForRow:(index) inSection:0];
    NSIndexPath * path = (NSIndexPath *)sender;
     AQPodCastCell* cellPod = [self.tblPodscast cellForRowAtIndexPath:path];
    podCastObj = [arrPodList objectAtIndex:[indexes firstIndex]];
    if (![[[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]allKeys]containsObject:@"arrayWithoutInternet"]) {
        NSMutableArray *arrObj=[[NSMutableArray alloc]init];
        NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:podCastObj];
        [arrObj addObject:personEncodedObject];
        [[NSUserDefaults standardUserDefaults]setObject:arrObj forKey:@"arrayWithoutInternet"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }else
    {
        NSMutableArray *arrObj=[[[NSUserDefaults standardUserDefaults]objectForKey:@"arrayWithoutInternet"] mutableCopy];
        NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:podCastObj];

        [arrObj addObject:personEncodedObject];
    [[NSUserDefaults standardUserDefaults]setObject:arrObj forKey:@"arrayWithoutInternet"];
    }
      dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [cellPod.btnDownload setEnabled:NO];
                       cellPod.isDownloading=NO;

                       [cellPod.deleteBtn setEnabled:YES];
//                       [cellPod.deleteBtn setBackgroundColor:[UIColor redColor]];
                       cellPod.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-disable.png"];
                       if (index%2!=0)
                       {
                           [cellPod.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0]];
                           cellPod.imgDelete.image = [UIImage imageNamed:@"icon_delete_1.png"];
                       }
                       else
                       {
                           [cellPod.cellProgress setProgressTintColor:[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0]];
                       cellPod.imgDelete.image = [UIImage imageNamed:@"icon_delete_2.png"];
                       }
                   });

   

}

-(void)downloadedPrograssPodcast :(float)receivedata withExpectedData:(float)expectedData withCellID:(int)cellId withClass:(UIViewController *)isclass withURL:(NSURL *)url
{
    if ([isclass isKindOfClass:[AQPodcastScreen class]]==YES)
    {
        receivedData = receivedata;
        
        float progressive = receivedData/expectedData;
        NSIndexSet *indexes = [arrPodList indexesOfObjectsPassingTest:^BOOL(PodCasts * obj, NSUInteger idx, BOOL *stop){
            if ([obj isKindOfClass:[PodCasts class]]==YES)
            {
            NSString *s = [obj url];
            NSRange range = [s rangeOfString:url.absoluteString];
                return range.location != NSNotFound;

            }else
            {
                return nil;
            }

        }];
    
        NSIndexPath *sender = [NSIndexPath indexPathForRow:([indexes firstIndex]) inSection:0];
//        NSIndexPath *sender = [NSIndexPath indexPathForRow:(cellId) inSection:0];
        NSIndexPath * path = (NSIndexPath *)sender;
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           AQPodCastCell* cellPod = [self.tblPodscast cellForRowAtIndexPath:path];
//                           if (cellPod==nil) {
//                               NSPredicate *predicate=[NSPredicate predicateWithFormat:@"SELF.url contains[c] %@",url.absoluteString];
//                               NSArray *arrFilter= [arrPodList filteredArrayUsingPredicate:predicate];
//                               cellPod=arrFilter[0];
//                           }
                           if ([cellPod.cellUrl isEqual:url]) {
                               [cellPod.cellProgress setProgress:progressive];

                           }

                            [cellPod.btnDownload setEnabled:NO];
                           cellPod.isDownloading=YES;
                          // cellPod.imgDownloadpod.image = [UIImage imageNamed:@"icon_download-disable.png"];

                       });
    }
   
}

-(void)scrollViewEndCallBack:(NSInteger)index
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
    AppDelegate *appdel= (AppDelegate *)[[UIApplication sharedApplication] delegate] ;
   

    if (appdel.openPodcastCellArray.count==0)
    {

        [appdel.openPodcastCellArray addObject:[NSString stringWithFormat:@"%ld",(long)index]];
        
    }else
    {
       
                          NSInteger indexVal=[[appdel.openPodcastCellArray objectAtIndex:0]integerValue];
//                          long val=[appdel.openPodcastCellArray[0] longValue];
                          AQPodCastCell *oldcell=[self.tblPodscast cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexVal inSection:0]];
                          [oldcell.scrlVW setContentOffset:CGPointMake(0, 0)];
        
       
        [appdel.openPodcastCellArray removeAllObjects];
       [appdel.openPodcastCellArray addObject:[NSString stringWithFormat:@"%ld",(long)index]];

    }

});

    
}

-(void)downloadFailedWithError:(int)index
{
    NSIndexPath *sender = [NSIndexPath indexPathForRow:(index) inSection:0];
    NSIndexPath * path = (NSIndexPath *)sender;
    AQPodCastCell* cellPod = [self.tblPodscast cellForRowAtIndexPath:path];
    [cellPod.btnDownload setEnabled:YES];
    cellPod.isDownloading=NO;
}


//getPodCast Request Method
-(void)getPodCastRequest
{
    
    if ([AQHelpers isNetworkAvailable])
    {
    if (!isIntialLoad)
    {
        [self.activityIndicator startAnimating];
    }
        isoffline=NO;
    AQGetPodCastsRequest *request=[[AQGetPodCastsRequest alloc]init];
    [request setBaseUrl:SERVERAPIBASEURL];
    [request setServiceUriMiddleValue:[NSString stringWithFormat:@"%@%@",SERVERAPIMEDILEURL,@"podcast"]];
    [request setPodType:@"standard"];
    if (!isIntialLoad)
    {
        [request setPageNo:++pageCount];
    }
    else
    {
        pageCount=1;
        [request setPageNo:pageCount];
        
    }
    [request setPageSize:20];
        
        if ([strForReferingMethod isEqualToString:@"searchByDate"])
        {
            [request setpubDate:[self convertdatetostring:_datePicker.date]];

        }else if ([strForReferingMethod isEqualToString:@"searchByText"])
        {
            [request setSearchString:_txtSearchicon.text];

        }else
        {
            
        }
        
        
    [AQDataManager getMasterData:request controller:self];
    
}
else
{
    [AQHelpers hideActivityIndicator:self];
//    UIAlertView *alertConnection=[[UIAlertView alloc]initWithTitle:@"Connection Failed" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alertConnection show];
    isoffline=YES;
    if ([[[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]allKeys]containsObject:@"arrayWithoutInternet"]) {
        NSMutableArray *arr=[[NSUserDefaults standardUserDefaults]objectForKey:@"arrayWithoutInternet"];
        NSMutableArray *newarray=[[NSMutableArray alloc]init];
        for (NSData *data in arr)
        {
         PodCasts*    obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            [newarray addObject:obj];
        }
        NSMutableArray *tempArray = [newarray mutableCopy];

        for (PodCasts *pods in newarray)
        {
            NSString *documentsPath    = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            
            
            NSURL *fileURL = [NSURL URLWithString:pods.url];
            NSString *finalPath        = [documentsPath stringByAppendingPathComponent:fileURL.lastPathComponent];
            NSLog(@"%@",finalPath);
            NSFileManager *fileManager = [NSFileManager defaultManager];
            if (![fileManager fileExistsAtPath:finalPath])
            {
                [tempArray removeObject:pods];
            }

        }
        newarray=tempArray;
        arrPodList=newarray;
        makeDeletable=YES;

//        [self.tblPodscast setEditing:YES animated:YES];
        [self.tblPodscast reloadData];

    }else
    {
        
    }

}
}


-(void)bind:(id)obj
{
  
    AQGetPodCastsResponse *response=obj;
    if ([response getisValid])
    {
        NSString *errorMessage = [NSString stringWithFormat:@"%@",response.error.message];
        if ([AQHelpers IsNull:errorMessage])
        {
            
            //check over here something fishy can be found
            if (!isIntialLoad)
            {
               
                    
                for(int i=0;i<response.podCasts.count;++i)
                {
                    [arrPodList addObject:[response.podCasts objectAtIndex:i]];
                }
                

            }
            else
            {
                if (response.podCasts.count==0)
                {
                    AQAlertView(@"Search Completed", @"No Results Found");
                }else
                {
                    arrPodList=response.podCasts;

                }

                
            }
           
            NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
            [myQueue addOperationWithBlock:^{
                
                // Background work
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Main thread work (UI usually)
                    makeDeletable=NO;
                    [self.tblPodscast reloadData];

                }];
            }];



        }
        else{
            AQAlertView(@"Error", errorMessage);
            
        }
        
    }
    else
    {
        AQAlertView(@"Error",@"Unable to connect to the server. Please try again later.");
    }
      [self.activityIndicator stopAnimating];
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
     beginOffset=  scrollView.contentOffset.y;
}
float beginOffset;

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float afterOffset=scrollView.contentOffset.y;
    if (afterOffset - beginOffset >0)
    {
       
    CGFloat height = scrollView.frame.size.height;
    CGFloat contentYoffset = scrollView.contentOffset.y;
    CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
    if(distanceFromBottom == height)
    {
        
            [self doPaging];

   
    }
    }
}


-(void)doPaging
{
    id visibleCells=self.tblPodscast.indexPathsForVisibleRows;
    for (id obj in visibleCells)
    {
        isIntialLoad=false;
        //self.activityIndicator.frame=CGRectMake(self.activityIndicator.frame.origin.x,screenSize.height- self.activityIndicator.frame.size.height - 10,self.activityIndicator.frame.size.width,self.activityIndicator.frame.size.height);
        self.activityIndicator.color=[UIColor whiteColor];
     
            [self getPodCastRequest];
       
        break;
    }
}

- (IBAction)btnFiilerClicked:(UIButton *)sender
{
    [self.txtSearchicon resignFirstResponder];

    if (sender.tag==0) {
        sender.tag=1;
        [_vwSearchIcon setHidden:YES];

    [_scrollVW setContentSize:CGSizeMake(_scrollVW.frame.size.width, _scrollVW.frame.size.height+_vwdatePicker.frame.size.height)];
//    [_scrollVW insertSubview:_vwdatePicker belowSubview:_vwFooter];
    [_vwdatePicker setFrame:CGRectMake(0, _vwFooter.frame.size.height+_vwFooter.frame.origin.y, _vwdatePicker.frame.size.width, _vwdatePicker.frame.size.height)];
    [UIView animateWithDuration:0.3 animations:^{
        [_vwdatePicker setHidden:NO];
        [_scrollVW setContentOffset:CGPointMake(0, _vwdatePicker.frame.size.height)];
    }];
    
    }else
    {
        sender.tag=0;
        self.btnSearch.tag=0;
        [_txtSearchicon resignFirstResponder];

        [UIView animateWithDuration:0.3 animations:^{

        [_scrollVW setContentSize:CGSizeMake(_scrollVW.frame.size.width, _scrollVW.frame.size.height)];


        }completion:^(BOOL finished) {
            if (finished) {
                [_vwdatePicker setHidden:YES];
                [_vwSearchIcon setHidden:YES];

            }

        }];
    }
    
    
}
- (IBAction)btnSearchIconClicked:(UIButton *)sender {
    if (sender.tag==0) {
        sender.tag=1;
        [_vwdatePicker setHidden:YES];

        [_scrollVW setContentSize:CGSizeMake(_scrollVW.frame.size.width, _scrollVW.frame.size.height+_vwSearchIcon.frame.size.height)];
        [_vwSearchIcon setFrame:CGRectMake(0, _vwFooter.frame.size.height+_vwFooter.frame.origin.y, _vwSearchIcon.frame.size.width, _vwSearchIcon.frame.size.height)];
        [_vwSearchIcon setHidden:NO];

        [UIView animateWithDuration:0.3 animations:^{
            [_scrollVW setContentOffset:CGPointMake(0, _vwSearchIcon.frame.size.height)];
        }];
        
    }else
    {
        sender.tag=0;
        self.btnFilter.tag=0;
        [_txtSearchicon resignFirstResponder];

        [UIView animateWithDuration:0.3 animations:^{

        [_scrollVW setContentSize:CGSizeMake(_scrollVW.frame.size.width, _scrollVW.frame.size.height)];
        
        }completion:^(BOOL finished) {
            if (finished) {
                [_vwSearchIcon setHidden:YES];
                [_vwdatePicker setHidden:YES];

            }
            
        }];

    }
    
}


- (IBAction)btnSearchWithTextClicked:(id)sender {
    
//    [_txtSearchicon resignFirstResponder];
        isIntialLoad=true;
        strForReferingMethod=@"searchByText";
    [self hidesearchviewwithBtn:_btnSearch];
    [self getPodCastRequest];

    
    
}

-(void)hidesearchviewwithBtn:(UIButton *)sender
{
    sender.tag=0;
    self.btnFilter.tag=0;
    
    [UIView animateWithDuration:0.3 animations:^{
        [_txtSearchicon resignFirstResponder];

        [_scrollVW setContentSize:CGSizeMake(_scrollVW.frame.size.width, _scrollVW.frame.size.height)];
        
    }completion:^(BOOL finished) {
        if (finished) {
            [_vwSearchIcon setHidden:YES];
            [_vwdatePicker setHidden:YES];
            
        }
        
    }];

}


- (IBAction)btnSearchByDateClicked:(id)sender {
   
        isIntialLoad=true;

    strForReferingMethod=@"searchByDate";
    [self getPodCastRequest];
        [self hidesearchviewwithBtn:_btnFilter];
  
  
   
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(NSString *)convertdatetostring:(NSDate *)date
{
    NSDateComponents *components=[[NSCalendar currentCalendar]components:NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitDay fromDate:date];
    NSString *datestr=[NSString stringWithFormat:@"%ld-%ld-%ld",(long)[components year],(long)[components month],(long)[components day]];
    return datestr;
}


@end
