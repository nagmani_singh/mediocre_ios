//
//  AQBGDownloader.m
//  MediocreApp
//
//  Created by Pankaj Kumar on 07/04/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import "AQBGDownloader.h"
@implementation AQBGDownloader
{
    PodCasts *pods;
    AQPodCastCell *podcell;
}

- (id)initWithAqpodcastCell:(PodCasts *)podcast :(AQPodCastCell *)cell
{
    self = [super init];
    if(self) {
        NSLog(@"_init: %@", self);
        pods = podcast;
        podcell=cell;
        [self startDownloadingWithUrl:pods.url :pods.title];
    }
    return self;
}

-(void)startDownloadingWithUrl:(NSString *)myurl :(NSString *)titleStr
{
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfiguration:@"BGDownload"];
    sessionConfiguration.HTTPMaximumConnectionsPerHost = 5;
    
    NSURLSession *session=[NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self
        delegateQueue:nil];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:myurl]         cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];

    NSURLSessionDownloadTask *downloadTask= [session downloadTaskWithRequest:theRequest];
   //downloadTask.taskIdentifier
    [downloadTask resume];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
      
}


@end
