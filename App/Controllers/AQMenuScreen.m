//
//  AQMenuScreen.m
//  MediocreApp
//
//  Created by Swati Sharma on 08/03/16.
//  Copyright © 2016 Swati Sharma. All rights reserved.
//

#import "AQMenuScreen.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AQLiveStreamScreen.h"
#import "AQHomeScreen.h"
#import "AQPodcastScreen.h"
#import "AQBDMScreen.h"
#import "AQMessageScreen.h"
#import "AppDelegate.h"
#import "AQLoginScreen.h"
#import <AppAPI/AQGetIsUserActiveRequest.h>
#import <AppAPI/AQGetIsUserActiveResponse.h>
#import <AppAPI/AQDataManager.h>
#import "AQCorporatePodcastScreen.h"
#import "AQEventsViewController.h"
#import "AQSponserScreen.h"
@interface AQMenuScreen ()<UITableViewDataSource,UITableViewDelegate,AQMenuDelegate>
{
    AppDelegate *appObj;
    NSString  *strLoginUser;
    NSString *strScreen;
}
@end

@implementation AQMenuScreen


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setFont];
    appObj=[UIApplication sharedApplication].delegate ;
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    NSString *userActiveLocaly=[[NSUserDefaults standardUserDefaults]objectForKey:AUTHKEY];
    if (![AQHelpers IsNull:userActiveLocaly])
            {
      arrMenuList = [[NSArray alloc]initWithObjects:@"HOME",@"PODCASTS",@"BDM",@"ACT",@"LIVE STREAM",@"MEDIOCRE MESSAGE",@"EVENTS",@"MERCHANDISE",@"SPONSORS",@"LOGOUT", nil];
            }else
            {
                  arrMenuList = [[NSArray alloc]initWithObjects:@"HOME",@"PODCASTS",@"BDM",@"ACT",@"LIVE STREAM",@"MEDIOCRE MESSAGE",@"EVENTS",@"MERCHANDISE",@"SPONSORS", nil];
            }
    [self.TblMenu reloadData];
    [self setFooterView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSString *userActiveLocaly=[[NSUserDefaults standardUserDefaults]objectForKey:AUTHKEY];
    if (![AQHelpers IsNull:userActiveLocaly])
    {
        arrMenuList = [[NSArray alloc]initWithObjects:@"HOME",@"PODCASTS",@"BDM",@"ACT",@"LIVE STREAM",@"MEDIOCRE MESSAGE",@"EVENTS",@"MERCHANDISE",@"SPONSORS",@"LOGOUT", nil];
    }else
    {
        arrMenuList = [[NSArray alloc]initWithObjects:@"HOME",@"PODCASTS",@"BDM",@"ACT",@"LIVE STREAM",@"MEDIOCRE MESSAGE",@"EVENTS",@"MERCHANDISE",@"SPONSORS", nil];
    }
    [self.TblMenu reloadData];

}

-(void)setFont{
    self.view.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblFooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.lblFooterDanCom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// UITableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrMenuList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSBundle mainBundle]loadNibNamed:@"AQMenuCell" owner:self options:nil];
    AQMenuCell *cell=self.cellMenu;
    cell.lblTitle.textColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:1.0];
    [cell setDelegate:self];
    cell.btnCell.tag=indexPath.row+1;
    cell.lblTitle.text = [arrMenuList objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.TblMenu deselectRowAtIndexPath:indexPath animated:YES];
    
}
-(void)setFooterView
{
    [self.TblMenu setTableFooterView:self.vwFooter];
}

-(void)clickToCellButton:(NSInteger)tagValue{
    
    if (tagValue==0)
    {
        AQHomeScreen *screen = [[AQHomeScreen alloc]initWithNibName:@"AQHomeScreen" bundle:nil];
        [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
    }
    else if (tagValue==1)
    {
        if ([AQHelpers isNetworkAvailable])
        {
            AQPodcastScreen *screen = [[AQPodcastScreen alloc]initWithNibName:@"AQPodcastScreen" bundle:nil];
            [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
            }else
            {
                if ([[[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]allKeys]containsObject:@"arrayWithoutInternet"])
                {
                    NSMutableArray *arr=[[NSUserDefaults standardUserDefaults]objectForKey:@"arrayWithoutInternet"];
                    if (arr.count>0)
                    {
                        AQPodcastScreen *screen = [[AQPodcastScreen alloc]initWithNibName:@"AQPodcastScreen" bundle:nil];
                        [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
                    }else
                    {
                       
                        AQAlertView(@"Message!", @"You have not yet downloaded any podcast");
                       
                    }
            }else
            {
                AQAlertView(@"Message!", @"You have not yet downloaded any podcast");

            }
        }
        
    }
    else if (tagValue==2)
    {
        
        NSString *authKey =[[NSUserDefaults standardUserDefaults]objectForKey:AUTHKEY];
        [[NSUserDefaults standardUserDefaults]synchronize];
        strScreen=nil;

        if ([AQHelpers IsNull:authKey])
        {
            AQLoginScreen *screen = [[AQLoginScreen alloc]initWithNibName:@"AQLoginScreen" bundle:nil];
            [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
        }
        else{
            [self getIsUserActiveRequest];
        }
        
    }else if (tagValue==3)
    {
        
       
        if ([AQHelpers isNetworkAvailable])
        {
            AQCorporatePodcastScreen *screen = [[AQCorporatePodcastScreen alloc]initWithNibName:@"AQCorporatePodcastScreen" bundle:nil];
            [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
        }else
        {
            if ([[[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]allKeys]containsObject:@"arraycorpWithoutInternet"])
            {
                NSMutableArray *arr=[[NSUserDefaults standardUserDefaults]objectForKey:@"arraycorpWithoutInternet"];
                if (arr.count>0)
                {
                    AQCorporatePodcastScreen *screen = [[AQCorporatePodcastScreen alloc]initWithNibName:@"AQCorporatePodcastScreen" bundle:nil];
                    [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
                }else
                {
                    
                    AQAlertView(@"Message!", @"You have not yet downloaded any podcast");
                    
                }
            }else
            {
                AQAlertView(@"Message!", @"You have not yet downloaded any podcast");
                
            }
        }
        

        
    }
    else if (tagValue==4)
    {
        if ([AQHelpers isNetworkAvailable])
        {
        AQLiveStreamScreen *screen = [[AQLiveStreamScreen alloc]initWithNibName:@"AQLiveStreamScreen" bundle:nil];
        [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
        }else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Connection Failed" message:@"please check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }

    }
    else if (tagValue==5){
        AQMessageScreen *screen = [[AQMessageScreen alloc]initWithNibName:@"AQMessageScreen" bundle:nil];
        [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
    }else if (tagValue==6)
    {
        if ([AQHelpers isNetworkAvailable])
        {
            [AQHelpers showActivtyIndicator:self];
            strScreen=@"Events";
            [[Application instance] authKey:[[NSUserDefaults standardUserDefaults]objectForKey:AUTHKEY]];
            
            AQGetIsUserActiveRequest *request=[[AQGetIsUserActiveRequest alloc]init];
            
            NSString *email = [[NSUserDefaults standardUserDefaults]objectForKey:LOGINEMAILID];
            [request setBaseUrl:SERVERAPIBASEURL];
            [request setServiceUriMiddleValue:SERVERAPIMEDILEURL emailId:email];
            [AQDataManager getMasterData:request controller:self];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Connection Failed" message:@"please check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    else if (tagValue==7)
    {
//        NSString *userActiveLocaly=[[NSUserDefaults standardUserDefaults]objectForKey:@"isuserActiveKey"];
//        if ([userActiveLocaly isEqualToString:@"1"])
//        {
//            AQBDMScreen *screen = [[AQBDMScreen alloc]initWithNibName:@"AQBDMScreen" bundle:nil];
//            [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
//        }else
//        {
//            [self resetData];
//            AQLoginScreen *screen = [[AQLoginScreen alloc]initWithNibName:@"AQLoginScreen" bundle:nil];
//            [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
//        }
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://tomanddan.com/merchandise"]];
       

    }else if (tagValue == 8){
        
        AQSponserScreen *screen = [[AQSponserScreen alloc]initWithNibName:@"AQSponserScreen" bundle:nil];
        [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
        
    }else if (tagValue == 9){
        NSString *userActiveLocaly=[[NSUserDefaults standardUserDefaults]objectForKey:AUTHKEY];
        if ([AQHelpers IsNull:userActiveLocaly])
        {
            AQLoginScreen *screen = [[AQLoginScreen alloc]initWithNibName:@"AQLoginScreen" bundle:nil];
            [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
        }
        else{
            AQAlertView(@"Logged Out", @"You Have Successfully Logged Out");
            [self resetData];
            AQHomeScreen *screen = [[AQHomeScreen alloc]initWithNibName:@"AQHomeScreen" bundle:nil];
            [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
        }
    }
}




//getisUserActive Request Method
-(void)getIsUserActiveRequest{
      if ([AQHelpers isNetworkAvailable])
      {
    [AQHelpers showActivtyIndicator:self];
          [[Application instance] authKey:[[NSUserDefaults standardUserDefaults]objectForKey:AUTHKEY]];

    AQGetIsUserActiveRequest *request=[[AQGetIsUserActiveRequest alloc]init];

     NSString *email = [[NSUserDefaults standardUserDefaults]objectForKey:LOGINEMAILID];
    [request setBaseUrl:SERVERAPIBASEURL];
    [request setServiceUriMiddleValue:SERVERAPIMEDILEURL emailId:email];
    [AQDataManager getMasterData:request controller:self];
}
else{
    [AQHelpers hideActivityIndicator:self];
    if ([[[[NSUserDefaults standardUserDefaults]dictionaryRepresentation]allKeys]containsObject:@"arrayBDMWithoutInternet"]) {
        NSMutableArray *arr=[[NSUserDefaults standardUserDefaults]objectForKey:@"arrayBDMWithoutInternet"];
        if (arr.count>0)
        {
            AQBDMScreen *screen = [[AQBDMScreen alloc]initWithNibName:@"AQBDMScreen" bundle:nil];
            [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
        }else
        {
            AQAlertView(@"Message!", @" You have not yet downloaded any BDM");

        }
    }else
    {
        AQAlertView(@"Message!", @" You have not yet downloaded any BDM");
    }
}
}


-(void)bind:(id)obj
{
   
    
    AQGetIsUserActiveResponse *response=obj;
    if ([response getisValid]) {
        
        NSString *errorMessage = [NSString stringWithFormat:@"%@",response.error.message];
        if ([AQHelpers IsNull:errorMessage]) {
          
            
            NSDictionary *dict = [response dictIsUserActive];
            
            NSString * isuserActive = [NSString stringWithFormat:@"%@",[dict objectForKey:@"IsUserActive"]];
            if (![strScreen isEqualToString:@"Events"]) {
           
            if ([isuserActive isEqualToString:@"1"]) {
                
                [[NSUserDefaults standardUserDefaults]setObject:isuserActive forKey:@"isuserActiveKey"];
                [[NSUserDefaults standardUserDefaults]synchronize];

                
                
                AQBDMScreen *screen = [[AQBDMScreen alloc]initWithNibName:@"AQBDMScreen" bundle:nil];
                [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
                
            }
            else{
                 [self resetData];
                AQLoginScreen *screen = [[AQLoginScreen alloc]initWithNibName:@"AQLoginScreen" bundle:nil];
                [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
            }
            }else{
                AQEventsViewController *screen = [[AQEventsViewController alloc]initWithNibName:@"AQEventsViewController" bundle:nil];
                screen.isUserLoggedIN=isuserActive;
                [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
            }
            
        }
        else{
            AQAlertView(@"Error", errorMessage);
        }
    }
    else
    {
        NSString *errorMessage = [NSString stringWithFormat:@"%@",response.error.message];
        if (![strScreen isEqualToString:@"Events"]) {
       

        if([AQHelpers IsNull:errorMessage])
        {
            AQBDMScreen *screen = [[AQBDMScreen alloc]initWithNibName:@"AQBDMScreen" bundle:nil];
            [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
        }
        else
        {
        AQAlertView(@"Error",errorMessage);

        [self resetData];
        AQLoginScreen *screen = [[AQLoginScreen alloc]initWithNibName:@"AQLoginScreen" bundle:nil];
        [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
        }
         }else{
            AQEventsViewController *screen = [[AQEventsViewController alloc]initWithNibName:@"AQEventsViewController" bundle:nil];
             screen.isUserLoggedIN=nil;
            [appObj.drawerObj setCenterViewController:screen withCloseAnimation:YES completion:nil];
        }
    }
    
             [AQHelpers hideActivityIndicator:self];
    
}


-(void)resetData{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:AUTHKEY];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:ISUSERACTIVE];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:ISUSERLOGGEDIN];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:LOGINEMAILID];
}
@end
