//
//  AQSendImageScreen.m
//  MediocreApp
//
//  Created by Swati Sharma on 10/03/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import "AQSendImageScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>
@interface AQSendImageScreen ()<AVAudioRecorderDelegate,MFMailComposeViewControllerDelegate,AQHeaderDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    MFMailComposeViewController     *picker;
    NSString                        *subjectEmail;
    AQHeaderView                    *header;
    AppDelegate                     *appObj;
    UIImagePickerController         *pickerController;
    NSString                        *sentFileName;
    MFMailComposeViewController     *mailPicker;
}
@end

@implementation AQSendImageScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    appObj=[UIApplication sharedApplication].delegate ;
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [AQHelpers setScrollVwContentSize:self.vwScroll];
    [self setHeaderView];
    [self setFontAndColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setHeaderView{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"SEND PHOTO"];
    header.delegate=self;
    [self.vwHeader addSubview:header.view];
    [header.imgMenu setHidden:YES];
    [header.imgBack setHidden:NO];
    [header.btnMenu setHidden:YES];
    [header.btnBack setHidden:NO];
}
-(void)setFontAndColor{
    self.vwfooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblFooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.btnFooterDan.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.vwScroll.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    [self.btnCamera setTitleColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:0.8] forState:UIControlStateNormal];
     [self.btnGallery setTitleColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:0.8] forState:UIControlStateNormal];
}
-(void)clickToMenu{
    
}
-(void)clickToBack{
    [self.navigationController popViewControllerAnimated:self];
}

- (IBAction)btnCameraClicked:(id)sender{
    subjectEmail=@"Image From Camera";
    sentFileName=@"cameraImage.jpeg";
     [self initilizePhotoCapturing];
}

-(void)initilizePhotoCapturing
{
    self.selectedImage=nil;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
        pickerController.allowsEditing = NO;
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:pickerController animated:YES completion:NULL];
    }
}


- (IBAction)btnGalleryClicked:(id)sender{
    subjectEmail=@"Image From Gallery";
    sentFileName=@"galleryImage.jpeg";
    [self initilizeGalleryImages];
}

-(void)initilizeGalleryImages
{
    self.selectedImage=nil;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        
        pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
        pickerController.allowsEditing = NO;
        pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:pickerController animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    self.selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
    [self performSelectorInBackground:@selector(getNSDataAndOpenMail) withObject:nil];
      
    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:^{
        //[self.navigationController popViewControllerAnimated:YES];
    }];
    
}

-(void)getNSDataAndOpenMail
{

    NSData *imageData = UIImageJPEGRepresentation(self.selectedImage, 1);
    [self performSelectorOnMainThread:@selector(openMailWithAttachment:) withObject:imageData waitUntilDone:YES];
}

-(void)openMailWithAttachment:(NSData*)data
{
    
    if ([MFMailComposeViewController canSendMail])
    {
        mailPicker =[[MFMailComposeViewController alloc] init];
        mailPicker.mailComposeDelegate = self;
        [mailPicker setToRecipients:@[@"hello@tomanddan.com"]];

        [mailPicker setSubject:subjectEmail];
         [mailPicker setToRecipients:[NSArray arrayWithObject:@""]];
       // NSData *imageData = UIImageJPEGRepresentation(self.selectedImage, 1);
        [mailPicker addAttachmentData:data mimeType:@"image/jpeg"
                             fileName:sentFileName];

        [mailPicker setMessageBody:nil isHTML:YES];
        [self presentViewController:mailPicker animated:YES completion:nil];
    }
    else {  
        AQAlertView(nil, ERROR_EMAIL_CONFIGURE);
        [self.navigationController popViewControllerAnimated:YES];
    }


}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString *message=@"";
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = @"";
            break;
        case MFMailComposeResultSent:
            message = @"Email has been sent.";
            break;
        case MFMailComposeResultFailed:
            message=@"Email cannot be sent at this time due to an unknown system problem. Please try again later.";
            break;
    }
    if (message.length>0) {

        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Email" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];

    }
    [self dismissViewControllerAnimated:YES completion:^{
       // [self.navigationController popViewControllerAnimated:YES];
    }];
}


@end
