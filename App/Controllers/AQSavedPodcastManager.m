//
//  AQSavedPodcastManager.m
//  TomandDanMediocreApp
//
//  Created by Pankaj Kumar on 03/08/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import "AQSavedPodcastManager.h"
#define LIMIT 25
@implementation AQSavedPodcastManager


-(void)saveOrUpdatePodcast:(PodCasts *)podcast
{
    if (podcast!=nil) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.title contains[c] %@)",podcast.title];
        NSArray * filteredArray = [_allPodcastArray filteredArrayUsingPredicate:predicate];
    if (_allPodcastArray.count< LIMIT )
    {
       
        if (filteredArray.count>0)
        {
            [self updateandSavePodcast:podcast];
        }else
        {
            [self savePodcast:podcast];
        }

    }else
    {
       
        if (filteredArray.count>0)
        {
            [self updateandSavePodcast:podcast];
        }else
        {
            [self saveForMoreThanLimit:podcast];
        }

    }
}
}

+ (id)sharedManager
{
    static AQSavedPodcastManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        sharedMyManager.allPodcastArray =[NSMutableArray new];
    });
    return sharedMyManager;
}


-(double)getAudioPlayingPosition:(PodCasts *)podcast
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.title contains[c] %@)",podcast.title ];
    NSArray * filteredArray = [_allPodcastArray filteredArrayUsingPredicate:predicate];
    if (filteredArray.count>0)
    {
        return [filteredArray[0] lastTimePlayed];
    }else
    {
        return 0;
    }
}


-(void)saveForMoreThanLimit:(PodCasts *)podcast
{

    [self.allPodcastArray removeObjectAtIndex:0];
    if (_allPodcastArray.count<LIMIT)
    {
        [self.allPodcastArray addObject:podcast];
    }
  
    
}


-(void)updateandSavePodcast:(PodCasts *)podcast
{
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.title contains[c] %@)",podcast.title ];
//    NSArray * filteredArray = [_allPodcastArray filteredArrayUsingPredicate:predicate];
//    if (filteredArray>0)
//    {
        for (int i=0; i<self.allPodcastArray.count; i++)
        {
            if ([podcast id]==[self.allPodcastArray[i] id])
            {
                [self.allPodcastArray replaceObjectAtIndex:i withObject:podcast];
                break;
            }

        }
//        [self savePodcast:podcast];
//    }
   
    
    
}

-(void)savePodcast:(PodCasts *)podcast
{
    [_allPodcastArray addObject:podcast];
}


-(void)savingPodcastOnTermination
{
    if (self.allPodcastArray.count>0)
    {
        NSData *data=[NSKeyedArchiver archivedDataWithRootObject:self.allPodcastArray];
        [[NSUserDefaults standardUserDefaults]setObject:data forKey:@"userData"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}


-(void)getUserData
{
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedObject = [currentDefaults objectForKey:@"userData"];
    if (dataRepresentingSavedObject != nil)
    {
        NSMutableArray *dataArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedObject];
        self.allPodcastArray=[[NSMutableArray alloc]initWithArray:dataArray];
    }
}



@end

