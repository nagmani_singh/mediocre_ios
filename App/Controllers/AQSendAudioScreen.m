//
//  AQSendAudioScreen.m
//  MediocreApp
//
//  Created by Swati Sharma on 10/03/16.
//  Copyright © 2016 com.aquevix. All rights reserved.
//

#import "AQSendAudioScreen.h"
#import "AQHeaderView.h"
#import "AQConfig.h"
#import "AQHelpers.h"
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>
#import "AQPlayerMnager.h"

@interface AQSendAudioScreen ()<AVAudioRecorderDelegate,MFMailComposeViewControllerDelegate,AQHeaderDelegate>
{
    AVAudioRecorder                 *audioRecorder;
    NSURL                           *urlSavedAudio;
    MFMailComposeViewController     *picker;
    NSString                        *subjectEmail;
    AQHeaderView                   *header;
    AppDelegate                    *appObj;
    NSString                       *sentFileName;
    BOOL isPlayingPlayer;
}
@end

@implementation AQSendAudioScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    appObj=[UIApplication sharedApplication].delegate ;
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [AQHelpers setScrollVwContentSize:self.vwScroll];
    [self setHeaderView];
    [self setFontAndColor];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setHeaderView{
    header = [[AQHeaderView alloc]InitWithdata:self header:@"SEND AUDIO"];
    header.delegate=self;
    [self.vwHeader addSubview:header.view];
    [header.imgMenu setHidden:YES];
    [header.imgBack setHidden:NO];
    [header.btnMenu setHidden:YES];
    [header.btnBack setHidden:NO];
}
-(void)setFontAndColor{
    self.vwfooter.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_RED alpha:1.0];
    self.lblFooterTom.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.btnFooterDan.textColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    self.vwScroll.backgroundColor=[UIColor getUIColorObjectFromHexString:COLOR_DARK_RED alpha:1.0];
    [self.btnRecord setTitleColor:[UIColor getUIColorObjectFromHexString:COLOR_LIGHT_YELLOW alpha:0.8] forState:UIControlStateNormal];
}
-(void)clickToMenu{
    
}
-(void)clickToBack{
    [audioRecorder deleteRecording];
    [audioRecorder stop];
    urlSavedAudio=nil;
    [self.navigationController popViewControllerAnimated:self];
}
- (IBAction)btnRecordClicked:(id)sender {
        if (audioRecorder == nil) {
        [self setupAudioRecording];
    }
    [self startAudioRecording];
}


-(void)setupAudioRecording
{
    // Set the audio file
    
    NSArray *pathComponents = [NSArray arrayWithObjects:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],@"MyAudioMemo.m4a", nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    urlSavedAudio=outputFileURL;
    // Setup audio session
    NSError *setOverrideError;
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&setOverrideError];
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    audioRecorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    audioRecorder.delegate = self;
    audioRecorder.meteringEnabled = YES;
    [audioRecorder prepareToRecord];
    
}


-(void)startAudioRecording
{

    if (!audioRecorder.recording)
    {
        if ([[AQPlayerMnager sharedManager] player].rate>0) {
            [[[AQPlayerMnager sharedManager] player]pause];
            isPlayingPlayer=YES;
        }
        [self.btnRecord setTitle:@"STOP" forState:UIControlStateNormal];
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [audioRecorder record];
        
    } else {
        if (isPlayingPlayer==YES) {
            [[[AQPlayerMnager sharedManager] player]play];
            isPlayingPlayer=NO;
        }
        [self.btnRecord setTitle:@"RECORD" forState:UIControlStateNormal];
        [audioRecorder stop];
        [self performSelectorInBackground:@selector(getNSDataAndOpenMail) withObject:nil];
    }
    
}

-(void)getNSDataAndOpenMail
{
    if([urlSavedAudio absoluteString].length>0)
    {
        NSError* error = nil;
        NSData* data = [NSData dataWithContentsOfURL:urlSavedAudio options:0 error:&error];
        [self performSelectorOnMainThread:@selector(openMailWithAttachment:) withObject:data waitUntilDone:YES];
    }

}


-(void)openMailWithAttachment:(NSData*)data
{
    subjectEmail=@"Audio";
    sentFileName=@"Audio";
    if ([MFMailComposeViewController canSendMail])
     {
         picker =[[MFMailComposeViewController alloc] init];
         [picker setToRecipients:@[@"hello@tomanddan.com"]];

         picker.mailComposeDelegate = self;
         [picker setSubject:subjectEmail];
         NSError* error = nil;
         NSLog(@"Audio File Length is %.05f length",(float)(int)[data length]);
         if ([data length]>!(1048576*14)) {
             if (error) {
                 NSLog(@"Unable to load file from provided URL %@: %@", urlSavedAudio, error);
             }
              //[picker setToRecipients:[NSArray arrayWithObject:EMAIL_ID]];
                [picker addAttachmentData:data mimeType:@"audio/mp3"
                                   fileName:sentFileName];

             [picker setMessageBody:nil isHTML:YES];
             [self presentViewController:picker animated:YES completion:nil];
         }
         else
         {
             [self resetAudioRecording];
            AQAlertView(nil, @"Attachment limit exceeded;");
         }
     }
     else{
          [self resetAudioRecording];
         AQAlertView(nil, ERROR_EMAIL_CONFIGURE);
     }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString *message=@"";
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = @"";
            break;
        case MFMailComposeResultSent:
            message = @"Email has been sent.";
            break;
        case MFMailComposeResultFailed:
            message=@"Email cannot be sent at this time due to an unknown system problem. Please try again later.";
            break;
    }
   if (message.length>0) {

       
           UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Email" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
           [alert show];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    [self resetAudioRecording];
}


-(void)resetAudioRecording
{
    [audioRecorder stop];
    [self.btnRecord setTitle:@"RECORD" forState:UIControlStateNormal];

  
}

@end
