//
//  AQSponserScreen.h
//  TomandDanMediocreApp
//
//  Created by techugo on 25/03/18.
//  Copyright © 2018 com.aquevix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AQSponserScreen : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblfooterTom;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionMain;
@property (strong, nonatomic) IBOutlet UILabel *lblfooterdan;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@end
