
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//
//  AQFullScreenCameraController.m
//
//  Added photo merge capability and changed several methods.
////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) Aquevix Solutions Pvt Ltd.
//
//  $AQUEVIX_FRAMEWORK_LICENSE$
//
//     For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
//////////////////////////////////////////////////////////////////////////////
//
//----------------- ORIGINAL --------------------------------------
//
//  Created by P. Mark Anderson on 8/6/2009.
//  Copyright (c) 2009 Bordertown Labs, LLC.
//  
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.
//

#import "AQFullScreenCameraController.h"
#include <QuartzCore/QuartzCore.h>
#import "UIImage+Category.h"
#import "UIImage+RotationAndScalling.h"

#define IS_IPHONE ( [[[UIDevice currentDevice] model] isEqualToString:@"iPhone"] )
#define IS_HEIGHT_GTE_568 [[UIScreen mainScreen ] bounds].size.height >= 568.0f
#define IS_IPHONE_5 ( IS_IPHONE && IS_HEIGHT_GTE_568 )

#define CAMERA_SCALAR5 1.80
#define CAMERA_SCALAR 1.40 // scalar = (480 / (2048 / 480))
#define ScreenWidth 480
#define ScreenHeight 320

@implementation AQFullScreenCameraController

@synthesize statusLabel, shadeOverlay, overlayController,isPortrait;

- (id)init {
    if (self = [super init]) {
        self.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.showsCameraControls = NO;
        self.navigationBarHidden = YES;
        self.toolbarHidden = YES;
        self.wantsFullScreenLayout = YES;
        if (IS_IPHONE_5) {
            self.cameraViewTransform = CGAffineTransformScale(self.cameraViewTransform, CAMERA_SCALAR5, CAMERA_SCALAR5);
        }
        else{
            self.cameraViewTransform = CGAffineTransformScale(self.cameraViewTransform, CAMERA_SCALAR, CAMERA_SCALAR);
        }
		
		if ([self.overlayController respondsToSelector:@selector(initStatusMessage)]) {
			[self.overlayController performSelector:@selector(initStatusMessage)];
		} else {
			[self initStatusMessage];
		}
    }
    return self;
}


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    //AQVB: Here we are setting image Views at runtime
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    bgImgVw=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, screenHeight)];
    overlayImgVw=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, screenHeight)];
	self.shadeOverlay = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlay1.png"]];
	self.shadeOverlay.alpha = 0.0f;
	//[self.view addSubview:self.shadeOverlay];
}


+ (BOOL)isAvailable {
    return [self isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (void)displayModalWithController:(UIViewController*)controller animated:(BOOL)animated {
    [controller presentModalViewController:self animated:YES];
}

- (void)dismissModalViewControllerAnimated:(BOOL)animated {
    [self.overlayController dismissModalViewControllerAnimated:animated];
}



- (void)takePicture {
    
    
    //self.overlayController
    
	if ([self.overlayController respondsToSelector:@selector(cameraWillTakePicture:)]) {
		[self.overlayController performSelector:@selector(cameraWillTakePicture:) withObject:self];
	}
	
	self.delegate = self;
	[self showStatusMessage:@"Taking photo..."];
	
	[self showShadeOverlay];
	[super takePicture];
}

- (UIImage*)dumpOverlayViewToImage {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    NSLog(@"############# Screen width : %f and Height : %f",screenWidth,screenHeight);
	CGSize imageSize = self.cameraOverlayView.bounds.size;
    UIGraphicsBeginImageContext(imageSize);
	[self.cameraOverlayView.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
	return viewImage;
}

-(UIImage*) transformImage:(UIImage*) backgroundImg overlayImage:(UIImage*) ovrlImg overlayBounds:(CGRect) overlayBounds //isLandscape:(BOOL) isLandscape
{
    NSLog(@"transformImage: Background Image width:%f height:%f",backgroundImg.size.width,backgroundImg.size.height);
    
    // Get size of camera.
    //    CGRect screenRect = [[UIScreen mainScreen] bounds];
    //CGFloat screenWidth = screenRect.size.width;
    
    // Size of image
    CGSize bgImgSize=backgroundImg.size;
    double ratio=0;
    if(isPortrait)
        ratio=bgImgSize.height/ovrlImg.size.height;
    else
        ratio=bgImgSize.width/ovrlImg.size.width;
    
    
    
    int overlayXnew = overlayBounds.origin.x  * ratio;
    int overlayYnew = overlayBounds.origin.y  * ratio;
    
    int overlayWidthNew    = overlayBounds.size.width  * ratio;
    int overlayHeightNew   = overlayBounds.size.height * ratio;
    
    // Resize overlay to new size
    UIImage* imgAftrScal=[ovrlImg imageByScalingToSize:CGSizeMake(overlayWidthNew, overlayHeightNew)];
    
    // Now merge
    UIImage* mergedImage=[backgroundImg overlayWith:imgAftrScal xPos:overlayXnew yPos:overlayYnew];
    
    return mergedImage;
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	[self showStatusMessage:@"Saving Photo ..."];
	UIImage *baseImage = [info objectForKey:UIImagePickerControllerOriginalImage];
	if (baseImage == nil) return;
    
	// save composite
	//UIImage *compositeImage = [self addOverlayToBaseImage:baseImage];
    UIImage *overlayImage = [self dumpOverlayViewToImage];
    BOOL isLandscape=true;
    UIDeviceOrientation currOrientation= [[UIDevice currentDevice] orientation];
    if(currOrientation==UIDeviceOrientationLandscapeLeft ||currOrientation==UIDeviceOrientationLandscapeRight)
        isLandscape=true;
    if(currOrientation==UIDeviceOrientationPortrait ||currOrientation==UIDeviceOrientationPortraitUpsideDown)
        isLandscape=false;
    
    CGRect overlayBounds=CGRectMake(0, 0, overlayImage.size.width, overlayImage.size.height);
    UIImage *compositeImage = [self transformImage:baseImage overlayImage:overlayImage overlayBounds:overlayBounds];
	[self hideStatusMessage];
	[self hideShadeOverlay];
    
    
	UIImageWriteToSavedPhotosAlbum(compositeImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
	if ([self.overlayController respondsToSelector:@selector(cameraDidTakePicture:)]) {
		[self.overlayController performSelector:@selector(cameraDidTakePicture:) withObject:self];
	}
}

- (void)image:(UIImage*)image didFinishSavingWithError:(NSError *)error contextInfo:(NSDictionary*)info {
}

- (void)initStatusMessage {
	self.statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height)];
	[self.statusLabel setTextAlignment:NSTextAlignmentCenter];
	self.statusLabel.adjustsFontSizeToFitWidth = YES;
	self.statusLabel.backgroundColor = [UIColor clearColor];
	self.statusLabel.textColor = [UIColor whiteColor];
	self.statusLabel.shadowOffset = CGSizeMake(0, -1);
	self.statusLabel.shadowColor = [UIColor blackColor];
	self.statusLabel.hidden = YES;
	[self.view addSubview:self.statusLabel];
}

- (void)showStatusMessage:(NSString*)message {
	if ([self.overlayController respondsToSelector:@selector(showStatusMessage:)]) {
		[self.overlayController performSelector:@selector(showStatusMessage) withObject:message];
	} else {
		self.statusLabel.text = message;
		self.statusLabel.hidden = NO;
		[self.view bringSubviewToFront:self.statusLabel];
	}
}

- (void)hideStatusMessage {
	if ([self.overlayController respondsToSelector:@selector(hideStatusMessage)]) {
		[self.overlayController performSelector:@selector(hideStatusMessage)];
	} else {
		self.statusLabel.hidden = YES;
	}
}

- (void)showShadeOverlay {
	[self animateShadeOverlay:0.82f];
}

- (void)hideShadeOverlay {
	[self animateShadeOverlay:0.0f];
}

- (void)animateShadeOverlay:(CGFloat)alpha {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.35f];
    self.shadeOverlay.alpha = alpha;
    [UIView commitAnimations];
}

- (void)writeImageToDocuments:(UIImage*)image {
	NSData *png = UIImagePNGRepresentation(image);
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	NSError *error = nil;
	[png writeToFile:[documentsDirectory stringByAppendingPathComponent:@"image.png"] options:NSAtomicWrite error:&error];
}

- (BOOL)canBecomeFirstResponder { return YES; }




@end