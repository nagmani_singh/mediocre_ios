
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) Aquevix Solutions Pvt Ltd.
//
//  $AQUEVIX_FRAMEWORK_LICENSE$
//
//     For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
//////////////////////////////////////////////////////////////////////////////


#import "AQUIExtensions.h"
#import "NINetworkImageView.h"
#include "AQNSExtensions.h"
#include "TPKeyboardAvoidingScrollView.h"


BOOL uiisSave=NO;
AQUIExtensions *uiextensionGlobal;


@implementation  AQUIExtensions
@synthesize parentController;
@synthesize activity;

+(AQUIExtensions *)sharedInstance
{
  if(!uiextensionGlobal) uiextensionGlobal=[[AQUIExtensions alloc]init];
  return uiextensionGlobal;
}

@end

@implementation UIImage (device)

-(void) saveToDeviceGalleryWithCallback:(SEL)selector andTarget:(UIViewController *)controller
{
    UIImageWriteToSavedPhotosAlbum(self, controller, selector, nil);
}

@end

@implementation UIImageView (device)


-(void) loadFromPhotoLibrary:(UIViewController *)controller
{
    [self initilizeImagePickerController:UIImagePickerControllerSourceTypePhotoLibrary withController:controller];

}

-(void) loadFromCamera:(UIViewController *)controller{
    [self initilizeImagePickerController:UIImagePickerControllerSourceTypeCamera withController:controller];
    
}
-(void)loadFromPhotoAlbum:(UIViewController *)controller
{
   [self initilizeImagePickerController:UIImagePickerControllerSourceTypeSavedPhotosAlbum withController:controller];
}

-(void) loadAndSaveFromPhotoLibrary:(UIViewController *)controller
{
    uiisSave=YES;
    [self loadFromPhotoLibrary:controller];

}
-(void) loadAndSaveFromCamera:(UIViewController *)controller
{

    uiisSave=YES;
    [self loadFromCamera:controller];

}
-(void) loadAndSaveFromPhotoAlbum:(UIViewController *)controller
{

    uiisSave=YES;
    [self loadFromPhotoAlbum:controller];

}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self setImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
    if(uiisSave) UIImageWriteToSavedPhotosAlbum(self.image, nil, nil, nil);
    [[AQUIExtensions sharedInstance].parentController dismissModalViewControllerAnimated:YES];
}


-(void)initilizeImagePickerController:(UIImagePickerControllerSourceType)srctyp withController:(UIViewController *)controller
{

    [AQUIExtensions sharedInstance].parentController=controller;
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = srctyp;
    [controller presentModalViewController:picker animated:YES];
}




-(void)loadImageFromURL:(NSString *)imageUrl andActivityIndicator:(UIActivityIndicatorView *)activity
{
    
    NINetworkImageView *imageView=[[NINetworkImageView alloc]init];
    imageView.delegate=self;
    [imageView setPathToNetworkImage:imageUrl];
}

- (void)networkImageViewDidStartLoad:(NINetworkImageView *)imageView
{
 
    [[AQUIExtensions sharedInstance].activity startAnimating];
    
}

/**
 * The image has completed an asynchronous download of the image.
 */
- (void)networkImageView:(NINetworkImageView *)imageView didLoadImage:(UIImage *)imagenetwork
{
    [self setImage:imagenetwork];
    [[AQUIExtensions sharedInstance].activity stopAnimating];
}

/**
 * The asynchronous download failed.
 */
- (void)networkImageView:(NINetworkImageView *)imageView didFailWithError:(NSError *)error
{
    [[AQUIExtensions sharedInstance].activity stopAnimating];
    
//    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"Due to some problem, Image cannot be fetched right now.Please try later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//    [alert show];
//    [alert autorelease];
    
}

@end


@implementation UIScrollView (device)

@end



@implementation UIButton (device)

-(void)setButtonImage:(NSString *)imageName
{
    [self setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}

-(void)setButtonBackgroundImage:(NSString *)imageName
{
    [self setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}
-(void)setButtonTitleColor:(UIColor *)color
{
    [self setTitleColor:color forState:UIControlStateNormal];

}

-(void)setButtonTitle:(NSString *)title
{

  [self setTitle:title forState:UIControlStateNormal];

}


@end
