
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//
//  AQRequest.m
//  ApiFramework
//
//  Created by Velidi Bharatdwaj on 18/11/13.
//  Copyright (c) 2013 Velidi Bharatdwaj. All rights reserved.
//

#import "AQRequest.h"
#import "AQResponse.h"
#include "AQNSExtensions.h"

@interface AQRequest()
{
    NSData*   data;
    NSData *videoData;
    NSData *audioData;
}
@property(nonatomic,strong) NSMutableDictionary* params;
@property(nonatomic,readwrite) AQHttpType httptype;
@property(nonatomic,readwrite) bool isPaged;
@property(nonatomic,readwrite) int pageSize;
@property(nonatomic,readwrite) int pageNum;
@property(nonatomic,readwrite) bool isLocationAware;
@property(nonatomic,readwrite) double lat;
@property(nonatomic,readwrite) double lon;
@property(nonatomic,strong) NSMutableDictionary* headers;
@property(nonatomic,readwrite) bool isPersistable;
@property(nonatomic,strong) NSString* body;
//@property(nonatomic,strong) NSData*   data;
@end

@implementation AQRequest
@synthesize baseUrl,serviceUri,httptype,isPaged,pageSize,pageNum,isLocationAware,lat,lon,headers,params,isPersistable,body;
static NSString* defaultBaseUrl;


-(id) init
{
    self=[super init];
    headers=[[NSMutableDictionary alloc]init];
    params=[[NSMutableDictionary alloc]init];
    return self;
}

-(bool) updateLocation
{
    return false;
}
-(void) nextPage
{}
-(AQResponse*) createResponse
{
    return nil;
}
-(void) serialize
{}
-(void) deserialize
{}
+(NSString*) getdefaultBaseUrl
{
    return defaultBaseUrl;
}
+(void) setdefaultBaseUrl:(NSString*) baseUrl
{
    defaultBaseUrl=baseUrl;
}

-(id) getParam:(NSString*) key
{
    return [self.params objectForKey:key];
}
-(void) setParam:(NSString*) key value:(id) value
{
    [self.params setValue:value forKey:key];
}
-(id) getHeader:(NSString*) key
{
    return [self.headers objectForKey:key];
}
-(void) setHeader:(NSString*) key value:(id) value
{
    [self.headers setValue:value forKey:key];
}

-(NSMutableDictionary *) getParams
{
    return self.params;
}

-(NSMutableDictionary *) getHeaders
{
    return self.headers;
}

-(enum AQHttpType) getType
{
    return self.httptype;
}

-(void) setType:(enum AQHttpType) currType
{
    self.httptype= currType;
}

-(NSString *) getBaseUrl
{
    if(baseUrl.length>0)
        return self.baseUrl;
    else
        return [AQRequest getdefaultBaseUrl];
}

//-(void) setObject:(const Json::Value&) val
//{
//    self.body=[NSString stringWithStdString:Aquevix::AQConvert::toString(val)];
//}
-(void) setString:(NSString *) val
{
    self.body=val;
}

-(NSString *) getString
{
    return self.body;
}
-(void)setVideoData:(NSData*)val {
    videoData = val;
}
-(NSData *)getVideoData {
    return videoData;
}
-(void)setAudioData:(NSData*)val {
    audioData = val;
}
-(NSData *)getAudioData {
    return audioData;
}
-(void) setData:(NSData *) val
{
    data=val;
}
-(NSData *) getData
{
    return data;
}
@end
