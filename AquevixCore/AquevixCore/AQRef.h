
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

////__AQ_INSERT_HEADER
//
////////////////////////////////////////////////////////////////////////////////
////      AQRef.h
////
////      Implementation of base classes and helpers used in automated reference
////      counting and memory management. Includes MT-2 thread safe implementation
////      as well.
////
////      Author: Amit Jindal
////      Date:   02-Jan-2012
////
////      
////      Protected by international laws.
////
////////////////////////////////////////////////////////////////////////////////
//
//#ifndef __AQ_REF_H__
//#define __AQ_REF_H__
//
//#include <Poco/AtomicCounter.h>
//#include <Poco/Mutex.h>
//#include <Poco/ScopedLock.h>
//
//#define AQSTATIC_CAST(T,O) ((T)O)  //static_cast<T>(O)
//#define AQASSERT(x)
//#define AQGUARD2(T,O)   Poco::ScopedLock<T> __l(const_cast<T&>(O));
//
//class AQReference {
//public:
//    AQReference(){}
//    virtual ~AQReference() {}
//
//    void addReference() {
//        ++refCount_;
//    }
//
//    int removeReference() {
//        return refCount_--;
//    }
//
//    int references() const {
//        return refCount_.value();
//    }
//
//private:
//    Poco::AtomicCounter         refCount_;
//private:
//    // unimplemented: So that no one can make copies of this object. Forced.
//    AQReference(const AQReference&);
//    AQReference& operator=(const AQReference&);
//};
//
////#ifdef NOTDEFINED
//
///////////////////////////////////////////////////////////////////////
////
////  class:  AQCountedRef<T>
////
////  Simple class to manage the lifetimes of objects that inherit from
////  AQReference. The class is responsible for reference count management
////  and object ownership sharing and transfer.
////  We also have a multithread level II safe version of this class:
////                      AQMTCountedRef
////  This class also allows the encapsulated object to have a NULL
////     pointer (allows default construction)
////
///////////////////////////////////////////////////////////////////////
//template<class T> 
//class AQCountedRef
//{
//public:
//    AQCountedRef(T* impl)
//        : impl_(impl)
//    {
//        if(impl_) {
//            AQSTATIC_CAST(AQReference*, impl_)->addReference();
//        }
//    }
//
//    AQCountedRef(const AQCountedRef<T>& ref)
//        :impl_(ref.impl_) // Always assign, even if NULL
//    {
//        if(ref.impl_) { 
//            AQSTATIC_CAST(AQReference*, ref.impl_)->addReference();
//        }
//    }
//
//    ~AQCountedRef()
//    {
//        detach();
//    }
//
//    AQCountedRef<T>& operator=(const AQCountedRef<T>& rhs)
//    {
//        if(rhs.impl_) {
//            AQSTATIC_CAST(AQReference*, rhs.impl_)->addReference();
//        }
//        detach();
//        impl_ = rhs.impl_;
//        return *this;
//    }
//
//    AQCountedRef<T>& operator=(T* impl)
//    {
//        if(impl) {
//            AQSTATIC_CAST(AQReference*, impl)->addReference();
//        }
//        detach();
//        impl_ = impl;
//        return *this;
//    }
//
//    operator T*() const { 
//        return impl_; 
//    }
//
//    T* operator->() const { 
//        AQASSERT(impl_);
//        return impl_; 
//    }
//
//    T& operator*() const {
//        AQASSERT(impl_);
//        return *impl_;
//    }
//
//    bool operator==(const AQCountedRef<T>& rhs) const {
//        return (impl_ && (impl_ == rhs.impl_));
//    }
//    bool operator!=(const AQCountedRef<T>& rhs) const {
//        return !(impl_ && (impl_ == rhs.impl_));
//    }
//protected:
//    void detach() {
//        //if(impl_ && AQSTATIC_CAST(AQReference*, impl_)->removeReference() < 1) {
//            if(impl_ && reinterpret_cast<AQReference*>(impl_)->removeReference() < 1) {
//            //If the following line fails then the possibilities are:
//            // (1) It was already deleted?
//            // (2) The object was created on the stack 
//            //delete AQSTATIC_CAST(AQReference*, impl_);
//            delete reinterpret_cast<AQReference*>(impl_);
//            impl_ = NULL;
//        }
//    }
//    T* impl_;
//};
//
////#endif
////#define AQCountedRef    AQMTCountedRef
///////////////////////////////////////////////////////////////////////
////
////     class:  AQMTCountedRef<T>
////
////     Multithread Level-II safe version of AQCountedRef.
////
///////////////////////////////////////////////////////////////////////
//template<class T> 
//class AQMTCountedRef
//{
//    typedef  Poco::Mutex    AQMTMutexType;
//public:
//    // We didn't assign NULL for initialization to avoid threading problems
//    AQMTCountedRef(T* impl)
//    {
//        AQGUARD2(AQMTMutexType, levelIIMutex());
//        if(impl) AQSTATIC_CAST(AQReference*, impl)->addReference();
//        impl_ = impl;
//    }
//    
//    // We didn't assign NULL for initialization to avoid threading problems
//    AQMTCountedRef(const AQMTCountedRef<T>& ref)
//    {
//        AQGUARD2(AQMTMutexType, levelIIMutex());
//        if(ref.impl_) AQSTATIC_CAST(AQReference*, ref.impl_)->addReference();
//        impl_ = ref.impl_;
//    } 
//    virtual ~AQMTCountedRef()
//    {
//        AQGUARD2(AQMTMutexType, levelIIMutex()); 
//        detach();
//    }
//    AQMTCountedRef<T>& operator=(const AQMTCountedRef<T>& rhs)
//    {
//        AQGUARD2(AQMTMutexType, levelIIMutex());
//        if(rhs.impl_) AQSTATIC_CAST(AQReference*, 
//                            rhs.impl_)->addReference();
//        detach();
//        impl_ = rhs.impl_;
//        return *this;
//    }
//    AQMTCountedRef<T>& operator=(T* impl)
//    {
//        AQGUARD2(AQMTMutexType, levelIIMutex());
//        if(impl) AQSTATIC_CAST(AQReference*, impl)->addReference();
//        detach();
//        impl_ = impl;
//        return *this;
//    }
//    operator T*() const { 
//        AQGUARD2(AQMTMutexType, levelIIMutex());
//        return impl_; 
//    }
//    T* operator->() const { 
//        AQGUARD2(AQMTMutexType, levelIIMutex());
//        return impl_; 
//    }
//    T& operator*() const {
//        AQGUARD2(AQMTMutexType, levelIIMutex());
//        return *impl_;
//    }
//    bool operator==(const AQMTCountedRef<T>& rhs) const {
//        AQGUARD2(AQMTMutexType, levelIIMutex());
//        return ( impl_ && ( impl_ == rhs.impl_ ) );
//    }
//    bool operator!=(const AQMTCountedRef<T>& rhs) const {
//        AQGUARD2(AQMTMutexType, levelIIMutex());
//        return ! ( impl_ && ( impl_ == rhs.impl_ ) );
//    }
//protected:
//    /////////////////////////////////////////////////////////////////
//    //
//    // ::referenceCountingMutex (virtual)
//    //   This method returns the mutex to use for MT Level-II safe
//    //   reference counting.  Default implementation is to use the
//    //   same mutex for all instances of this class, but you can
//    //   override this method in a derived class like so to reduce
//    //   contention for this mutex: 
//    //   template <class T>
//    //   class AQMyLevelIISafeCountedRef : public AQMTCountedRef<T> {
//    //          //copy of the levelIIMutex() method here.
//    //   };
//    //
//    /////////////////////////////////////////////////////////////////
//    const AQMTMutexType& levelIIMutex() const { return _mutex;}
//    void detach() {
//        //No need for the MT Guard, because we are already heavily
//        //  guarded against concurrent access.
//        if(impl_ && reinterpret_cast<AQReference*>(impl_)->removeReference() < 1) {
//            //If the following line fails then:
//            // (1) It was already deleted?
//            // (2) The object was created on the stack 
//            delete reinterpret_cast<AQReference*>(impl_);
//            impl_ = NULL;
//        }
//    }
//private:
//    T* impl_;
//    AQMTMutexType _mutex;
//};
//
//
//#endif  // __AQ_REF_H__
