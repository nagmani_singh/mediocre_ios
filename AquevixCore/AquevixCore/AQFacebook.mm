//
/////////////////////////////////////////////////////////////////////////////////
////#aquevix_copyright#
////
//// Copyright (c) Aquevix Solutions Pvt Ltd.
////
//// For any questions you may contact us at:
////
////       Attn:
////         Aquevix Solutions Pvt Ltd.
////         Suite 8-D, A-8 Bigjo's Tower,
////         Netaji Subhash Place,
////         New Delhi - 110034, INDIA
////
////       Contact:
////         http://www.aquevix.com
////         info@aquevix.com
////         +91-11-45600412
////
/////////////////////////////////////////////////////////////////////////////////
////
//// The MIT License (MIT)
//// 
//// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
//// 
//// Permission is hereby granted, free of charge, to any person obtaining a copy
//// of this software and associated documentation files (the "Software"), to deal
//// in the Software without restriction, including without limitation the rights
//// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//// copies of the Software, and to permit persons to whom the Software is
//// furnished to do so, subject to the following conditions:
//// 
//// The above copyright notice and this permission notice shall be included in
//// all copies or substantial portions of the Software.
//// 
//// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//// THE SOFTWARE.
////
/////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////
////
////  Copyright (c) Aquevix Solutions Pvt Ltd.
////
////  $AQUEVIX_FRAMEWORK_LICENSE$
////
////     For any questions you may contact us at:
////
////       Attn:
////         Aquevix Solutions Pvt Ltd.
////         Suite 8-D, A-8 Bigjo's Tower,
////         Netaji Subhash Place,
////         New Delhi - 110034, INDIA
////
////       Contact:
////         http://www.aquevix.com
////         info@aquevix.com
////         +91-11-45600412
////
////////////////////////////////////////////////////////////////////////////////
//
//
//#import "AQFacebook.h"
//#include "AQFileSystem.h"
//#import "SHKItem.h"
//#import "SHK.h"
//#import "SHKFacebook.h"
//#import <Accounts/Accounts.h>
//#import <Social/Social.h>
//#import "AQSocialUserInfo.h"
//
//@interface AQFacebook ()
//{
//    ACAccountStore *accountStore;
//    ACAccount *facebookAccount;
//    BOOL isAccountExist;
//    id _delegate;
//}
//@property(nonatomic,strong) SHKFacebook * fb;
//@property(nonatomic,strong) NSString * fbID;
//-(BOOL) isAccountAdded;
//- (void)getUserCredential;
//@end
//
//@implementation AQFacebook
//@synthesize fb,fbID;
//
///// ******************************************************************
///// init methode for initializing SHKFacebook with facebookID.
/////
///// ******************************************************************
//-(id)init
//{
//    self=[super init];
//    self.fbID= [[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"];
//    //Here we are creating object for SHKFacebook.
//    self.fb=[[SHKFacebook alloc]init];
//    //Here we are checking for any inbuilt FBAccount is added.
//    isAccountExist= [self isAccountAdded];
//    return self;
//}
//
///// ******************************************************************
///// init methode for initializing SHKFacebook with facebookID and
///// delegate for callbacks.
///// ******************************************************************
//-(id) initWithDelegate:(id) delegate
//{
//    self=[self init];
//    _delegate=delegate;
//    return  self;
//}
//
//
///// ******************************************************************
///// Here we are doing login using SHKFacebook.
/////
///// ******************************************************************
//-(void) login
//{
//    //We are calling SHKFacebook whenever isAccountExist is false.
//    if(!isAccountExist)
//        [fb authorize];
//}
//
///// ******************************************************************
///// Here we are doing logout using SHKFacebook.
/////
///// ******************************************************************
//-(void) logout
//{
//    //We are calling SHKFacebook whenever isAccountExist is false.
//    if(!isAccountExist)
//        [SHKFacebook logout];
//}
///// ******************************************************************
///// Here we are checking any account is added or login is there
///// with SHKFacebook.
///// ******************************************************************
//-(bool) isLoggedIn
//{
//    if(isAccountExist)
//        return true;
//    else
//        return [SHKFacebook isServiceAuthorized];
//}
//
///// ******************************************************************
///// Here we are posting message to wall.
/////
///// ******************************************************************
//-(bool) post:(NSString *) message
//{
//    if (isAccountExist) {
//        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//        [controller setInitialText:message];
//        [_delegate presentViewController:controller animated:YES completion:Nil];
//    }
//    else
//    {
//        SHKItem* shareItem=[SHKItem text:message];
//        shareItem.shareType=SHKShareTypeText;
//        [SHKFacebook shareItem:shareItem];
//    }
//    return true;
//}
//
//
///// ******************************************************************
///// Here we are posting message along with ImageURL.
/////
///// ******************************************************************
//-(bool) post:(NSString *) message ImageCaption:(NSString *) imgCaption ImageURL :(NSString *) imgurl
//{
//    if (isAccountExist)
//    {
//        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//        [controller setInitialText:message];
//        [controller addURL:[NSURL URLWithString:imgurl]];
//        [_delegate presentViewController:controller animated:YES completion:Nil];
//    }
//    else
//    {
//        SHKItem* shareItem=[SHKItem URL:[NSURL URLWithString:imgurl] title:imgCaption contentType:SHKURLContentTypeImage];
//        shareItem.shareType=SHKShareTypeURL;
//        [SHKFacebook shareItem:shareItem];
//    }
//    return true;
//}
//
///// ******************************************************************
///// Here we are posting message along with Image.
/////
///// ******************************************************************
//-(bool) post:(NSString *) message ImageCaption:(NSString *) imgCaption Image :(UIImage *) img
//{
//    if (isAccountExist)
//    {
//        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//        [controller setInitialText:message];
//        [controller addImage:img];
//        [_delegate presentViewController:controller animated:YES completion:Nil];
//    }
//    else
//    {
//        SHKItem* shareItem=[SHKItem image:img title:imgCaption];
//        shareItem.shareType=SHKShareTypeImage;
//        [SHKFacebook shareItem:shareItem];
//    }
//    return true;
//}
//
///// ******************************************************************
///// Here we are reading the userDetails like firstName,lastName etc
///// who ever logged in.
///// ******************************************************************
//-(void) requestForUserInfo
//{
//    if(isAccountExist)
//        [self getUserCredential];
//    else
//    {
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sharerAuthDidFinish:) name:SHKSendDidFinishNotification object:nil];
//        [SHKFacebook getUserInfo];
//    }
//}
//
///// ******************************************************************
///// Private
///// Here we will save loggedin userdetails to our object and to
///// userpreferences.
///// ******************************************************************
//
//-(void) saveUserDetails:(NSDictionary *) userInfoDict
//{
//    NSString* fName=[userInfoDict objectForKey:@"first_name"];
//    NSLog(@"AQFacebook Firstname : [%@]",fName);
//    NSString* lName=[userInfoDict objectForKey:@"last_name"];
//    NSLog(@"AQFacebook Lastname : [%@]",lName);
//    NSString* emal=[userInfoDict objectForKey:@"email"];
//    NSLog(@"AQFacebook Email : [%@]",emal);
//    NSString* uid=[userInfoDict objectForKey:@"id"];
//    NSLog(@"AQFacebook Uid : [%@]",uid);
//    AQSocialUserInfo * socialInfo=[[AQSocialUserInfo alloc]init:fName LastName:lName Email:emal UniqueID:uid];
//    if([_delegate respondsToSelector:@selector(userInfo:)])
//        [_delegate userInfo:socialInfo];
//}
//
///// ******************************************************************
///// Callback methode for reading userinfo
///// Here we are saving user credintials of the user who logged in
///// through Sharekit.
///// ******************************************************************
//-(void) sharerAuthDidFinish:(NSNotification*) response
//{
//    NSDictionary *facebookUserInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSHKFacebookUserInfo"];
//    [self saveUserDetails:facebookUserInfo];
//}
//
///// ******************************************************************
///// Private
///// Here we are saving user credintials of the logged userDetails.
///// ******************************************************************
//- (void)getUserCredential{
//    //Here we are creating object for ACAccountStore.
//    if(!accountStore)
//        accountStore = [[ACAccountStore alloc] init];
//    ACAccountType *facebookTypeAccount = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
//    [accountStore requestAccessToAccountsWithType:facebookTypeAccount options:@{ACFacebookAppIdKey:self.fbID, ACFacebookPermissionsKey: @[@"email"]}
//                                       completion:^(BOOL granted, NSError *error)
//     {
//         if(!granted)
//         {
//             // ouch
//             switch ([error code]) {
//                 case 6:
//                     NSLog(@"FBError Occured : [%@]",@"Facebook account does not exists. Please create it in Settings and come back!");
//                     isAccountExist=false;
//                     break;
//                 default:
//                     break;
//             }
//         }
//         else
//         {
//             NSArray *accounts = [accountStore accountsWithAccountType:facebookTypeAccount];
//             facebookAccount = [accounts firstObject];
//         }
//     }];
//    NSURL *meurl = [NSURL URLWithString:@"https://graph.facebook.com/me"];
//    SLRequest *merequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
//                                              requestMethod:SLRequestMethodGET
//                                                        URL:meurl
//                                                 parameters:nil];
//    merequest.account = facebookAccount;
//    [merequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
//    NSDictionary *dictFbInfo = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
//    [self saveUserDetails:dictFbInfo];
//    }];
//}
//
///// ******************************************************************
///// Private
///// Here we will verify any facebook account is added or not.
///// ******************************************************************
//-(BOOL) isAccountAdded
//{
//    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
//        return true;
//    return false;
//}
//
//@end
