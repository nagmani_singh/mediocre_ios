
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//
//  AQRequest.h
//  ApiFramework
//
//  Created by Velidi Bharatdwaj on 18/11/13.
//  Copyright (c) 2013 Velidi Bharatdwaj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AQHttpType.h"


@class AQResponse;

@interface AQRequest : NSObject
@property(nonatomic,strong) NSString* baseUrl;
@property(nonatomic,strong) NSString* serviceUri;

-(bool) updateLocation;
-(void) nextPage;
-(AQResponse*) createResponse;
-(void) serialize;
-(void) deserialize;
+(NSString*) getdefaultBaseUrl;
+(void) setdefaultBaseUrl:(NSString*) baseUrl;
-(id) getParam:(NSString*) key;
-(void) setParam:(NSString*) key value:(id) value;
-(id) getHeader:(NSString*) key;
-(void) setHeader:(NSString*) key value:(id) value;
-(NSMutableDictionary *) getParams;
-(NSMutableDictionary *) getHeaders;
-(enum AQHttpType) getType;
-(void) setType:(enum AQHttpType) currType;
-(NSString *) getBaseUrl;
//-(void) setObject:(const Json::Value&) val;
-(void) setString:(NSString *) val;
-(NSString *) getString;
-(void) setData:(NSData *) val;
-(NSData *) getData;
-(void) setVideoData:(NSData *) val;
-(NSData *) getVideoData;
-(void) setAudioData:(NSData *) val;
-(NSData *) getAudioData;


@end
