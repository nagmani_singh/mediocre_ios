
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) Aquevix Solutions Pvt Ltd.
//
//  $AQUEVIX_FRAMEWORK_LICENSE$
//
//     For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
//////////////////////////////////////////////////////////////////////////////


#import "LocationController.h"
#import "Application.h"
#import "GPSCoordinates.h"

@interface LocationController()
@property (nonatomic, retain) CLLocation *currentLocation;
@property (nonatomic, retain) NSDate* _lastUpdated;
@property (nonatomic, retain) CLLocationManager *locationManager;

@end


@implementation LocationController

@synthesize currentLocation;
@synthesize _lastUpdated;
@synthesize locationManager;

static LocationController *sharedInstance;

+ (LocationController *)sharedInstance {
    @synchronized(self) {
        if (!sharedInstance)
			sharedInstance = [[LocationController alloc] init];
    }
    return sharedInstance;
}

+(id)alloc {
    @synchronized(self) {
        NSAssert(sharedInstance == nil, @"Attempted to allocate a second instance of a singleton LocationController.");
        sharedInstance = [super alloc];
    }
    return sharedInstance;
}

-(id) init {
    if (self = [super init]) {
        self.locationManager = [[CLLocationManager alloc] init];
//        [self.locationManager setDesiredAccuracy:10];
        self.locationManager.delegate = self;
        self._lastUpdated = nil;
        [self start];
    }
    return self;
}

-(void) start {
    [self.locationManager startUpdatingLocation];
}

-(void) stop {
    [self.locationManager stopUpdatingLocation];
}

-(BOOL) locationKnown {
    if (self.currentLocation)
        return YES;
    else
        return NO;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
	if (!self.currentLocation)
		self.currentLocation = [[CLLocation alloc] init];

    //if the time interval returned from core location is more than two minutes we ignore it because it might be from an old session
    if ( abs([newLocation.timestamp timeIntervalSinceDate: [NSDate date]]) < 120) {             
        self.currentLocation = newLocation;
        GPSCoordinates currCoord;
        currCoord.latitude=newLocation.coordinate.latitude;
        currCoord.longitude=newLocation.coordinate.longitude;
        [[Application instance] gpsCoordinates:currCoord];
        if(self._lastUpdated == nil) // || abs([self._lastUpdated timeIntervalSinceDate: [NSDate date]]) > LOCATION_UPDATE_INTERVAL) 
        {
            self._lastUpdated = [NSDate date];
        }
        
    }
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
}
@end