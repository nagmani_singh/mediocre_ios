
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) Aquevix Solutions Pvt Ltd.
//
//  $AQUEVIX_FRAMEWORK_LICENSE$
//
//     For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
////////////////////////////////////////////////////////////////////////////


#include "AQFileSystem.h"
//#include <AQNSExtensions.h>
//#include <Poco/String.h>
//
//namespace Aquevix {
//    
//    bool UserPreferences::Save(const char* key, const std::string& val) {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        NSString* nVal = [NSString stringWithStdString:val];
//        [[NSUserDefaults standardUserDefaults]setObject:nVal forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//
//    bool UserPreferences::Save(const std::string& key, const std::string& val) {
//        NSString* nKey = [NSString stringWithStdString:key];
//        NSString* nVal = [NSString stringWithStdString:val];
//        [[NSUserDefaults standardUserDefaults]setObject:nVal forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    bool UserPreferences::Save(const char* key, const char* val) {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        NSString* nVal = [NSString stringWithUTF8String:val];
//        [[NSUserDefaults standardUserDefaults]setObject:nVal forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    bool UserPreferences::Save(const char* key, bool val){
//        return Save(key, (val?"TRUE":"FALSE"));
//    }
//    
//    bool UserPreferences::Save(const std::string& key, bool val){
//        return Save(key, (val?"TRUE":"FALSE"));
//    }
//    
//    bool UserPreferences::Save(const std::string& key, int val)
//    {
//        NSString* nKey = [NSString stringWithStdString:key];
//        [[NSUserDefaults standardUserDefaults]setInteger:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    bool UserPreferences::Save(const std::string& key, float val)
//    {
//        NSString* nKey = [NSString stringWithStdString:key];
//        [[NSUserDefaults standardUserDefaults]setFloat:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    bool UserPreferences::Save(const std::string& key, double val)
//    {
//        NSString* nKey = [NSString stringWithStdString:key];
//        [[NSUserDefaults standardUserDefaults]setDouble:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    bool UserPreferences::Save(const std::string& key, NSData *val)
//    {
//        NSString* nKey = [NSString stringWithStdString:key];
//        [[NSUserDefaults standardUserDefaults]setObject:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    bool UserPreferences::Save(const std::string& key, NSMutableArray* val)
//    {
//        NSString* nKey = [NSString stringWithStdString:key];
//        [[NSUserDefaults standardUserDefaults]setObject:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//
//    bool UserPreferences::Save(const std::string& key, NSDate *val)
//    {
//        NSString* nKey = [NSString stringWithStdString:key];
//        [[NSUserDefaults standardUserDefaults]setObject:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    bool UserPreferences::Save(const char* key, int val)
//    {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        [[NSUserDefaults standardUserDefaults]setInteger:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    bool UserPreferences::Save(const char* key, float val)
//    {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        [[NSUserDefaults standardUserDefaults]setFloat:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    bool UserPreferences::Save(const char* key, double val)
//    {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        [[NSUserDefaults standardUserDefaults]setDouble:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    bool UserPreferences::Save(const char* key, NSData *val)
//    {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        [[NSUserDefaults standardUserDefaults]setObject:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    bool UserPreferences::Save(const char* key, NSMutableArray* val)
//    {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        [[NSUserDefaults standardUserDefaults]setObject:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//   
//    bool UserPreferences::Save(const char* key, NSDate *val)
//    {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        [[NSUserDefaults standardUserDefaults]setObject:val forKey:nKey];
//        BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
//        return rslt;
//    }
//    
//    int   UserPreferences::ReadInt(const char* key)
//    {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        int response= [[[[NSUserDefaults standardUserDefaults] objectForKey:nKey]mutableCopy]integerValue];
//        return response;
//    }
//    
//    int   UserPreferences::ReadInt(const std::string& key)
//    {
//        NSString* nKey = [NSString stringWithStdString:key];
//        int response= [[[[NSUserDefaults standardUserDefaults] objectForKey:nKey]mutableCopy]integerValue];
//        return response;
//    }
//    
//    float UserPreferences::ReadFloat(const char* key)
//    {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        float response= [[[[NSUserDefaults standardUserDefaults] objectForKey:nKey]mutableCopy]floatValue];
//        return response;
//    }
//    float UserPreferences::ReadFloat(const std::string& key)
//    {
//        NSString* nKey = [NSString stringWithStdString:key];
//        float response= [[[[NSUserDefaults standardUserDefaults] objectForKey:nKey]mutableCopy]floatValue];
//        return response;
//    }
//
//    double UserPreferences::ReadDouble(const char* key) {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        double response= [[[[NSUserDefaults standardUserDefaults] objectForKey:nKey]mutableCopy]doubleValue];
//        return response;
//    }
//    
//    double UserPreferences::ReadDouble(const std::string& key) {
//        NSString* nKey = [NSString stringWithStdString:key];
//        double response= [[[[NSUserDefaults standardUserDefaults] objectForKey:nKey]mutableCopy]doubleValue];
//        return response;
//    }
//    
//    std::string UserPreferences::Read(const char* key) {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        NSString* response= [[[NSUserDefaults standardUserDefaults] objectForKey:nKey] mutableCopy];
//        return (response == nil)?std::string():[response toStdString];
//    }
//    
//    std::string UserPreferences::Read(const std::string& key) {
//        NSString* nKey = [NSString stringWithStdString:key];
//        NSString* response= [[[NSUserDefaults standardUserDefaults] objectForKey:nKey] mutableCopy];
//        return (response == nil)?std::string():[response toStdString];
//    }
//    
//    bool UserPreferences::ReadBoolean(const char* key){
//        return (Poco::icompare(Read(key),"TRUE")==0);
//    }
//    
//    bool UserPreferences::ReadBoolean(const std::string& key){
//        return (Poco::icompare(Read(key),"TRUE")==0);
//    }
//    
//    id UserPreferences::ReadID(const char *key){
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        id response= [[[NSUserDefaults standardUserDefaults] objectForKey:nKey] mutableCopy];
//        return response;
//    }
//    
//    id UserPreferences::ReadID(const std::string& key){
//        NSString* nKey = [NSString stringWithStdString:key];
//        id response= [[[NSUserDefaults standardUserDefaults] objectForKey:nKey] mutableCopy];
//        return response;
//    }
//    
//    NSMutableArray* UserPreferences::ReadArray(const char *key)
//    {
//        NSString* nKey = [NSString stringWithUTF8String:key];
//        NSMutableArray *response= [[[NSUserDefaults standardUserDefaults] objectForKey:nKey]mutableCopy];
//        return response;                                             
//    }
//    
//    NSMutableArray* UserPreferences::ReadArray(const std::string& key)
//    {
//        NSString* nKey = [NSString stringWithStdString:key];
//        NSMutableArray *response= [[[NSUserDefaults standardUserDefaults] objectForKey:nKey]mutableCopy];
//        return response;   
//    }
//}

@implementation UserPreferences
+(bool) save:(NSString *) key value: (NSString *) val
{
    [[NSUserDefaults standardUserDefaults]setObject:val forKey:key];
    BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
    return rslt;
}
+(bool) saveBoolean:(NSString *) key value: (bool) val
{
    return [self save:key value:(val?@"TRUE":@"FALSE")];
}
+(bool) saveInt:(NSString *) key value: (int) val
{
    [[NSUserDefaults standardUserDefaults]setInteger:val forKey:key];
    BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
    return rslt;
}
+(bool) saveFloat:(NSString *) key value: (float) val
{
    [[NSUserDefaults standardUserDefaults]setFloat:val forKey:key];
    BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
    return rslt;
}
+(bool) saveDouble:(NSString *) key value: (double) val
{
    [[NSUserDefaults standardUserDefaults]setDouble:val forKey:key];
    BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
    return rslt;
}
+(bool) saveNSData:(NSString *) key value: (NSData *) val
{
    [[NSUserDefaults standardUserDefaults]setObject:val forKey:key];
    BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
    return rslt;
}
+(bool) saveArray:(NSString *) key value: (NSMutableArray *) val
{
    [[NSUserDefaults standardUserDefaults]setObject:val forKey:key];
    BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
    return rslt;
}
+(bool) saveNSDate:(NSString *) key value: (NSDate *) val
{
    [[NSUserDefaults standardUserDefaults]setObject:val forKey:key];
    BOOL rslt= [[NSUserDefaults standardUserDefaults] synchronize];
    return rslt;
}
+(NSString *) Read:(NSString *) key
{
    NSString* response= [[[NSUserDefaults standardUserDefaults] objectForKey:key] mutableCopy];
    return (response == nil)?@"":response;
}
+(int) ReadInt:(NSString *) key
{
    int response= [[[[NSUserDefaults standardUserDefaults] objectForKey:key]mutableCopy]integerValue];
    return response;
}
+(float) ReadFloat:(NSString *) key
{
    float response= [[[[NSUserDefaults standardUserDefaults] objectForKey:key]mutableCopy]floatValue];
    return response;
}
+(double) ReadDouble:(NSString *) key
{
    double response= [[[[NSUserDefaults standardUserDefaults] objectForKey:key]mutableCopy]doubleValue];
    return response;
}
+(bool) ReadBoolean:(NSString *) key
{
    NSString * val=[self Read:key];
    return [val isEqualToString:@"TRUE"];
}
+(id) ReadID:(NSString *) key
{
    id response= [[[NSUserDefaults standardUserDefaults] objectForKey:key] mutableCopy];
    return response;
}
+(NSMutableArray *) ReadArray:(NSString *) key
{
    NSMutableArray *response= [[[NSUserDefaults standardUserDefaults] objectForKey:key]mutableCopy];
    return response;
}
@end



