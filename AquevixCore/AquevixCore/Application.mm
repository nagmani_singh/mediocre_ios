
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//
//  Application.cpp
//  AQLayoutManager
//
//  Created by Velidi Bharatdwaj on 6/6/12.
//  Copyright (c) 2012 Aquevix Solutions Pvt Ltd. All rights reserved.
//

#import "Application.h"
#import "AquevixOpenUDID.h"
#import "GPSCoordinates.h"

@interface Application ()
{
    GPSCoordinates _gpsCoordinates;
}
@property(nonatomic,retain) NSString * _notifyID;
@property(nonatomic,retain) NSString * _deviceID;
@property(nonatomic,retain) NSString * _versionStr;
@property(nonatomic,retain) NSString * _phoneType;
@property(nonatomic,retain) NSString * _appKey;
@property(nonatomic,retain) NSString * _authKey;
@end


@implementation Application
@synthesize _notifyID,_deviceID,_versionStr,_phoneType,_appKey,_authKey;
-(id)init
{
    if (self = [super init]) {
        _phoneType=@"iPhone";
        NSString* openUDID = [AquevixOpenUDID value];
        if(openUDID != nil) {
            _deviceID=openUDID;
        }
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        _versionStr=version;
    }
    return self;
}

+(Application *) instance
{
    static Application* applcn=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        applcn=[[Application alloc] init];
    });
    return applcn;
}

-(NSString *) deviceID
{
    return self._deviceID;
}
-(NSString *) versionString
{
    return self._versionStr;
}
-(void) versionString:(NSString *) version
{
    self._versionStr=version;
}
-(NSString *) notificationID
{
    return self._notifyID;
}
-(void) notificationID:(NSString *) notifyID
{
    self._notifyID=notifyID;
}
-(NSString *) phoneType
{
    return self._phoneType;
}
-(void) phoneType:(NSString *) phone
{
    self._phoneType=phone;
}
-(GPSCoordinates) gpsCoordinates
{
    return _gpsCoordinates;
}
-(void) gpsCoordinates:(GPSCoordinates) coord
{
    _gpsCoordinates=coord;
}
-(NSString *) appKey
{
    return self._appKey;
}
-(void) appKey:(NSString *) val
{
    self._appKey=val;
}
-(NSString *) authKey
{
    return self._authKey;
}
-(void) authKey:(NSString *) val
{
    self._authKey=val;
}

@end