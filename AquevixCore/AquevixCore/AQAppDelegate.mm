
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//
//  AQAppDelegate.m
//  Aquevix.AppFramework
//
//  Created by Amit Jindal on 24/04/13.
//  Copyright (c) 2013 Aquevix Solutions Pvt Ltd. All rights reserved.
//

#import "AQAppDelegate.h"
#import "LocationController.h"
#import "AQFileSystem.h"
#import "Application.h"
#import <Reachability.h>


@interface AQAppDelegate ()
@property (nonatomic, retain) Reachability *rechability;
@property BOOL isNetworkGone;
@end

@implementation AQAppDelegate


@synthesize window = _window;
@synthesize navigationController;
@synthesize rechability;
@synthesize isNetworkGone;

- (void)dealloc
{
    //[_window release];
    //[super dealloc];
}

-(void)startLocationUpdation
{
    [self requiredLocationInfo:YES];

}

-(void)requiredLocationInfo:(BOOL)required
{
    
    if(required)
    {
        NSLog(@"Override this method to stop your location updation with nop parameter method name- -(void)requiredLocationInfo:(BOOL)required");
       [self setLocationInfo];
    }
    
}

-(void)setLocationInfo
{
     [[LocationController sharedInstance]start];

}


- (void) registerPush {
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
}

- (void) initializeNavigationController {
    
//    NSLog(@"override this method initializeNavigationController to use this");
    
}

- (void) initializeNavigationController:(NSString*) viewName {
    NSAssert((viewName==nil || [viewName isEqualToString:@""]), @"Please provide a valid viewname to initialize.");
    id view = [[NSClassFromString(viewName) alloc]initWithNibName:viewName bundle:nil];
    [self initializeNavigationControllerWithController:view :viewName];
}

- (void) initializeNavigationControllerWithController:(id)view :(NSString*) viewName {
    NSAssert((view==nil), @"RootView cannot be nil.");
    NSAssert((viewName==nil || [viewName isEqualToString:@""]), @"Please provide a valid viewname to initialize.");
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:view];
}

-(void) initAppSettings {
    NSAssert(false, @"This needs to be inherited and settings should be initialized.");
}


-(void)initializeRechability
{
    [self settingRechabilityHost:nil];

}

-(void)settingRechabilityNotification
{
    // Internet is reachable
    self.rechability.reachableBlock = ^(Reachability*reach)
    {
        if(self.isNetworkGone==YES)
        {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network" message:@"Application is now connected to host." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            //[alert release];

        });
      }
    };
    
    // Internet is not reachable
    self.rechability.unreachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network" message:@"Application cannot be connected to host right now.Please check your internet connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
         //   [alert release];
            self.isNetworkGone=YES;
        });
    };
    [self.rechability startNotifier];
}


-(void)settingRechabilityHost:(NSString *)reach
{
    if(reach==nil)
    {
        NSAssert((reach==nil || [reach isEqualToString:@""]), @"Assertion It can not be nil over ride this method to give the value method name : settingRechabilityHost.");
    }
    else
    {
        if([reach rangeOfString:@"http://"].location != NSNotFound)
           reach=[reach stringByReplacingOccurrencesOfString:@"http://" withString:@""];
        if([reach rangeOfString:@"https://"].location != NSNotFound)
            reach=[reach stringByReplacingOccurrencesOfString:@"https://" withString:@""];
        self.rechability = [Reachability reachabilityWithHostname:reach];
        [self settingRechabilityNotification];
    }
}


-(void)settingAppKey:(NSString *)appkey version:(NSString *)version deviceType:(NSString *)type;
{
    [[Application instance] appKey:appkey];
    [[Application instance] phoneType:type];
    [[Application instance] versionString:version];
}

-(void) setAuthKey
{
    NSString * athKey=[UserPreferences Read:@"AUTHKEY"];
    [[Application instance] authKey:athKey];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.isNetworkGone=NO;
    [self initAppSettings];
    [self registerPush];
    [self initializeRechability];
    [self startLocationUpdation];
    [self initializeNavigationController];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    [self.navigationController setNavigationBarHidden:YES];
    
    return YES;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    // TODO: We need to call our library and that should take care of rest.
    
    // NSString *status = [NSString stringWithFormat:@"Device Token: [%@]\n",deviceToken];
    if(deviceToken!=nil)
    {
       // [ApplicationInitializer saveNotifyCationID:deviceToken];
    }
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    NSAssert(false, @"This cannot be called. Please override and use.");
    return false;
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSAssert(false, @"This cannot be called. Please override and use.");
}

@end
