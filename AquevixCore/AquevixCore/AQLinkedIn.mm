//
/////////////////////////////////////////////////////////////////////////////////
////#aquevix_copyright#
////
//// Copyright (c) Aquevix Solutions Pvt Ltd.
////
//// For any questions you may contact us at:
////
////       Attn:
////         Aquevix Solutions Pvt Ltd.
////         Suite 8-D, A-8 Bigjo's Tower,
////         Netaji Subhash Place,
////         New Delhi - 110034, INDIA
////
////       Contact:
////         http://www.aquevix.com
////         info@aquevix.com
////         +91-11-45600412
////
/////////////////////////////////////////////////////////////////////////////////
////
//// The MIT License (MIT)
//// 
//// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
//// 
//// Permission is hereby granted, free of charge, to any person obtaining a copy
//// of this software and associated documentation files (the "Software"), to deal
//// in the Software without restriction, including without limitation the rights
//// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//// copies of the Software, and to permit persons to whom the Software is
//// furnished to do so, subject to the following conditions:
//// 
//// The above copyright notice and this permission notice shall be included in
//// all copies or substantial portions of the Software.
//// 
//// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//// THE SOFTWARE.
////
/////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////
////
////  Copyright (c) Aquevix Solutions Pvt Ltd.
////
////  $AQUEVIX_FRAMEWORK_LICENSE$
////
////     For any questions you may contact us at:
////
////       Attn:
////         Aquevix Solutions Pvt Ltd.
////         Suite 8-D, A-8 Bigjo's Tower,
////         Netaji Subhash Place,
////         New Delhi - 110034, INDIA
////
////       Contact:
////         http://www.aquevix.com
////         info@aquevix.com
////         +91-11-45600412
////
////////////////////////////////////////////////////////////////////////////////
//
//#include "AQLinkedIn.h"
//#import "SHKLinkedIn.h"
//#import "SHK.h"
//#import "SHKItem.h"
//#import "AQSocialUserInfo.h"
//
//@interface AQLinkedIn()
//{
//    id _delegate;
//    BOOL isAccountExist;
//}
//@property(nonatomic,strong) SHKLinkedIn* linkedin;
//-(BOOL) isAccountAdded;
//@end
//
//@implementation AQLinkedIn
//@synthesize linkedin;
//
///// ******************************************************************
///// init methode for initializing SHKLinkedin.
/////
///// ******************************************************************
//-(id) init
//{
//    self.linkedin=[[SHKLinkedIn alloc]init];
//    isAccountExist=false;
//    return self;
//}
//
///// ******************************************************************
///// init methode for initializing SHKTwitter with delegate for
///// callbacks.
///// ******************************************************************
//-(id) initWithDelegate:(id) delegate
//{
//    self=[self init];
//    _delegate=delegate;
//    return self;
//}
//
///// ******************************************************************
///// Here we are doing login using SHKLinkedin.
/////
///// ******************************************************************
//-(void) login
//{
//    if(!isAccountExist)
//        [self.linkedin authorize];
//}
///// ******************************************************************
///// Here we are doing logout using SHKLinkedin.
/////
///// ******************************************************************
//-(void) logout
//{
//    if(!isAccountExist)
//        [SHKLinkedIn logout];
//}
//
///// ******************************************************************
///// Here we are checking any account is added or login is there
///// with SHKLinkedin.
///// ******************************************************************
//-(bool) isLoggedIn
//{
//    if(isAccountExist)
//        return true;
//    else
//        return [SHKLinkedIn isServiceAuthorized];
//}
//
///// ******************************************************************
///// Here we are posting message to wall.
/////
///// ******************************************************************
//-(bool) post:(NSString *) message
//{
//    if(isAccountExist)
//    {}
//    else
//    {
//        SHKItem* shareItem=[SHKItem text:message];
//        shareItem.shareType=SHKShareTypeText;
//        [SHKLinkedIn shareItem:shareItem];
//    }
//    return true;
//}
//
///// ******************************************************************
///// Here we are posting message along with ImageURL.
/////
///// ******************************************************************
//-(bool) post:(NSString *) message ImageCaption:(NSString *) imgCaption ImageURL :(NSString *) imgurl
//{
//    if(isAccountExist)
//    {}
//    else
//    {
//        SHKItem* shareItem=[SHKItem URL:[NSURL URLWithString:imgurl] title:imgCaption contentType:SHKURLContentTypeImage];
//        shareItem.shareType=SHKShareTypeURL;
//        [SHKLinkedIn shareItem:shareItem];
//    }
//    return true;
//}
//
///// ******************************************************************
///// Here we are posting message along with Image.
/////
///// ******************************************************************
//-(bool) post:(NSString *) message ImageCaption:(NSString *) imgCaption Image :(UIImage *) img
//{
//    if(isAccountExist)
//    {}
//    else
//    {
//        SHKItem* shareItem=[SHKItem image:img title:imgCaption];
//        shareItem.shareType=SHKShareTypeImage;
//        [SHKLinkedIn shareItem:shareItem];
//    }
//    return true;
//}
//
///// ******************************************************************
///// Here we are getting AccessToken for the logged user.
/////
///// ******************************************************************
//-(OAToken *) getAccessToken
//{
//    OAToken *tokenKey= [linkedin accessToken];
//    return tokenKey;
//}
//
///// ******************************************************************
///// Here we are getting Consumer for the logged user.
/////
///// ******************************************************************
//-(OAConsumer *) getConsumer
//{
//    OAConsumer *consumeKey=[linkedin consumer];
//    return consumeKey;
//}
//
///// ******************************************************************
///// Private
///// Here we will verify any facebook account is added or not.
///// ******************************************************************
//-(BOOL) isAccountAdded
//{
//    return false;
//}
//
//-(void) requestForUserInfo
//{
//    if(isAccountExist)
//    {}
//    else
//    {
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sharerAuthDidFinish:) name:SHKSendDidFinishNotification object:nil];
//        [SHKLinkedIn getUserInfo];
//    }
//}
//
//
///// ******************************************************************
///// Private
///// Here we will save loggedin userdetails to our object and to
///// userpreferences.
///// ******************************************************************
//
//-(void) saveUserDetails:(NSDictionary *) userInfoDict
//{
//    NSString* fName=[userInfoDict objectForKey:@"first_name"];
//    NSLog(@"AQLinkedin Firstname : [%@]",fName);
//    NSString* lName=[userInfoDict objectForKey:@"last_name"];
//    NSLog(@"AQLinkedin Lastname : [%@]",lName);
//    NSString* emal=[userInfoDict objectForKey:@"email"];
//    NSLog(@"AQLinkedin Email : [%@]",emal);
//    NSString* uid=[userInfoDict objectForKey:@"id"];
//    NSLog(@"AQLinkedin Uid : [%@]",uid);
//    AQSocialUserInfo * socialInfo=[[AQSocialUserInfo alloc]init:fName LastName:lName Email:emal UniqueID:uid];
//    if([_delegate respondsToSelector:@selector(userInfo:)])
//        [_delegate userInfo:socialInfo];
//}
//
///// ******************************************************************
///// Callback methode for reading userinfo
///// Here we are saving user credintials of the user who logged in
///// through Sharekit.
///// ******************************************************************
//-(void) sharerAuthDidFinish:(NSNotification*) response
//{
//    NSDictionary *facebookUserInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSHKLinkedinUserInfo"];
//    [self saveUserDetails:facebookUserInfo];
//}
//
//
//@end
