
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//
//  AQServiceManager.m
//  ApiFramework
//
//  Created by Velidi Bharatdwaj on 18/11/13.
//  Copyright (c) 2013 Velidi Bharatdwaj. All rights reserved.
//

#import "AQServiceManager.h"
#import "AQRequest.h"
#import "AQResponse.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "AQHttpType.h"
//####################### NSMutableArray start #######################

///Category for NSMutableArray for performing
///queue operations.
@implementation NSMutableArray (QueueAdditions)
// Queues are first-in-first-out, so we remove objects from the head
- (id) dequeue {
    // if ([self count] == 0) return nil; // to avoid raising exception (Quinn)
    id headObject =nil;
    @synchronized(self){
        [self objectAtIndex:0];
    }
    if (headObject != nil) {
        @synchronized(self){
            [self removeObjectAtIndex:0];
        }
    }
    return headObject;
}

// Add to the tail of the queue (no one likes it when people cut in line!)
- (void) enqueue:(id)anObject {
    //this method automatically adds to the end of the array
    @synchronized(self){
        [self addObject:anObject];
    }
}
@end

//####################### NSMutableArray end #######################


//####################### RequestwithCallback start #######################
///Here we are declaring class for
///request callback class.
@interface RequestwithCallback:NSObject
-(id)init:(AQRequest*) req delegate:(id) delegt callbackSelector:(SEL) sel;
@property(nonatomic,strong)    AQRequest* request;
@property(nonatomic,strong)    id         delegate;
@property(nonatomic,readwrite) SEL        selector;
@end

@implementation RequestwithCallback
@synthesize request,delegate,selector;
-(id)init:(AQRequest*) req delegate:(id) delegt callbackSelector:(SEL) sel
{
    self=[super init];
    self.request=req;
    self.delegate=delegt;
    self.selector=sel;
    return self;
}
@end

//####################### RequestwithCallback end #######################

//####################### AQServiceManager start #######################

@interface AQServiceManager ()
@property(nonatomic,strong) NSMutableArray* requests;
-(void) execute:(AQRequest*) request delegate: (id) delegate callbackSelector:(SEL) selector;
-(int) maxThreads;
@end

@implementation AQServiceManager
@synthesize requests;
-(int) maxThreads
{
    return 4;
}

+(AQServiceManager*) instance
{
    static AQServiceManager* manager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager=[[self alloc]init];
        manager.requests = [[NSMutableArray alloc]init];
    });
    return manager;
}



-(void) createResponse:(NSHTTPURLResponse *) stringResponse JSONBody:(id) body CallbackRequest:(RequestwithCallback*) requ
{
    AQRequest * request=requ.request;
    AQResponse* resp = [request createResponse];
    NSString* jsonStr=@"";
    if(body!=nil)
    {
        NSData * data = [NSJSONSerialization dataWithJSONObject:body options:0 error:nil];
        jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    bool isValid=false;
    if(stringResponse.statusCode>=200 && stringResponse.statusCode<=299)
        isValid=true;
    [resp initData:request jsonData:jsonStr isValid:isValid ResponseCode:stringResponse.statusCode];
    NSLog(@"\n[AQServiceManager getRequest] JSON GET Response code:%ld \n",(long)stringResponse.statusCode);
    NSLog(@"\n ******************* [AQServiceManager getRequest] Response Start***************** \n");
    NSLog(@"\n %@ \n",jsonStr);
    NSLog(@"\n ******************* [AQServiceManager getRequest] Response End***************** \n");
    [resp parseResult];
    if ([resp getisValid]) {
       if ([requ.delegate respondsToSelector:@selector(onDataSuccess:)]) {
           [requ.delegate performSelectorOnMainThread:@selector(onDataSuccess:) withObject:resp waitUntilDone:false];

       }
    }
    else {
        if ([requ.delegate respondsToSelector:@selector(onDataError:)]) {
          [requ.delegate performSelectorOnMainThread:@selector(onDataError:) withObject:resp waitUntilDone:false];

        }
    }
//    if ([requ.delegate respondsToSelector:requ.selector]) {
//        [requ.delegate performSelectorOnMainThread:requ.selector withObject:resp waitUntilDone:false];
//    }
}


-(void) getRequest:(RequestwithCallback*) requ
{
    NSString* strurl=[NSString stringWithFormat:@"%@%@",[requ.request getBaseUrl],requ.request.serviceUri];
    NSLog(@"[AQServiceManager postRequest] URL:%@",strurl);
   AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSMutableDictionary* headers=[requ.request getHeaders];
    for (id key in [headers allKeys]) {
        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
    }
    [manager GET:requ.request.serviceUri parameters:requ.request.getParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
       [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
     } failure:^(AFHTTPRequestOperation *operation,NSError *error){
       [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
    }];
    
    
}

-(void) postRequest:(RequestwithCallback*) requ
{
    NSString* strurl=[NSString stringWithFormat:@"%@%@",[requ.request getBaseUrl],requ.request.serviceUri];
    NSLog(@"[AQServiceManager postRequest] URL:%@",strurl);
   AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSMutableDictionary* headers=[requ.request getHeaders];
    for (id key in [headers allKeys]) {
        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
    }
    [manager POST:requ.request.serviceUri parameters:[requ.request getParams] success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
    } failure:^(AFHTTPRequestOperation *operation,NSError *error)
    {
        [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
    }];
}


//-(void) putRequest:(RequestwithCallback*) requ
//{
//    NSString* strurl=[NSString stringWithFormat:@"%@%@",[requ.request getBaseUrl],requ.request.serviceUri];
//    NSLog(@"[AQServiceManager postRequest] URL:%@",strurl);
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    NSMutableDictionary* headers=[requ.request getHeaders];
//    for (id key in [headers allKeys]) {
//        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
//    }
//    [manager PUT:requ.request.serviceUri parameters:[requ.request getParams] success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
//    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
//        [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
//    }];
//}


-(void) postBodyRequest:(RequestwithCallback*) requ
{
    NSString* strurl=[NSString stringWithFormat:@"%@%@",[requ.request getBaseUrl],requ.request.serviceUri];
    NSLog(@"[AQServiceManager postRequest] URL:%@",strurl);
   AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSMutableDictionary* headers=[requ.request getHeaders];
    for (id key in [headers allKeys]) {
        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
    }

   [manager POST:requ.request.serviceUri parameters:[requ.request getString] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
        [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
    }];
    

}

//-(void) putBodyRequest:(RequestwithCallback*) requ
//{
//    NSString* strurl=[NSString stringWithFormat:@"%@%@",[requ.request getBaseUrl],requ.request.serviceUri];
//    NSLog(@"[AQServiceManager postRequest] URL:%@",strurl);
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];
//    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
//    NSMutableDictionary* headers=[requ.request getHeaders];
//    for (id key in [headers allKeys]) {
//        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
//    }
//    
//    [manager PUT:requ.request.serviceUri parameters:[requ.request getString] success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
//    } failure:^(AFHTTPRequestOperation *operation,NSError *error){
//        [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
//    }];
//    
//    
//}

//-(void) putImageRequest:(RequestwithCallback*) requ
//{
//    NSString* strurl=[NSString stringWithFormat:@"%@%@",[requ.request getBaseUrl],requ.request.serviceUri];
//    NSLog(@"[AQServiceManager postRequest] URL:%@",strurl);
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    NSMutableDictionary* headers=[requ.request getHeaders];
//    for (id key in [headers allKeys]) {
//        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
//    }
//    NSData *imageData = [requ.request getData];
//    [manager PUT:requ.request.serviceUri parameters:[requ.request getParams] constructingBodyWithBlock:^(id <AFMultipartFormData>formData){
//        [formData appendPartWithFileData:imageData name:@"file" fileName:@"image.png" mimeType:@"image/png"];
//    } success:^(AFHTTPRequestOperation *operation, id responseObject){
//        [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
//    }failure:^(AFHTTPRequestOperation *operation,NSError *error)
//     {
//         [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
//     }];
//}


-(void) postImageRequest:(RequestwithCallback*) requ
{
    NSString* strurl=[NSString stringWithFormat:@"%@%@",[requ.request getBaseUrl],requ.request.serviceUri];
    NSLog(@"[AQServiceManager postRequest] URL:%@",strurl);
   AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSMutableDictionary* headers=[requ.request getHeaders];
    for (id key in [headers allKeys]) {
        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
    }
      NSData *imageData = [requ.request getData];
      [manager POST:requ.request.serviceUri parameters:[requ.request getParams] constructingBodyWithBlock:^(id <AFMultipartFormData>formData){
      [formData appendPartWithFileData:imageData name:@"file" fileName:@"image.png" mimeType:@"image/png"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject){
        [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
    }failure:^(AFHTTPRequestOperation *operation,NSError *error)
    {
        [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
    }];
}

-(void) postVideoRequest:(RequestwithCallback*) requ
{
    NSString* strurl=[NSString stringWithFormat:@"%@%@",[requ.request getBaseUrl],requ.request.serviceUri];
    NSLog(@"[AQServiceManager postRequest] URL:%@",strurl);
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSMutableDictionary* headers=[requ.request getHeaders];
    for (id key in [headers allKeys]) {
        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
    }
    NSData *videoData = [requ.request getVideoData];
    [manager POST:requ.request.serviceUri parameters:[requ.request getParams] constructingBodyWithBlock:^(id <AFMultipartFormData>formData){
        [formData appendPartWithFileData:videoData name:@"file" fileName:@"video.mov" mimeType:@"video/quicktime"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject){
        [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
    }failure:^(AFHTTPRequestOperation *operation,NSError *error)
     {
         [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
     }];
}

//-(void) putVideoRequest:(RequestwithCallback*) requ
//{
//    NSString* strurl=[NSString stringWithFormat:@"%@%@",[requ.request getBaseUrl],requ.request.serviceUri];
//    NSLog(@"[AQServiceManager postRequest] URL:%@",strurl);
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    NSMutableDictionary* headers=[requ.request getHeaders];
//    for (id key in [headers allKeys]) {
//        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
//    }
//    NSData *videoData = [requ.request getVideoData];
//    [manager PUT:requ.request.serviceUri parameters:[requ.request getParams] constructingBodyWithBlock:^(id <AFMultipartFormData>formData){
//        [formData appendPartWithFileData:videoData name:@"file" fileName:@"video.mov" mimeType:@"video/quicktime"];
//    } success:^(AFHTTPRequestOperation *operation, id responseObject){
//        [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
//    }failure:^(AFHTTPRequestOperation *operation,NSError *error)
//     {
//         [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
//     }];
//}

-(void) postAudioRequest:(RequestwithCallback*) requ
{
    NSString* strurl=[NSString stringWithFormat:@"%@%@",[requ.request getBaseUrl],requ.request.serviceUri];
    NSLog(@"[AQServiceManager postRequest] URL:%@",strurl);
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSMutableDictionary* headers=[requ.request getHeaders];
    for (id key in [headers allKeys]) {
        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
    }
    NSData *audioData = [requ.request getAudioData];
    [manager POST:requ.request.serviceUri parameters:[requ.request getParams] constructingBodyWithBlock:^(id <AFMultipartFormData>formData){
        [formData appendPartWithFileData:audioData name:@"file" fileName:@"audio.caf" mimeType:@"audio/caf"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject){
        [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
    }failure:^(AFHTTPRequestOperation *operation,NSError *error)
     {
         [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
     }];
}
//-(void) putAudioRequest:(RequestwithCallback*) requ
//{
//    NSString* strurl=[NSString stringWithFormat:@"%@%@",[requ.request getBaseUrl],requ.request.serviceUri];
//    NSLog(@"[AQServiceManager postRequest] URL:%@",strurl);
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    NSMutableDictionary* headers=[requ.request getHeaders];
//    for (id key in [headers allKeys]) {
//        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
//    }
//    NSData *audioData = [requ.request getAudioData];
//    [manager PUT:requ.request.serviceUri parameters:[requ.request getParams] constructingBodyWithBlock:^(id <AFMultipartFormData>formData){
//        [formData appendPartWithFileData:audioData name:@"file" fileName:@"audio.caf" mimeType:@"audio/caf"];
//    } success:^(AFHTTPRequestOperation *operation, id responseObject){
//        [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
//    }failure:^(AFHTTPRequestOperation *operation,NSError *error)
//     {
//         [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
//     }];
//}

-(void) deleteRequest:(RequestwithCallback*) requ
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager]initWithBaseURL:[NSURL URLWithString:[requ.request getBaseUrl]]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSMutableDictionary* headers=[requ.request getHeaders];
    for (id key in [headers allKeys]) {
        [manager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
    }
    [manager DELETE:requ.request.serviceUri parameters:[requ.request getParams] success:^(AFHTTPRequestOperation *operation, id responseObject){
     [self createResponse:operation.response JSONBody:responseObject CallbackRequest:requ];
    }failure:^(AFHTTPRequestOperation *operation,NSError *error){
         [self createResponse:operation.response JSONBody:operation.responseObject CallbackRequest:requ];
    }];
}

-(void) execute:(RequestwithCallback*) req
{
    if([req.request getType]==GET)
        [self getRequest:req];
    if([req.request getType]==POST)
    {
        NSString* body=[req.request getString];
        NSData * imgData=[req.request getData];
        NSData *videoData = [req.request getVideoData];
        NSData *audioData = [req.request getAudioData];

        if(body.length>0)
            [self postBodyRequest:req];
        else if(imgData.length>0)
            [self postImageRequest:req];
        else if (videoData.length>0)
            [self postVideoRequest:req];
        else if (audioData.length>0)
            [self postAudioRequest:req];
        else
            [self postRequest:req];
    }
//    if([req.request getType]==PUT)
//    {
//        NSString* body=[req.request getString];
//        NSData * imgData=[req.request getData];
//        NSData *videoData = [req.request getVideoData];
//        NSData *audioData = [req.request getAudioData];
//        
//        if(body.length>0)
//            [self putBodyRequest:req];
//        else if(imgData.length>0)
//            [self putImageRequest:req];
//        else if (videoData.length>0)
//            [self putVideoRequest:req];
//        else if (audioData.length>0)
//            [self putAudioRequest:req];
//        else
//            [self putRequest:req];
//    }
    if([req.request getType]==DELETE)
        [self deleteRequest:req];
}   

-(void) enqueue:(AQRequest*) request delegate:(id) delegate callbackSelector:(SEL) selector
{
    RequestwithCallback* req = [[RequestwithCallback alloc] init:request delegate:delegate callbackSelector:selector];
    [self.requests enqueue:req];
    [self execute:req];
}
@end

//####################### AQServiceManager end #######################

