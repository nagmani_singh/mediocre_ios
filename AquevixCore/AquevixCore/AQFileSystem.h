
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) Aquevix Solutions Pvt Ltd.
//
//  $AQUEVIX_FRAMEWORK_LICENSE$
//
//     For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
//////////////////////////////////////////////////////////////////////////////


//#ifndef _AQFILESYSTEM_H
//#define _AQFILESYSTEM_H
//
//#include <string>
//
//namespace Aquevix {
//    class UserPreferences {
//    public:
//        static bool          Save(const char* key, const std::string& val);
//        static bool          Save(const std::string& key, const std::string& val);
//        static bool          Save(const char* key, const char* val);
//        static bool          Save(const char* key, bool val);
//        static bool          Save(const std::string& key, bool val);
//        
//        
//        static bool          Save(const std::string& key, int val);
//        static bool          Save(const std::string& key, float val);
//        static bool          Save(const std::string& key, double val);
//        static bool          Save(const std::string& key, NSData *val);
//        
//        
//        static bool          Save(const char* key, int val);
//        static bool          Save(const char* key, float val);
//        static bool          Save(const char* key, double val);
//        static bool          Save(const char* key, NSData *val);
//        
//        static bool          Save(const char* key, NSMutableArray* val);
//        static bool          Save(const std::string& key, NSMutableArray* val);
//        
//        static bool          Save(const std::string& key, NSDate* val);
//        static bool          Save(const char* key, NSDate* val);
//        
//        
//        static std::string   Read(const char* key);
//        static std::string   Read(const std::string& key);
//        
//        static int           ReadInt(const char* key);
//        static int           ReadInt(const std::string& key);
//        
//        static float         ReadFloat(const char* key);
//        static float         ReadFloat(const std::string& key);
//        
//        static double        ReadDouble(const char* key);
//        static double        ReadDouble(const std::string& key);
//        
//        static bool          ReadBoolean(const char* key);
//        static bool          ReadBoolean(const std::string& key);
//        
//        static id            ReadID(const char* key);
//        static id            ReadID(const std::string& key);
//        
//        static NSMutableArray* ReadArray(const char* key);
//        static NSMutableArray* ReadArray(const std::string& key);
//       
//    
//    };
//    
////    class FileHelper {
////        
////    public:
////        static std::string   Read(const char* filename);
////        static std::string   Read(const std::string& filename);
////
////    };
//}
//
//
//
//#endif

#import <Foundation/Foundation.h>

@interface UserPreferences : NSObject
+(bool) save:(NSString *) key value: (NSString *) val;
+(bool) saveBoolean:(NSString *) key value: (bool) val;
+(bool) saveInt:(NSString *) key value: (int) val;
+(bool) saveFloat:(NSString *) key value: (float) val;
+(bool) saveDouble:(NSString *) key value: (double) val;
+(bool) saveNSData:(NSString *) key value: (NSData *) val;
+(bool) saveArray:(NSString *) key value: (NSMutableArray *) val;
+(bool) saveNSDate:(NSString *) key value: (NSDate *) val;
+(NSString *) Read:(NSString *) key;
+(int) ReadInt:(NSString *) key;
+(float) ReadFloat:(NSString *) key;
+(double) ReadDouble:(NSString *) key;
+(bool) ReadBoolean:(NSString *) key;
+(id) ReadID:(NSString *) key;
+(NSMutableArray *) ReadArray:(NSString *) key;
@end