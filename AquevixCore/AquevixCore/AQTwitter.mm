//
/////////////////////////////////////////////////////////////////////////////////
////#aquevix_copyright#
////
//// Copyright (c) Aquevix Solutions Pvt Ltd.
////
//// For any questions you may contact us at:
////
////       Attn:
////         Aquevix Solutions Pvt Ltd.
////         Suite 8-D, A-8 Bigjo's Tower,
////         Netaji Subhash Place,
////         New Delhi - 110034, INDIA
////
////       Contact:
////         http://www.aquevix.com
////         info@aquevix.com
////         +91-11-45600412
////
/////////////////////////////////////////////////////////////////////////////////
////
//// The MIT License (MIT)
//// 
//// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
//// 
//// Permission is hereby granted, free of charge, to any person obtaining a copy
//// of this software and associated documentation files (the "Software"), to deal
//// in the Software without restriction, including without limitation the rights
//// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//// copies of the Software, and to permit persons to whom the Software is
//// furnished to do so, subject to the following conditions:
//// 
//// The above copyright notice and this permission notice shall be included in
//// all copies or substantial portions of the Software.
//// 
//// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//// THE SOFTWARE.
////
/////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////
////
////  Copyright (c) Aquevix Solutions Pvt Ltd.
////
////  $AQUEVIX_FRAMEWORK_LICENSE$
////
////     For any questions you may contact us at:
////
////       Attn:
////         Aquevix Solutions Pvt Ltd.
////         Suite 8-D, A-8 Bigjo's Tower,
////         Netaji Subhash Place,
////         New Delhi - 110034, INDIA
////
////       Contact:
////         http://www.aquevix.com
////         info@aquevix.com
////         +91-11-45600412
////
////////////////////////////////////////////////////////////////////////////////
//
//#include "AQTwitter.h"
//#import "SHKItem.h"
//#import "SHKTwitter.h"
//#import "SHK.h"
//#import <Accounts/Accounts.h>
//#import <Social/Social.h>
//#import <Twitter/Twitter.h>
//#import "AQSocialUserInfo.h"
//
//@interface AQTwitter()
//{
//    ACAccountStore *accountStore;
//    ACAccount *facebookAccount;
//    BOOL isAccountExist;
//    id _delegate;
//}
//@property(nonatomic,strong) SHKTwitter* twitter;
//- (BOOL) isAccountAdded;
//- (void) getUserCredential;
//@end
//
//@implementation AQTwitter
//@synthesize twitter;
///// ******************************************************************
///// init methode for initializing SHKTwitter.
/////
///// ******************************************************************
//-(id) init
//{
//    //Here we are creating object for SHKTwitter.
//    self.twitter=[[SHKTwitter alloc]init];
//    //Here we are creating object for ACAccountStore.
//    isAccountExist=[self isAccountAdded];
//    return self;
//}
//
///// ******************************************************************
///// init methode for initializing SHKTwitter with delegate for
///// callbacks.
///// ******************************************************************
//-(id) initWithDelegate:(id) delegate
//{
//    self=[self init];
//    _delegate=delegate;
//    return self;
//}
//
///// ******************************************************************
///// Here we are doing login using SHKTwitter.
/////
///// ******************************************************************
//-(void) login
//{
//    if(!isAccountExist)
//        [twitter authorize];
//}
//
///// ******************************************************************
///// Here we are doing logout using SHKTwitter.
///// 
///// ******************************************************************
//-(void) logout
//{
//    if(!isAccountExist)
//        [SHKTwitter logout];
//}
//
///// ******************************************************************
///// Here we are doing logout using SHKTwitter.
/////
///// ******************************************************************
//-(bool) isLoggedIn
//{
//    if(isAccountExist)
//        return true;
//    else
//        return [SHKTwitter isServiceAuthorized];
//}
//
///// ******************************************************************
///// Here we are posting message to wall.
/////
///// ******************************************************************
//-(bool) post:(NSString *) message
//{
//    if (isAccountExist) {
//        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
//        [controller setInitialText:message];
//        [_delegate presentViewController:controller animated:YES completion:Nil];
//    }
//    else
//    {
//        SHKItem* shareItem=[SHKItem text:message];
//        shareItem.shareType=SHKShareTypeText;
//        [SHKTwitter shareItem:shareItem];
//    }
//    return true;
//}
//
//
///// ******************************************************************
///// Here we are posting message along with ImageURL.
/////
///// ******************************************************************
//-(bool) post:(NSString *) message ImageCaption:(NSString *) imgCaption ImageURL :(NSString *) imgurl
//{
//    if (isAccountExist) {
//        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
//        [controller setInitialText:message];
//        [controller addURL:[NSURL URLWithString:imgurl]];
//        [_delegate presentViewController:controller animated:YES completion:Nil];
//    }
//    else
//    {
//        SHKItem* shareItem=[SHKItem URL:[NSURL URLWithString:imgurl] title:imgCaption contentType:SHKURLContentTypeImage];
//        shareItem.shareType=SHKShareTypeURL;
//        [SHKTwitter shareItem:shareItem];
//    }
//    return true;
//}
//
///// ******************************************************************
///// Here we are posting message along with Image.
/////
///// ******************************************************************
//-(bool) post:(NSString *) message ImageCaption:(NSString *) imgCaption Image :(UIImage *) img
//{
//    if (isAccountExist) {
//        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
//        [controller setInitialText:message];
//        [controller addImage:img];
//        [_delegate presentViewController:controller animated:YES completion:Nil];
//    }
//    else
//    {
//        SHKItem* shareItem=[SHKItem image:img title:imgCaption];
//        shareItem.shareType=SHKShareTypeImage;
//        [SHKTwitter shareItem:shareItem];
//    }
//    return true;
//}
//
///// ******************************************************************
///// Here we are reading the userDetails like firstName,lastName etc
///// who ever logged in.
///// ******************************************************************
//-(void) requestForUserInfo
//{
//    if(isAccountExist)
//        [self getUserCredential];
//    else
//    {
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sharerAuthDidFinish:) name:SHKSendDidFinishNotification object:nil];
//        [SHKTwitter getUserInfo];
//    }
//}
//
///// ******************************************************************
///// Private
///// Here we will verify any facebook account is added or not.
///// ******************************************************************
//-(BOOL) isAccountAdded
//{
//    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
//        return true;
//    return false;
//}
//
//
///// ******************************************************************
///// Private
///// Here we will save loggedin userdetails to our object and to
///// userpreferences.
///// ******************************************************************
//
//-(void) saveUserDetails:(NSDictionary *) userInfoDict
//{
//    NSString* fName=[userInfoDict objectForKey:@"name"];
//    NSLog(@"AQTwitter Firstname : [%@]",fName);
//    //There is no LastName in twitter.
//    NSString* lName=@"";
//    NSLog(@"AQTwitter Lastname : [%@]",lName);
//    //There is no Email in twitter.
//    NSString* emal=@"";
//    NSLog(@"AQTwitter Email : [%@]",emal);
//    NSString* uid=[userInfoDict objectForKey:@"user_id"];
//    NSLog(@"AQTwitter Uid : [%@]",uid);
//    AQSocialUserInfo * socialInfo=[[AQSocialUserInfo alloc]init:fName LastName:lName Email:emal UniqueID:uid];
//    if([_delegate respondsToSelector:@selector(userInfo:)])
//        [_delegate userInfo:socialInfo];
//}
//
///// ******************************************************************
///// Here we are saving user credintials of the user who logged in
///// through Sharekit.
///// ******************************************************************
//-(void) sharerAuthDidFinish:(NSNotification*) response
//{
//    NSDictionary *twitterUserInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSHKTwitterUserInfo"];
//    [self saveUserDetails:twitterUserInfo];
//}
//
//
///// ******************************************************************
///// Private
///// For reading userInfo of the logged account user.
///// ******************************************************************
//- (void) getUserCredential
//{
//    if(!accountStore)
//        accountStore = [[ACAccountStore alloc] init];
//    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
//    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
//        if(granted)
//        {
//            NSArray *accounts = [accountStore accountsWithAccountType:accountType];
//             // Check if the users has setup at least one Twitter account
//            if (accounts.count > 0)
//            {
//                ACAccount *twitterAccount = [accounts objectAtIndex:0];
//                // Creating a request to get the info about a user on Twitter
//                NSDictionary* accountInfo=[twitterAccount valueForKeyPath:@"properties"];
//                [self saveUserDetails:accountInfo];
//            }
//        }
//    }];
//}
//
//@end
//
