
///////////////////////////////////////////////////////////////////////////////
//#aquevix_copyright#
//
// Copyright (c) Aquevix Solutions Pvt Ltd.
//
// For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
///////////////////////////////////////////////////////////////////////////////
//
// The MIT License (MIT)
// 
// Copyright (c) 2013 Aquevix Solutions Pvt Ltd. (http://www.aquevix.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
///////////////////////////////////////////////////////////////////////////////

//
//  AQFullScreenCameraController.m
//
//  Added photo merge capability and changed several methods.
////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) Aquevix Solutions Pvt Ltd.
//
//  $AQUEVIX_FRAMEWORK_LICENSE$
//
//     For any questions you may contact us at:
//
//       Attn:
//         Aquevix Solutions Pvt Ltd.
//         Suite 8-D, A-8 Bigjo's Tower,
//         Netaji Subhash Place,
//         New Delhi - 110034, INDIA
//
//       Contact:
//         http://www.aquevix.com
//         info@aquevix.com
//         +91-11-45600412
//
//////////////////////////////////////////////////////////////////////////////
//
//----------------- ORIGINAL --------------------------------------
//
//  Created by P. Mark Anderson on 8/6/2009.
//  Copyright (c) 2009 Bordertown Labs, LLC.
//  
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.
//
    
#import <UIKit/UIKit.h>



@interface AQFullScreenCameraController : UIImagePickerController <UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
	UIViewController *overlayController;
	UILabel *statusLabel;
	UIImageView *shadeOverlay;
    
    //AQVB: Here we are adding UIImageViews
    UIImageView* bgImgVw;
    UIImageView* overlayImgVw;
	
    
}

@property (nonatomic, retain) UIViewController *overlayController;
@property (nonatomic, retain) UILabel *statusLabel;
@property (nonatomic, retain) UIImageView *shadeOverlay;
@property () BOOL isPortrait;

+ (BOOL)isAvailable;
- (void)displayModalWithController:(UIViewController*)controller animated:(BOOL)animated;
- (void)dismissModalViewControllerAnimated:(BOOL)animated;
- (void)takePicture;
- (void)writeImageToDocuments:(UIImage*)image;
- (void)initStatusMessage;
- (void)showStatusMessage:(NSString*)message;
- (void)hideStatusMessage;
- (void)showShadeOverlay;
- (void)hideShadeOverlay;
- (void)animateShadeOverlay:(CGFloat)alpha;

@end
