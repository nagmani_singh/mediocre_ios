Aquevix App Starter Project
===========================

Use this project to start a new iOS application. To do that simply
call the script changeproject.sh with a new project name and it will
reconfigure. Then follow the instructions given at the end of script.

Step by Step Prodecure
----------------------

1. Take a zip of the project aquevix-v5-ios-appstarter.
1. Clone the 'new project' where new application is to be built. Lets say 'MyNewApp'.
1. Extract the files from app-starter and copy paste in MyNewApp.
1. From zip, also copy the .gitignore file (hidden) to new project.
1. Now run the script on terminal in the folder of new app to rename and adjust the project as:
```
changeproject.sh MyNewApp
```
1. Follow the instructions at the end of the script carefully.



